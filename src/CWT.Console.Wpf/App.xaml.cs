﻿using System;
using System.Reflection;
using System.Threading;
using System.Windows;
using CWT.Console.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using Splat;
using Volo.Abp;

namespace CWT.Console
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var app = AbpApplicationFactory.Create<CwtConsoleWpfApplicationModule>();

            app.Initialize();
            app.ServiceProvider.GetRequiredService<IUpdateService>().StartAsync(CancellationToken.None);

            base.OnStartup(e);
        }
    }
}
