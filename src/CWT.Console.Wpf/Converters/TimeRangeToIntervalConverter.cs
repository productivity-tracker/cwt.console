﻿using System;
using System.Globalization;
using System.Windows.Data;
using CWT.Timing;

namespace CWT.Console.Converters
{
    public class TimeRangeToIntervalConverter : IValueConverter
    {
        public object Convert(object value,
                              Type targetType,
                              object parameter,
                              CultureInfo culture)
        {
            if (value != null && value is DateTimeRange timeRange)
            {
                return $"{timeRange.StartTime:HH:mm} / {timeRange.EndTime:HH:mm}";
            }

            return "-";
        }

        public object ConvertBack(object value,
                                  Type targetType,
                                  object parameter,
                                  CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}