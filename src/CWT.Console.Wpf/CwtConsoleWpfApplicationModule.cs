﻿using System;
using System.Reflection;
using CWT.Console.Presentation;
using CWT.Console.Presentation.Login;
using CWT.Console.Services.Updating;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using ReactiveUI;
using Splat;
using Splat.Microsoft.Extensions.DependencyInjection;
using Splat.Microsoft.Extensions.Logging;
using TranslateMe;
using Volo.Abp;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace CWT.Console
{
    [DependsOn(
        typeof(CwtConsoleApplicationModule),
        typeof(AbpAutofacModule))]
    public class CwtConsoleWpfApplicationModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddLogging(lb => lb.AddProvider(new NLogLoggerProvider()));
            TM.Instance.CurrentLanguage = "en"; // TODO
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.UseMicrosoftDependencyResolver();
            context.Services.AddSingleton<LoginViewModel>();
            context.Services.AddSingleton<MainWindowViewModel>();
            context.Services.AddSingleton(new UpdateOptions
            {
                CheckUpdatePeriod = TimeSpan.FromSeconds(30), 
                UrlOrPath = @"https://pl.cwt.host:9990/releases/CwtConsoleApp/"
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            Locator.CurrentMutable.UseMicrosoftExtensionsLoggingWithWrappingFullLogger(context.ServiceProvider.GetRequiredService<ILoggerFactory>());
            Locator.CurrentMutable.InitializeSplat();
            Locator.CurrentMutable.InitializeReactiveUI();

            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
        }
    }
}