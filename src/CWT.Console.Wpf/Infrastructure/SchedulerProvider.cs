﻿using System.Reactive.Concurrency;
using System.Windows.Threading;
using CWT.Console.Common;

namespace CWT.Console.Infrastructure
{
    public class SchedulerProvider : ISchedulerProvider
    {
        public SchedulerProvider(Dispatcher dispatcher)
        {
            MainThread = new DispatcherScheduler(dispatcher);
        }

        public IScheduler MainThread { get; }
        public IScheduler Background => TaskPoolScheduler.Default;
    }
}