﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CWT.Console.Domain.Entities;

namespace CWT.Console.Presentation.Common.Bars
{
    public class EmployeeFilterBar : Control
    {
        public static readonly DependencyProperty SearchBoxTextProperty = DependencyProperty.Register(
            "SearchBoxText",
            typeof(string),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(string)));

        public static readonly DependencyProperty SearchBoxHintProperty = DependencyProperty.Register(
            "SearchBoxHint",
            typeof(string),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(string)));

        public static readonly DependencyProperty OrganizationUnitsProperty = DependencyProperty.Register(
            "OrganizationUnits",
            typeof(ObservableCollection<OrganizationUnit>),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(ObservableCollection<OrganizationUnit>)));

        public static readonly DependencyProperty OrganizationUnitFilterHintProperty = DependencyProperty.Register(
            "OrganizationUnitFilterHint",
            typeof(string),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(string)));

        public static readonly DependencyProperty ClearFiltersButtonToolTipProperty = DependencyProperty.Register(
            "ClearFiltersButtonToolTip",
            typeof(string),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(string)));

        public static readonly DependencyProperty SelectedOrganizationUnitProperty = DependencyProperty.Register(
            "SelectedOrganizationUnit",
            typeof(OrganizationUnit),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(OrganizationUnit)));

        public static readonly DependencyProperty ApplyQuickTimeRangeCommandProperty = DependencyProperty.Register(
            "ApplyQuickTimeRangeCommand",
            typeof(ICommand),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(ICommand)));

        public static readonly DependencyProperty SelectedRangeTextProperty = DependencyProperty.Register(
            "SelectedRangeText",
            typeof(string),
            typeof(EmployeeFilterBar),
            new PropertyMetadata(default(string)));

        public ICommand ApplyQuickTimeRangeCommand
        {
            get => (ICommand)GetValue(ApplyQuickTimeRangeCommandProperty);
            set => SetValue(ApplyQuickTimeRangeCommandProperty, value);
        }

        public string ClearFiltersButtonToolTip
        {
            get => (string)GetValue(ClearFiltersButtonToolTipProperty);
            set => SetValue(ClearFiltersButtonToolTipProperty, value);
        }

        public string OrganizationUnitFilterHint
        {
            get => (string)GetValue(OrganizationUnitFilterHintProperty);
            set => SetValue(OrganizationUnitFilterHintProperty, value);
        }

        public ObservableCollection<OrganizationUnit> OrganizationUnits
        {
            get => (ObservableCollection<OrganizationUnit>)GetValue(OrganizationUnitsProperty);
            set => SetValue(OrganizationUnitsProperty, value);
        }

        public string SearchBoxHint
        {
            get => (string)GetValue(SearchBoxHintProperty);
            set => SetValue(SearchBoxHintProperty, value);
        }

        public string SearchBoxText
        {
            get => (string)GetValue(SearchBoxTextProperty);
            set => SetValue(SearchBoxTextProperty, value);
        }

        public OrganizationUnit SelectedOrganizationUnit
        {
            get => (OrganizationUnit)GetValue(SelectedOrganizationUnitProperty);
            set => SetValue(SelectedOrganizationUnitProperty, value);
        }

        public string SelectedRangeText
        {
            get => (string)GetValue(SelectedRangeTextProperty);
            set => SetValue(SelectedRangeTextProperty, value);
        }

        static EmployeeFilterBar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EmployeeFilterBar), new FrameworkPropertyMetadata(typeof(EmployeeFilterBar)));
        }
    }
}