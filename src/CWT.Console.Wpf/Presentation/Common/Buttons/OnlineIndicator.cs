﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CWT.Console.Presentation.Common.Buttons
{
    public class OnlineIndicator : CheckBox
    {
        public static readonly DependencyProperty OnlineColorProperty = DependencyProperty.Register(
            "OnlineColor",
            typeof(Brush),
            typeof(OnlineIndicator),
            new PropertyMetadata(Brushes.Green));

        public Brush OnlineColor
        {
            get => (Brush)GetValue(OnlineColorProperty);
            set => SetValue(OnlineColorProperty, value);
        }

        static OnlineIndicator()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(OnlineIndicator), new FrameworkPropertyMetadata(typeof(OnlineIndicator)));
        }
    }
}