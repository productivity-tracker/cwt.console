﻿using System.Windows.Media;
using System.Windows;
using System.Windows.Controls.Primitives;
using MaterialDesignThemes.Wpf;

namespace CWT.Console.Presentation.Common.Buttons
{
    public class SwitchIconToggleButton : ToggleButton
    {
        public static readonly DependencyProperty CheckedIconProperty = DependencyProperty.Register(
            "CheckedIcon",
            typeof(PackIconKind),
            typeof(SwitchIconToggleButton),
            new PropertyMetadata(default(PackIconKind)));

        public static readonly DependencyProperty UnCheckedIconProperty = DependencyProperty.Register(
            "UnCheckedIcon",
            typeof(PackIconKind),
            typeof(SwitchIconToggleButton),
            new PropertyMetadata(default(PackIconKind)));

        public static readonly DependencyProperty CheckedColorProperty = DependencyProperty.Register(
            "CheckedColor",
            typeof(Brush),
            typeof(SwitchIconToggleButton),
            new PropertyMetadata(Brushes.Gray));

        public PackIconKind UnCheckedIcon
        {
            get => (PackIconKind)GetValue(UnCheckedIconProperty);
            set => SetValue(UnCheckedIconProperty, value);
        }

        public Brush CheckedColor
        {
            get => (Brush)GetValue(CheckedColorProperty);
            set => SetValue(CheckedColorProperty, value);
        }

        public PackIconKind CheckedIcon
        {
            get => (PackIconKind)GetValue(CheckedIconProperty);
            set => SetValue(CheckedIconProperty, value);
        }

        static SwitchIconToggleButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SwitchIconToggleButton), new FrameworkPropertyMetadata(typeof(SwitchIconToggleButton)));
        }
    }
}