﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CWT.Console.Domain.Entities;
using CWT.Console.Presentation.Common.GridDashboard.MappedValue;

namespace CWT.Console.Presentation.Common.Grids
{
    public class DateGridDashboard : Control
    {
        private DataGrid _dataGrid;

        public static readonly DependencyProperty EmployeesProperty = DependencyProperty.Register(
            "Employees",
            typeof(ObservableCollection<Employee>),
            typeof(DateGridDashboard),
            new PropertyMetadata(default(ObservableCollection<Employee>)));

        public static readonly DependencyProperty FirstColumnNameProperty = DependencyProperty.Register(
            "FirstColumnName",
            typeof(string),
            typeof(DateGridDashboard),
            new PropertyMetadata(default(string)));

        public static readonly DependencyProperty DatesProperty = DependencyProperty.Register(
            "Dates",
            typeof(ObservableCollection<DateInfo>),
            typeof(DateGridDashboard),
            new PropertyMetadata(default(ObservableCollection<DateInfo>)));

        public static readonly DependencyProperty DateToEmployeeMappingProperty = DependencyProperty.Register(
            "DateToEmployeeMapping",
            typeof(MappedValueCollection),
            typeof(DateGridDashboard),
            new PropertyMetadata(default(MappedValueCollection)));

        public static readonly DependencyProperty OpenDetailsCommandProperty = DependencyProperty.Register(
            "OpenDetailsCommand",
            typeof(ICommand),
            typeof(DateGridDashboard),
            new PropertyMetadata(default(ICommand)));

        public static readonly DependencyProperty CellTemplateProperty = DependencyProperty.Register(
            "CellTemplate",
            typeof(DataTemplate),
            typeof(DateGridDashboard),
            new PropertyMetadata(default(DataTemplate)));

        public DataTemplate CellTemplate
        {
            get => (DataTemplate)GetValue(CellTemplateProperty);
            set => SetValue(CellTemplateProperty, value);
        }

        public ObservableCollection<DateInfo> Dates
        {
            get => (ObservableCollection<DateInfo>)GetValue(DatesProperty);
            set => SetValue(DatesProperty, value);
        }

        public MappedValueCollection DateToEmployeeMapping
        {
            get => (MappedValueCollection)GetValue(DateToEmployeeMappingProperty);
            set => SetValue(DateToEmployeeMappingProperty, value);
        }

        public string FirstColumnName
        {
            get => (string)GetValue(FirstColumnNameProperty);
            set => SetValue(FirstColumnNameProperty, value);
        }

        public ObservableCollection<Employee> Employees
        {
            get => (ObservableCollection<Employee>)GetValue(EmployeesProperty);
            set => SetValue(EmployeesProperty, value);
        }

        public ICommand OpenDetailsCommand
        {
            get => (ICommand)GetValue(OpenDetailsCommandProperty);
            set => SetValue(OpenDetailsCommandProperty, value);
        }

        static DateGridDashboard()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DateGridDashboard), new FrameworkPropertyMetadata(typeof(DateGridDashboard)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (GetTemplateChild("TimelineSchedule") is DataGrid dataGrid)
            {
                _dataGrid = dataGrid;
                _dataGrid.MouseDoubleClick += OnOpenDetails;
            }
        }

        private void OnOpenDetails(object sender, MouseButtonEventArgs e)
        {
            var cells = _dataGrid.SelectedCells;
            foreach (var cell in cells)
            {
                var dateText = cell.Column.Header as string;
                if (cell.Item != null && !string.IsNullOrWhiteSpace(dateText))
                {
                    OpenDetailsCommand.Execute(cell.Item);
                }
            }
        }
    }
}