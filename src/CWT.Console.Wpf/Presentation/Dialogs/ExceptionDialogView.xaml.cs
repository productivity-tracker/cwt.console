﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CWT.Console.Presentation.Dialogs
{
    /// <summary>
    /// Interaction logic for ExceptionDialogView.xaml
    /// </summary>
    public partial class ExceptionDialogView : UserControl
    {
        public ExceptionDialogView()
        {
            InitializeComponent();
        }
    }
}
