﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using ReactiveUI;
using Splat;

namespace CWT.Console.Presentation.Login
{
    public partial class LoginWindow : IViewFor<LoginViewModel>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(LoginViewModel), typeof(LoginWindow));

        public LoginViewModel ViewModel
        {
            get => (LoginViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (LoginViewModel)value;
        }
        
        public LoginWindow()
        {
            InitializeComponent();
            ViewModel = Locator.Current.GetService<LoginViewModel>();

            this.WhenActivated(disposableRegistration =>
            {
                this.BindCommand(ViewModel, vm => vm.TrySignIn, v => v.signInBtn)
                    .DisposeWith(disposableRegistration);
                this.Bind(ViewModel, vm => vm.UserName, v => v.userNameTextBox.Text)
                    .DisposeWith(disposableRegistration);
                this.Bind(ViewModel, vm => vm.RememberUsername, v => v.isRememberBtn.IsChecked)
                    .DisposeWith(disposableRegistration);

                ViewModel.TrySignIn.IsExecuting.Subscribe(
                             tryAuthorize => progressBar.Visibility = tryAuthorize ? Visibility.Visible : Visibility.Hidden)
                         .DisposeWith(disposableRegistration);;
                
                Observable.FromEventPattern<RoutedEventHandler, RoutedEventArgs>(
                              handler => pwdBox.PasswordChanged += handler,
                              handler => pwdBox.PasswordChanged -= handler)
                          .Do(_ => ViewModel.Password = pwdBox.Password)
                          .Subscribe()
                          .DisposeWith(disposableRegistration);

                this.Events().KeyUp
                    .Where(x => x.Key == Key.Escape)
                    .Do(_ => Application.Current.Shutdown())
                    .Subscribe()
                    .DisposeWith(disposableRegistration);

                ViewModel.TrySignIn
                         .TakeWhile(authorized => authorized)
                         .FirstAsync()
                         .Do(_ =>
                             {
                                 Application.Current.MainWindow = new MainWindow();
                                 Application.Current.MainWindow?.Show();
                                 this.Close();
                             })
                         .Subscribe()
                         .DisposeWith(disposableRegistration);
            });
        }
    }
}
