﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using CWT.Console.Presentation.Dashboard;
using CWT.Console.Presentation.Timeline;
using ReactiveUI;
using Splat;
using TranslateMe;
using MenuItem = CWT.Console.Presentation.Common.Helpers.Navigation.MenuItem;

namespace CWT.Console.Presentation
{
    public partial class MainWindow : IViewFor<MainWindowViewModel>
    {
        #region ReactiveUI Implementations

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(MainWindowViewModel), typeof(MainWindow));

        public MainWindowViewModel ViewModel
        {
            get => (MainWindowViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MainWindowViewModel)value;
        }

        #endregion

        public bool Hidden { get; private set; } = true;

        public MainWindow()
        {
            InitializeComponent();

            ViewModel = Locator.Current.GetService<MainWindowViewModel>();
            mainWin.Title = $"{TM.Tr(ViewModel.Title)} v{GetRunningVersion()}";
            InitMenu();
        }

        private string GetRunningVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            var version = fileVersionInfo.ProductVersion;
            return version;
        }

        private void OnMainMenuBtnClick(object sender, RoutedEventArgs e)
        {
            Storyboard sb;

            if (Hidden)
            {
                sb = Resources["SbShowLeftMenu"] as Storyboard;
                DarkBack.Visibility = Visibility.Visible;
            }
            else
            {
                sb = Resources["SbHideLeftMenu"] as Storyboard;
                DarkBack.Visibility = Visibility.Hidden;
            }

            sb?.Begin(MainLeftMenuPnl);
            Hidden = !Hidden;
        }

        private void MoveCursorMenu(int index)
        {
            TransitioningContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, 51 * index, 0, 0);
        }

        private void InitMenu()
        {
            _listBoxMenu.ItemsSource = new List<MenuItem>(new[]
            {
                new MenuItem
                {
                    Caption = "Dashboards",
                    Icon = "ViewDashboard",
                    Content = new DashboardView()
                },
                new MenuItem
                {
                    Caption = "Timeline",
                    Icon = "Timetable",
                    Content = new TimelineTabsView()
                },
                new MenuItem
                {
                    Caption = "ScreenshotsHistory",
                    Icon = "ImagesOutline",
                    //Content = new ScreenHistoryTabsView
                    //{
                    //    ViewModel = new ScreenHistoryTabsViewModel(new ViewContainer(TM.Tr("History"), ))
                    //    //DataContext = new ScreenshotsHistoryTabsViewModel(new ViewContainer(TM.Tr("History"),
                    //    //                                                                    new ScreenshotsHistoryDashboardView()))
                    //}
                },
                new MenuItem
                {
                    Caption = "ControlPanel",
                    Icon = "CardBulletedSettingsOutline",
                    //Content = new ControlPanelView()
                },
                new MenuItem
                {
                    Caption = "Downloads",
                    Icon = "Download",
                    //Content = new ControlPanelView()
                }
            });

            _listBoxMenu.SelectedIndex = 0;
        }

        private void ListBoxMenu_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = _listBoxMenu.SelectedIndex;
            MoveCursorMenu(index);
        }
    }
}
