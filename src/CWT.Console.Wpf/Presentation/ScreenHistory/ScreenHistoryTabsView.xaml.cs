﻿using ReactiveUI;

namespace CWT.Console.Presentation.ScreenHistory
{
    public partial class ScreenHistoryTabsView : ReactiveUserControl<ScreenHistoryTabsViewModel>
    {
        public ScreenHistoryTabsView()
        {
            InitializeComponent();
        }
    }
}
