﻿using ReactiveUI;

namespace CWT.Console.Presentation.Timeline
{
    public partial class EmployeesTimelineGridView : ReactiveUserControl<EmployeesTimelineGridViewModel>
    {
        public EmployeesTimelineGridView()
        {
            InitializeComponent();
        }
    }
}
