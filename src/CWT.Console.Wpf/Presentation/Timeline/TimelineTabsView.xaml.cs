﻿using System.Windows.Controls;

namespace CWT.Console.Presentation.Timeline
{
    /// <summary>
    /// Interaction logic for TimelineTabsView.xaml
    /// </summary>
    public partial class TimelineTabsView : UserControl
    {
        public TimelineTabsView()
        {
            InitializeComponent();
        }
    }
}
