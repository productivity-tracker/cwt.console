﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using CWT.Console.Domain.Interfaces;
using CWT.Console.Presentation;
using NLog;
using NLog.Layouts;
using NLog.Targets;
using Volo.Abp.DependencyInjection;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CWT.Console.Services
{
    public sealed class ApplicationService : IApplicationService, IDisposable
    {
        //private readonly LoginView _loginWindow = new LoginView();

        private readonly ILogger _logger;

        private string _logFolder;
        private MainWindow _mainWindow;

        public string LogFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(_logFolder))
                {
                    return _logFolder;
                }

                _logFolder = GetLogFolder();
                return _logFolder;
            }
        }

        public ApplicationService(ILogger logger)
        {
            _logger = logger;

            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }
        
        public void CopyToClipboard(string text) => Clipboard.SetText(text);

        public void Exit() => Application.Current.Shutdown();

        public void Restart()
        {
            Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        public void OpenFolder(string folder) => Process.Start(fileName: "explorer.exe", folder);

        public void SignIn()
        {
            //Application.Current.MainWindow = _loginWindow;
            //Application.Current.MainWindow?.Show();
        }

        public void LogOn()
        {
            //_mainWindow = new MainWindow();
            //Application.Current.MainWindow = _mainWindow;
            //Application.Current.MainWindow?.Show();
            //_loginWindow.Close();
        }

        public void LogOut()
        {
            SignIn();
            _mainWindow.Close();
        }

        private static string GetLogFolder()
        {
            var logFilePath = LogManager.Configuration.AllTargets.OfType<FileTarget>()
                                        .Select(selector: x => x.FileName as SimpleLayout)
                                        .Select(selector: x => x.Text)
                                        .FirstOrDefault();

            var baseDir = Assembly.GetExecutingAssembly().Location;
            var logFile = logFilePath?.Replace(oldValue: "{basedir}", baseDir);

            return Path.GetDirectoryName(logFile);
        }

        public void Dispose()
        { }
    }
}