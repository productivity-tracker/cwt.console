﻿using System;

namespace CWT.Console.Services.Updating
{
    public class UpdateOptions
    {
        public string UrlOrPath { get; set; }
        public TimeSpan CheckUpdatePeriod { get; set; }
    }
}