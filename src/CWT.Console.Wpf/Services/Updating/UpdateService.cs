﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using CWT.Console.Domain.Interfaces;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;

namespace CWT.Console.Services.Updating
{
    public class UpdateService : IUpdateService, ISingletonDependency, IDisposable
    {
        private const string UPDATE_EXECUTOR = "Update.exe";
        private const string UPDATE_EXECUTOR_RELATIVE_PATH = "../" + UPDATE_EXECUTOR;

        private readonly string _updateExecutorFullPath = Path.GetFullPath(
            UPDATE_EXECUTOR_RELATIVE_PATH,
            Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location));

        private readonly UpdateOptions _options;
        private readonly ILogger _logger;
        private IDisposable _disposable;

        public UpdateService(ILogger<UpdateService> logger, UpdateOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            if (!IsAppDeployed())
            {
                _logger.LogWarning("Not a deployed app");
                return Task.CompletedTask;
            }
            
            _logger.LogDebug("Updater Started");

            _disposable = Observable.Interval(_options.CheckUpdatePeriod, new EventLoopScheduler())
                                    .SelectMany(_ => CheckForSquirrelUpdates().ToObservable())
                                    .Retry()
                                    .Subscribe();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Dispose();
            return Task.CompletedTask;
        }

        public void Dispose() => _disposable?.Dispose();

        public bool IsAppDeployed() => File.Exists(_updateExecutorFullPath);

        private async Task CheckForSquirrelUpdates()
        {
            try
            {
                var (exitCode, output) = await RunSquirrel($"--update {_options.UrlOrPath}");

                if (exitCode != 0)
                {
                    _logger.LogWarning($"Failed to update! {output}");
                }
                else
                {
                    _logger.LogDebug(output);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Failed to invoke {UPDATE_EXECUTOR}.");
            }
        }

        private Task<(int exitCode, string output)> RunSquirrel(string args)
        {
            try
            {
                if (!File.Exists(_updateExecutorFullPath)) throw new Exception($"{UPDATE_EXECUTOR} doesn't exist.");
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, $"{UPDATE_EXECUTOR} not found, this is probably not a deployed app.");
                return Task.FromException<(int, string)>(new Exception("Not a deployed app."));
            }

            return InvokeProcessAsync(_updateExecutorFullPath, args, CancellationToken.None, Path.GetDirectoryName(_updateExecutorFullPath));
        }

        private Task<(int exitCode, string output)> InvokeProcessAsync(
            string fileName,
            string arguments,
            CancellationToken ct,
            string workingDirectory = "")
        {
            var psi = new ProcessStartInfo(fileName, arguments)
            {
                UseShellExecute = false,
                WindowStyle = ProcessWindowStyle.Hidden,
                ErrorDialog = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                WorkingDirectory = workingDirectory
            };

            return InvokeProcessAsync(psi, ct);
        }

        private async Task<(int exitCode, string output)> InvokeProcessAsync(ProcessStartInfo psi, CancellationToken ct)
        {
            var process = Process.Start(psi);

            await Task.Run(
                () =>
                {
                    while (!ct.IsCancellationRequested)
                    {
                        if (process.WaitForExit(2000)) return;
                    }

                    if (ct.IsCancellationRequested)
                    {
                        process.Kill();
                        ct.ThrowIfCancellationRequested();
                    }
                },
                ct);

            var textResult = await process.StandardOutput.ReadToEndAsync();
            if (string.IsNullOrWhiteSpace(textResult) || process.ExitCode != 0)
            {
                textResult = (textResult ?? "") + "\n" + await process.StandardError.ReadToEndAsync();

                if (string.IsNullOrWhiteSpace(textResult))
                {
                    textResult = string.Empty;
                }
            }

            return (process.ExitCode, textResult.Trim());
        }
    }
}