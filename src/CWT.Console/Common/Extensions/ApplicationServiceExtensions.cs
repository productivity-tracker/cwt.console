﻿using CWT.Console.Data.Network;
using Microsoft.Extensions.Caching.Memory;
using Volo.Abp.Application.Services;
using Volo.Abp.EventBus.SignalR.Client;

namespace CWT.Console.Common.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static ICrudAppService<TEntity, TKey> AsCached<TEntity, TKey>(
            this ICrudAppService<TEntity, TKey> service, 
            IMemoryCache cache, 
            IMessageDispatcher messageDispatcher) 
            => new CachedCrudAppService<TEntity,TKey>(service, cache, messageDispatcher); 
    }
}