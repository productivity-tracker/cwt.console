﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EventBus.SignalR.Client;

namespace CWT.Console.Common.Extensions
{
    public static class DynamicDataExtensions
    {
        public static SourceCache<TObject, TKey> UpdateOn<TObject, TKey>(
            this SourceCache<TObject, TKey> source, 
            IMessageDispatcher messageDispatcher, 
            IEqualityComparer<TObject> comparer,
            out IDisposable disposable)
        where TObject : IEntityDto<TKey>
        {
            messageDispatcher.On<EntityCreatedEto<TObject>>()
                             .Select(x => x.Entity)
                             .Merge(messageDispatcher.On<EntityUpdatedEto<TObject>>().Select(x => x.Entity));

            var update = messageDispatcher.On<EntityCreatedEto<TObject>>().Select(x => x.Entity)
                                          .Merge(messageDispatcher.On<EntityUpdatedEto<TObject>>().Select(x => x.Entity))
                                          .Subscribe(x => source.AddOrUpdate(x, comparer));

            var delete = messageDispatcher.On<EntityDeletedEto<TObject>>().Subscribe(x => source.RemoveKey(x.Entity.Id));
            disposable = new CompositeDisposable(update, delete);

            return source;
        }
    }
}