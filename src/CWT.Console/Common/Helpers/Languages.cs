﻿using System;
using System.IO;
using System.Reflection;
using TranslateMe;

namespace CWT.Console.Common.Helpers
{
    public static class Languages
    {
        private const string LANG_FILE_NAME = "Languages.tm.json";

        private static readonly string _languagesFilesDirectory = Path.GetFullPath(
            Path.Combine(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? throw new InvalidOperationException(),
                "Translations"
            ));

        public static void Init()
        {
            TM.Instance.LogOutMissingTranslations = true;
            var langFileFileName = Path.Combine(_languagesFilesDirectory, LANG_FILE_NAME);
            var loader = new TMLanguagesLoader(TM.Instance);

            if (!File.Exists(langFileFileName))
            {
                // TODO: 
            }

            loader.AddFile(langFileFileName);
        }
    }
}