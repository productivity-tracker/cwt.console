﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Text;

namespace CWT.Console.Common
{
    public interface ISchedulerProvider
    {
        IScheduler MainThread { get; }
        IScheduler Background { get; }
    }
}
