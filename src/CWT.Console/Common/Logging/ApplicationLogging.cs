﻿using System;
using System.Diagnostics;
using CWT.Console.Common.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace CWT.Console.Common.Logging
{
    public class ApplicationLogging
    {
        private static ILoggerFactory _factory;

        public static ILoggerFactory LoggerFactory => _factory ??= new NullLoggerFactory();

        public static void ConfigureLogger(ILoggerFactory factory)
        {
            _factory = factory;
        }

        public ILogger CreateLogger()
        {
            var frame = new StackFrame(1, false);

            var declaringType = frame.GetMethod().ReflectedType ?? GetType();

            if (declaringType == null)
            {
                throw new ApplicationException("Logger should be instantiated from somewhere...");
            }

            var readableName = declaringType.ReadableName();
            return LoggerFactory.CreateLogger(readableName);
        }

        /// <summary>
        /// Method for getting correct <see cref="T:Microsoft.Extensions.Logging.ILogger" /> for specific logger type
        /// </summary>
        public static ILogger GetLogger<T>() => GetLogger(typeof(T).ReadableName());

        public static ILogger GetLogger(string categoryName) => LoggerFactory.CreateLogger(categoryName);
    }
}