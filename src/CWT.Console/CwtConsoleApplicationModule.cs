﻿using CWT.Console.Common.Helpers;
using Volo.Abp.Modularity;

namespace CWT.Console
{
    public class CwtConsoleApplicationModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            Languages.Init();
        }
    }
}