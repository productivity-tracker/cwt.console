﻿using System.Collections.Generic;
using System.Reactive.Disposables;
using CWT.Console.Common.Extensions;
using Volo.Abp.EventBus.SignalR.Client;
using DynamicData;
using ReactiveUI;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace CWT.Console.Data
{
    internal abstract class DynamicContext<TEntityDto, TKey> : ReactiveObject
        where TEntityDto : EntityDto<TKey>
    {
        protected ICrudAppService<TEntityDto, TKey> RemoteService { get; }
        protected SourceCache<TEntityDto, TKey> Source { get; }
        protected IMessageDispatcher MessageDispatcher { get; }

        protected CompositeDisposable CleanUp { get; } = new CompositeDisposable();

        protected DynamicContext(
            IMessageDispatcher messageDispatcher, 
            ICrudAppService<TEntityDto, TKey> remoteService, 
            IEqualityComparer<TEntityDto> comparer)
        {
            MessageDispatcher = messageDispatcher;
            RemoteService = remoteService;
            Source = new SourceCache<TEntityDto, TKey>(x => x.Id).UpdateOn(
                MessageDispatcher, 
                comparer, 
                out var updateDisposable);
            
            updateDisposable.DisposeWith(CleanUp);
        }
    }
}