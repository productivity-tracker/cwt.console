﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EventBus.SignalR.Client;

namespace CWT.Console.Data.Network
{
    public class CachedCrudAppService<TEntityDto, TKey> : ICrudAppService<TEntityDto, TKey>, IDisposable
    {
        private readonly ICrudAppService<TEntityDto, TKey> _service;
        private readonly IMemoryCache _cache;
        private readonly MemoryCacheEntryOptions _cacheOptions = new MemoryCacheEntryOptions();

        private readonly IDisposable _disposables;

        public CachedCrudAppService(
            ICrudAppService<TEntityDto, TKey> service,
            IMemoryCache cache,
            IMessageDispatcher messageDispatcher)
        {
            _service = service;
            _cache = cache;

            var removeHandler = messageDispatcher.On<EntityDeletedEto<TEntityDto>>()
                                                 .Select(x => x.Entity)
                                                 .Do(x => _cache.Remove(GetCacheKey(x)))
                                                 .Subscribe();

            var updateHandler = messageDispatcher.On<EntityUpdatedEto<TEntityDto>>()
                                                 .Select(x => x.Entity)
                                                 .Do(x => _cache.Set(GetCacheKey(x), x))
                                                 .Subscribe();

            _disposables = new CompositeDisposable(removeHandler, updateHandler);
        }

        public void Dispose() => _disposables?.Dispose();

        public async Task<TEntityDto> GetAsync(TKey id)
        {
            var cacheKey = GetCacheKey(id);
            return _cache.TryGetValue(cacheKey, out TEntityDto value)
                ? value
                : _cache.Set(cacheKey, await _service.GetAsync(id), _cacheOptions);
        }

        public async Task<PagedResultDto<TEntityDto>> GetListAsync(PagedAndSortedResultRequestDto request)
        {
            var result = await _service.GetListAsync(request);
            foreach (var resultItem in result.Items)
            {
                var cacheKey = GetCacheKey(resultItem);
                _cache.Set(cacheKey, resultItem, _cacheOptions);
            }

            return result;
        }

        public async Task<TEntityDto> CreateAsync(TEntityDto input)
        {
            var cacheKey = GetCacheKey(input);
            return _cache.TryGetValue(cacheKey, out TEntityDto value)
                ? value
                : _cache.Set(cacheKey, await _service.CreateAsync(input), _cacheOptions);
        }

        public async Task<TEntityDto> UpdateAsync(TKey id, TEntityDto input)
        {
            var cacheKey = GetCacheKey(id);
            return _cache.TryGetValue(cacheKey, out TEntityDto value)
                ? value
                : _cache.Set(cacheKey, await _service.UpdateAsync(id, input), _cacheOptions);
        }

        public async Task DeleteAsync(TKey id)
        {
            _cache.Remove(GetCacheKey(id));
            await _service.DeleteAsync(id);
        }

        private string GetCacheKey(TKey id) => $"{typeof(TEntityDto).Name}:{id}";
        private string GetCacheKey(TEntityDto entity) => $"{typeof(TEntityDto).Name}:{((IEntityDto<TKey>)entity).Id}";
    }
}