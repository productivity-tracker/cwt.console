﻿namespace CWT.Console.Domain.Entities
{
    public class DateInfo
    {
        public Timing.Date Date { get; set; }
        public string DisplayText { get; set; }
    }
}