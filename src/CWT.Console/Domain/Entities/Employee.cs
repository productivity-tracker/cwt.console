﻿using System.Collections.Generic;

namespace CWT.Console.Domain.Entities
{
    public class Employee
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public bool IsOnline { get; set; }
        public string[] OrganizationUnitIDs { get; set; }
    }
}