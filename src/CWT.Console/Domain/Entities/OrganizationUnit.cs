﻿namespace CWT.Console.Domain.Entities
{
    public class OrganizationUnit
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}