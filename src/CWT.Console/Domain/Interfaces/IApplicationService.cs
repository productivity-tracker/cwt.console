﻿namespace CWT.Console.Domain.Interfaces
{
    public interface IApplicationService
    {
        string LogFolder { get; }

        void CopyToClipboard(string text);
        void Exit();
        void Restart();
        void OpenFolder(string folder);
        void SignIn();
        void LogOn();
        void LogOut();
    }
}