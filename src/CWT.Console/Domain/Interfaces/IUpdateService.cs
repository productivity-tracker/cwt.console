﻿using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace CWT.Console.Domain.Interfaces
{
    public interface IUpdateService : IHostedService
    {
        bool IsAppDeployed();
    }
}