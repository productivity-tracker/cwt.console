﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace CWT.Console.Domain.Interfaces.Repositories
{
    public interface IRepository<TEntity, in TKey>
        where TEntity : Entity<TKey>
    {
        Task DeleteAsync(TKey id);

        Task<TEntity> FindAsync(TKey id);

        Task<TEntity> GetAsync(TKey id);

        Task<long> GetCountAsync(CancellationToken cancellationToken = default);

        Task<IPagedAndSortedResultRequest> GetListAsync(IPagedAndSortedResultRequest request);

        Task<TEntity> InsertAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);
    }
}