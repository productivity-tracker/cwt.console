﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using CWT.Console.Common;
using CWT.Console.Domain.Entities;
using CWT.Console.Presentation.Common.GridDashboard.MappedValue;
using CWT.Console.Presentation.Common.GridDashboard.TopBar;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI.Fody.Helpers;

namespace CWT.Console.Presentation.Common.GridDashboard
{
    public abstract class GridDashboardViewModel : TopBarViewModel
    {
        private readonly ReadOnlyObservableCollection<Employee> _employees;
        private readonly SourceCache<Employee, string> _sourceEmployees;
        private readonly ReadOnlyObservableCollection<string> _organizationalUnits;

        private readonly CompositeDisposable _cleanUp = new CompositeDisposable();

        [ObservableAsProperty]
        public ReadOnlyObservableCollection<Employee> Employees => _employees;

        [ObservableAsProperty]
        public ObservableCollection<DateInfo> Dates { get; }

        [ObservableAsProperty]
        public MappedValueCollection DataMappings { get; }

        protected GridDashboardViewModel(ISchedulerProvider schedulerProvider)
        {
            DataMappings = new MappedValueCollection();
            DataMappings.CollectionChanged += OnMappingsChanged;

            Dates = new ObservableCollection<DateInfo>();
            _sourceEmployees = new SourceCache<Employee, string>(x => x.Id);

            var employeeFilter = this.WhenValueChanged(x => x.SearchText)
                                 .Throttle(TimeSpan.FromMilliseconds(250))
                                 .Select(BuildEmployeeNameFilter);

            var OUFilter = this.WhenValueChanged(x => x.SelectedOU)
                                     .Throttle(TimeSpan.FromMilliseconds(250))
                                     .Select(BuildOrgUnitFilter);

            _sourceEmployees.Connect()
                            .Filter(employeeFilter)
                            .Filter(OUFilter)
                            .ObserveOn(schedulerProvider.MainThread)
                            .Bind(out _employees)
                            .DisposeMany()
                            .Subscribe()
                            .DisposeWith(_cleanUp);

             _sourceEmployees.Connect()
                             .DistinctValues(x => "Unknown")
                             .ObserveOn(schedulerProvider.MainThread)
                             .Bind(out _organizationalUnits)
                             .DisposeMany()
                             .Subscribe()
                             .DisposeWith(_cleanUp);
        }

        private void OnMappingsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private Func<Employee, bool> BuildEmployeeNameFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return employee => true;
            }

            return employee => employee.DisplayName.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }

        private Func<Employee, bool> BuildOrgUnitFilter(string orgUnit)
        {
            if (string.IsNullOrEmpty(orgUnit))
            {
                return employee => true;
            }

            return employee => employee.OrganizationUnitIDs.Count(ou => ou.Contains(orgUnit, StringComparison.OrdinalIgnoreCase)) > 0;
        }
    }
}