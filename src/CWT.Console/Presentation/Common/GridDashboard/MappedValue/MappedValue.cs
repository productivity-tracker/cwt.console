﻿using DynamicData.Binding;

namespace CWT.Console.Presentation.Common.GridDashboard.MappedValue
{
    public class MappedValue : AbstractNotifyPropertyChanged, IMappedValue<object, object, object>
    {
        private object _value;
        public object ColumnBinding { get; set; }
        public object RowBinding { get; set; }

        public object Value
        {
            get => _value;
            set => SetAndRaise(ref _value, value);
        }
    }
}