﻿using System.Collections.ObjectModel;
using System.Reactive;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace CWT.Console.Presentation.Common.GridDashboard.TopBar
{
    public class TopBarViewModel : ReactiveObject
    {
        [ObservableAsProperty]
        public ReadOnlyObservableCollection<string> OrganizationUnits { get; }

        [Reactive]
        public string SearchText { get; set; }

        [Reactive]
        public string SelectedOU { get; set; }

        public ReactiveCommand<Unit,Unit> ClearFilters { get; set; }

        public TopBarViewModel()
        {
            ClearFilters = ReactiveCommand.Create(
                () =>
                {
                    SearchText = null;
                    SelectedOU = null;
                },
                this.WhenAnyValue(
                    x => x.SearchText, 
                    x => x.SelectedOU,
                    (searchEmployee, selectedOU) => !string.IsNullOrWhiteSpace(searchEmployee) ||
                                                            !string.IsNullOrWhiteSpace(selectedOU)));
        }
    }
}