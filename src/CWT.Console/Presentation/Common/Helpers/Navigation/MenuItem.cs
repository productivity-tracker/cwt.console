﻿using System.Collections.Generic;
using System.Reactive;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace CWT.Console.Presentation.Common.Helpers.Navigation
{
    public class MenuItem : ReactiveObject
    {
        [Reactive] public string Icon { get; set; }

        [Reactive] public bool IsChecked { get; set; }

        [Reactive] public string Caption { get; set; }

        [Reactive] public string Description { get; set; }

        [Reactive] public object Content { get; set; }

        [Reactive]  public IList<MenuItem> MenuItems { get; set; }

        [Reactive] public ReactiveCommand<Unit, Unit> Command { get; set; }

        public MenuItem() { }

        public MenuItem(string caption, ReactiveCommand<Unit, Unit> command, object content = null)
        {
            Caption = caption;
            Content = content;
            Command = command;
        }
    }
}