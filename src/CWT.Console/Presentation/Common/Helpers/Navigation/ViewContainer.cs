﻿using System;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace CWT.Console.Presentation.Common.Helpers.Navigation
{
    public class ViewContainer : ReactiveObject
    {
        public string Id { get; }

        [ObservableAsProperty]
        public string Title { get; }

        [ObservableAsProperty]
        public object Content { get; }

        public ViewContainer(string title, object content)
        {
            Id = Guid.NewGuid().ToString("N");
            Title = title;
            Content = content;
        }

        public ViewContainer(string id, string title, object content) : this(title, content)
        {
            Id = id;
            Title = title;
            Content = content;
        }
    }
}