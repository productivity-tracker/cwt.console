﻿using System;
using System.Reactive;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace CWT.Console.Presentation.Login
{
    public class LoginViewModel : ReactiveObject
    {
        private const int MAX_PASSWORD_LENGTH = 128;
        private const int MIN_PASSWORD_LENGTH = 6;
        
        [Reactive]
        public string UserName { get; set; }

        [Reactive]
        public string Password { get; set; }

        [Reactive]
        public bool RememberUsername { get; set; }

        public ReactiveCommand<Unit, bool> TrySignIn { get; }

        public LoginViewModel(ILogger<LoginViewModel> logger)
        {
            TrySignIn = ReactiveCommand.CreateFromTask(
                TryAuthenticate, 
                this.WhenAnyValue(
                    x => x.UserName,
                    x => x.Password,
                    (userName, password) => 
                        !string.IsNullOrWhiteSpace(userName) 
                     && !string.IsNullOrWhiteSpace(password) 
                     && password.Length.IsBetween(MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)));
            TrySignIn.ThrownExceptions.Subscribe(error => logger.LogError(error, "Authorization Failed."));
        }

        private async Task<bool> TryAuthenticate()
        { 
           await Task.Delay(100);
           return true;
        }
    }
}