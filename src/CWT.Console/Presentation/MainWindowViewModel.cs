﻿using ReactiveUI;

namespace CWT.Console.Presentation
{
    public class MainWindowViewModel : ReactiveObject
    {
        public string Title { get; set; } = "CWT Console";
    }
}