﻿using System;
using System.Reactive;
using CWT.Console.Data;
using CWT.Console.Presentation.Common.GridDashboard.MappedValue;
using EmployeeManagement.Employees.DTO;
using EmployeeManagement.OrganizationUnits.DTO;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using TimelineTracking.Timelines;
using TimelineTracking.Timelines.DTO;

namespace CWT.Console.Presentation.Timeline
{
    public class EmployeesTimelineGridViewModel : ReactiveObject
    {
        private readonly DynamicContext<EmployeeDto, Guid> _employeeContext;
        private readonly DynamicContext<OrganizationUnitDto, string> _ouContext;
        private readonly ITimelineService _timelineService;

        [Reactive]
        public string SearchText { get; set; }

        [Reactive]
        public string SelectedOrganizationalUnit { get; set; }

        public MappedValueCollection TimelinesMappings { get; }

        public ReactiveCommand<Unit, Unit> LoadData { get; }

        public EmployeesTimelineGridViewModel(ITimelineService timelineService)
        {
            _timelineService = timelineService;
            TimelinesMappings = new MappedValueCollection();


            LoadData = ReactiveCommand.CreateFromTask(
            async ct =>
            {
                var timelinesDtos = await _timelineService.GetRangeAsync(
                    new DateTimeRangeRequest
                    {
                        StartTime = DateTime.Today,
                        EndTime = DateTime.Today.AddDays(-15)
                    });
            });
        }
    }
}