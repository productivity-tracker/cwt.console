﻿using System.Collections.Generic;
using CWT.Timing;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace CWT.Console.Presentation.Timeline
{
    public class TimelineViewModel : ReactiveObject
    {
        [Reactive]
        public string Title { get; set; }

        [Reactive]
        public Date Date { get; set; }

        [Reactive]
        public IList<DateTimeRange> TimeRanges { get; set; }

        public TimelineViewModel(string title, Date date, IList<DateTimeRange> timeRanges)
        {
            this.Title = title;
            Date = date;
            TimeRanges = timeRanges;
        }
    }
}