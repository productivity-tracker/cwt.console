﻿using Autofac;
using Autofac.Core;
using CWT.Viewer.Extensions;
using CWT.Viewer.Services;
using CWT.Viewer.ViewModels;
using CWT.Viewer.Wpf.Autofac;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CWT.Viewer.Wpf
{
    public partial class App : Application
    {
        private readonly CompositeDisposable _disposable;

        private Bootstrapper _bootstrapper;
        private IExceptionDialogService _exceptionDialogService;
        private ILogger _logger;
        private ISchedulerService _schedulerService;

        public App()
        {
            InitializeComponent();

#if !DEBUG
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            Application.Current.DispatcherUnhandledException += DispatcherOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
#endif
            _disposable = new CompositeDisposable();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            _bootstrapper = new Bootstrapper();
            _bootstrapper.Start();

            _logger = _bootstrapper.Resolve<ILoggerFactory>().CreateLogger(GetType().ReadableName());

            _logger.LogInformation("Starting");
            _logger.LogInformation($"Dispatcher managed thread identifier = {Thread.CurrentThread.ManagedThreadId}");

            _exceptionDialogService = _bootstrapper.Resolve<IExceptionDialogService>();
            _schedulerService = _bootstrapper.Resolve<ISchedulerService>();

            Current.Exit += (s, a) =>
            {
                _logger.LogInformation("Closing App");
                _disposable.Dispose();
                _bootstrapper.Stop();
                LogManager.Flush();
            };
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            _logger.LogError("Unhandled app domain exception");
            HandleException(args.ExceptionObject as Exception);
        }

        private void DispatcherOnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
        {
            _logger.LogError("Unhandled dispatcher thread exception");
            args.Handled = true;

            HandleException(args.Exception);
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs args)
        {
            _logger.LogError("Unhandled task exception");
            args.SetObserved();

            HandleException(args.Exception.GetBaseException());
        }

        private void HandleException(Exception exception)
        {
            _logger.LogError("Exception", exception);

            _schedulerService.Dispatcher.Schedule(
                () =>
                {
                    var parameters = new Parameter[] { new NamedParameter("exception", exception) };
                    var viewModel = _bootstrapper.Resolve<IExceptionViewModel>(parameters);

                    _exceptionDialogService.Show(viewModel);
                });
        }
    }
}
