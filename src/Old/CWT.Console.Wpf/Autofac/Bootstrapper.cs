﻿using Autofac;
using Autofac.Core;
using Autofac.Extras.CommonServiceLocator;
using CommonServiceLocator;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Extensions;
using CWT.Viewer.Infrastructure;
using CWT.Viewer.Models.Settings;
using CWT.Viewer.Services;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels;
using CWT.Viewer.ViewModels.Abstractions;
using CWT.Viewer.ViewModels.ControlPanel.AgentSettings;
using CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList;
using CWT.Viewer.ViewModels.TimeRange;
using CWT.Viewer.Wpf.Autofac.Modules;
using CWT.Viewer.Wpf.Services.Dialogs;
using CWT.Viewer.Wpf.Services.Dialogs.Builders;
using CWT.Viewer.Wpf.Services.Notifications;
using CWT.Viewer.Wpf.Views.Controls.ControlPanel;
using CWT.Viewer.Wpf.Views.Controls.Dialogs;
using CWT.Viewer.Wpf.Views.Controls.Settings;
using DSFramework.Domain.Abstractions.Converters;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;
using CWT.Viewer.Helpers;
using CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces;
using CWT.Viewer.ViewModels.Timeline;
using CWT.Viewer.Wpf.Views.Controls.ControlPanel.OrganizationalUnits;
using CWT.Viewer.Wpf.Views.Controls.Timeline;
using TranslateMe;
using IDialogService = CWT.Viewer.Services.Dialogs.IDialogService;

namespace CWT.Viewer.Wpf.Autofac
{
    public class Bootstrapper
    {
        private IContainer _container;
        private ILogger _logger;

        public Bootstrapper()
        {
            Init();
        }

        public void Init()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new SettingsModule(typeof(SettingsBase).Assembly));
            builder.RegisterModule(new NLoggerModule());
            builder.RegisterModule(new CwtSdkModule("http://localhost:5000/",
                                                    TimeSpan.FromMinutes(5)));

            RegisterAssemblyTypes(builder);

            builder.RegisterInstance(new ExceptionDialogService(GlobalConstants.EXCEPTION_DIALOG_HOST_ID))
                   .As<IExceptionDialogService>()
                   .SingleInstance();

            builder.Register(context => BuildDialogService(context.Resolve<ILoggerFactory>()))
                   .As<IDialogService>()
                   .SingleInstance();

            builder.RegisterType<NotificationMessageQueue>().As<INotificationMessageQueue>();

            builder.RegisterType<ScreenshotsHistoryConverter>().As<IScreenshotHistoryConverter>();
            builder.RegisterType<EmployeeTimelineContext>().As<IEmployeeTimelineContext>();

            _container = builder.Build();
        }

        public void Start()
        {
            SetupLogger();
            SetLocatorProvider();
            LoadLanguage();

            Resolve<IPaletteService>().ApplyCurrentPalette();
            Resolve<ICwtFacade>().Start();
            Resolve<IApplicationService>().SignIn();
            //Resolve<IApplicationService>().LogOn();
        }

        public void Stop()
        {
            Resolve<IApplicationService>().Exit();
            _container?.Dispose();
        }

        public T Resolve<T>()
        {
            if (_container == null)
            {
                throw new Exception("Bootstrapper hasn't been started!");
            }

            return _container.Resolve<T>(new Parameter[0]);
        }

        public T Resolve<T>(Parameter[] parameters)
        {
            if (_container == null)
            {
                throw new Exception("Bootstrapper hasn't been started!");
            }

            return _container.Resolve<T>(parameters);
        }

        private void RegisterAssemblyTypes(ContainerBuilder container)
        {
            var assemblies = new[] { Assembly.GetExecutingAssembly(), typeof(IViewModel).Assembly };

            container.RegisterAssemblyTypes(assemblies)
                     .Where(t => typeof(IViewModel).IsAssignableFrom(t) && !typeof(ITransientViewModel).IsAssignableFrom(t))
                     .AsImplementedInterfaces();

            container.RegisterAssemblyTypes(assemblies)
                     .AsClosedTypesOf(typeof(IConverter<,>));

            container.RegisterAssemblyTypes(assemblies)
                     .AsClosedTypesOf(typeof(IViewModelContext<>));

            container.RegisterAssemblyTypes(assemblies)
                     .Where(t => typeof(IService).IsAssignableFrom(t))
                     .SingleInstance()
                     .AsImplementedInterfaces();
        }

        private void LoadLanguage()
        {
            var language = _container.Resolve<LanguageSettings>().Language;
            TM.Instance.CurrentLanguage = language;
            TryLanguageInit();
        }

        private void SetLocatorProvider()
        {
            var csl = new AutofacServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => csl);
        }

        private void SetupLogger()
        {
            var loggerFactory = Resolve<ILoggerFactory>();
            _logger = loggerFactory.CreateLogger(GetType().ReadableName());
            ApplicationLogging.ConfigureLogger(loggerFactory);
        }

        private IDialogService BuildDialogService(ILoggerFactory loggerFactory)
        {
            var builder = new DialogServiceBuilder(loggerFactory);
            builder.SetMapping<ISettingsViewModel, SettingsView>()
                   .SetMapping<IAgentSettingsEditViewModel, AgentSettingsEditView>()
                   .SetMapping<ICustomTimeRangeViewModel, CustomRangeView>()
                   .SetMapping<ITimelineViewModel, TimelineView>()
                   .SetMapping<IOrganizationalUnitEditViewModel, OrganizationUnitEdit>();

            return builder.Build();
        }

        private void TryLanguageInit()
        {
            try
            {
                Languages.Init();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error while {nameof(TryLanguageInit)}. ", ex);
            }
        }
    }
}