﻿using Autofac;
using CWT.Sdk.Facades;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Infrastructure.Polly;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net;

namespace CWT.Viewer.Wpf.Autofac.Modules
{
    public class CwtSdkModule : Module
    {
        private readonly string _baseUrl;
        private readonly TimeSpan _connectionLeaseTimeout;

        public CwtSdkModule(string baseUrl, TimeSpan connectionLeaseTimeout)
        {
            _baseUrl = baseUrl;
            _connectionLeaseTimeout = connectionLeaseTimeout;
        }

        protected override void Load(ContainerBuilder builder)
        {
            ServicePointManager.FindServicePoint(new Uri(_baseUrl)).ConnectionLeaseTimeout = (int)_connectionLeaseTimeout.TotalMilliseconds;

            builder.RegisterType<CwtFacadeBuilder>()
                   .WithParameter("baseWebApiUrl", _baseUrl)
                   .WithParameter((pi, _) => pi.Name == "application", (_, ctx) => "CWT.Console")
                   .WithParameter("authorityAddress", "localhost")
                   .OnActivating(x =>
                   {
                       x.Instance.SetLogging(x.Context.Resolve<ILoggerFactory>());
                       x.Instance.AddPolicyHandler(HttpResponseMessageHandlerPolicy.Timeout(30));
                       x.Instance.AddPolicyHandler(request => HttpResponseMessageHandlerPolicy.DefaultRetryPolicy(3, request));
                   });

            builder.Register(x => x.Resolve<CwtFacadeBuilder>().Build()).SingleInstance().AsImplementedInterfaces();

            var knownFacadeProperties = typeof(ICwtFacade).GetProperties().Where(x => x.PropertyType.Name.EndsWith("Facade"));

            foreach (var propertyInfo in knownFacadeProperties)
            {
                builder.Register(ctx => propertyInfo.GetValue(ctx.Resolve<ICwtFacade>()))
                       .As(propertyInfo.PropertyType.GetInterfaces())
                       .As(propertyInfo.PropertyType)
                       .SingleInstance();
            }
        }
    }
}