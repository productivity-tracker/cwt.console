﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Core;
using CWT.Viewer.Extensions;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CWT.Viewer.Wpf.Autofac.Modules
{
    public class NLoggerModule : Module
    {
        private readonly NLogLoggerProvider _provider;

        public NLoggerModule()
        {
            _provider = new NLogLoggerProvider();
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(new LoggerFactory(new []{_provider}))
                   .As<ILoggerFactory>()
                   .SingleInstance();

            builder.Register(CreateLogger).AsImplementedInterfaces();
        }

        private ILogger CreateLogger(IComponentContext c, IEnumerable<Parameter> p)
        {
            var logger = _provider.CreateLogger(p.TypedAs<Type>().ReadableName());
            return logger;
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            registration.Preparing += Registration_Preparing;
        }

        private void Registration_Preparing(object sender, PreparingEventArgs args)
        {
            var forType = args.Component.Activator.LimitType;

            var logParameter = new ResolvedParameter(
                (p, c) => p.ParameterType == typeof(ILogger),
                (p, c) => c.Resolve<ILogger>(TypedParameter.From(forType)));

            args.Parameters = args.Parameters.Union(new[] { logParameter });
        }
    }
}