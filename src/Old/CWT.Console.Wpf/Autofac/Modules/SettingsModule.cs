﻿using Autofac;
using CWT.Viewer.Services;
using CWT.Viewer.Wpf.Services;
using System;
using System.Linq;
using System.Reflection;
using Module = Autofac.Module;

namespace CWT.Viewer.Wpf.Autofac.Modules
{
    public class SettingsModule : Module
    {
        private readonly string _sectionNameSuffix;
        private readonly Assembly _assemblyWithSettings;

        public SettingsModule(Assembly assemblyWithSettings, string sectionNameSuffix = "Settings")
        {
            _sectionNameSuffix = sectionNameSuffix;
            _assemblyWithSettings = assemblyWithSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(new LocalSettingsService())
                   .As<ISettingsService>()
                   .SingleInstance();

            var settings = _assemblyWithSettings
                           .GetTypes()
                           .Where(t => t.Name.EndsWith(_sectionNameSuffix, StringComparison.InvariantCulture))
                           .ToList();

            settings.ForEach(type =>
                {
                    builder.Register(c => c.Resolve<ISettingsService>().LoadSettings(type, c.Resolve<ISettingsService>()))
                           .As(type)
                           .SingleInstance();
                });
        }
    }
}