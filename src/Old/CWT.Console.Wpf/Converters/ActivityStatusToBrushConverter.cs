﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CWT.Viewer.Wpf.Converters
{
    public class ActivityStatusToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isOnline)
            {
                return isOnline ? Brushes.MediumSeaGreen : Brushes.IndianRed;
            }

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}