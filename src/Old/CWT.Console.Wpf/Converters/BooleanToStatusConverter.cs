﻿using System;
using System.Globalization;
using System.Windows.Data;
using TranslateMe;

namespace CWT.Viewer.Wpf.Converters
{
    public class BooleanToStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value is bool isOnline)
            {
                return isOnline ? TM.Tr("Online") : TM.Tr("Offline");
            }

            return TM.Tr("Unknown");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}