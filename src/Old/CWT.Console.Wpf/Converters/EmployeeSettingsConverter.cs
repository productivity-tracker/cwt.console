﻿using CommonServiceLocator;
using CWT.Sdk.Facades.Interfaces;
using DSFramework.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;
using System.Windows.Data;

namespace CWT.Viewer.Wpf.Converters
{
    public class EmployeeSettingsConverter : IValueConverter
    {
        private const string UNKNOWN = "Unknown";
        private readonly IAgentSettingsFacade _settingsFacade;

        public EmployeeSettingsConverter()
        {
            _settingsFacade = ServiceLocator.Current.GetRequiredService<IAgentSettingsFacade>();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string settingsName = null;
            if (value is string settingsId)
            {
                var holder = _settingsFacade.Get(settingsId).GetAwaiter().GetResult();
                if (holder == null)
                {
                    return UNKNOWN;
                }

                settingsName = holder.Data?.Name;
            }

            return settingsName.IsNullOrWhiteSpace() ? UNKNOWN : settingsName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}