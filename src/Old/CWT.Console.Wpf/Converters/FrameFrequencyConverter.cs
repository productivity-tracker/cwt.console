﻿using CWT.Viewer.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace CWT.Viewer.Wpf.Converters
{
    public class FrameFrequencyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan timeSpan && parameter is FrameFrequency frequency)
            {
                return frequency.GetFrameCount(timeSpan);
            }

            return default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter is FrameFrequency frequency)
            {
                if (value != null && int.TryParse(value.ToString(), out var interval))
                {
                    return frequency.GetSleepDuration(interval);
                }
            }

            return default;
        }
    }
}