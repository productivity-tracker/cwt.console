﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace CWT.Viewer.Wpf.Converters
{
    [ContentProperty("Converter")]
    public class MultiValueConverterAdapter : IMultiValueConverter
    {
        private object lastParameter;
        public IValueConverter Converter { get; set; }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (Converter == null)
                return values[0]; // Required for VS design-time
            if (values.Length > 1)
                lastParameter = values[1];
            var result = Converter.Convert(values[0], targetType, lastParameter, culture);
            return result?.ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (Converter == null)
                return new[] { value }; // Required for VS design-time
            var result = new[] { Converter.ConvertBack(value, targetTypes[0], lastParameter, culture) };
            return result;
        }
    }
}