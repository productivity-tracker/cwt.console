﻿using CWT.Viewer.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace CWT.Viewer.Wpf.Converters
{
    public class PeriodConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan timeSpan)
            {
                var period = timeSpan.GetPeriod();

                switch (period)
                {
                    case Period.Day: return timeSpan.TotalDays;
                    case Period.Hour: return timeSpan.TotalHours;
                    case Period.Minute: return timeSpan.TotalMinutes;
                    case Period.Second: return timeSpan.TotalSeconds;
                }
            }

            return default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter is Period period)
            {
                if (value != null && int.TryParse(value.ToString(), out var interval))
                {
                    return period.GetTimeSpan(interval);
                }
            }

            return default;
        }

    }
}