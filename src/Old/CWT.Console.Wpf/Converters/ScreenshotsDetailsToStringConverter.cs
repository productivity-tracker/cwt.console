﻿using System;
using System.Globalization;
using System.Windows.Data;
using CWT.Viewer.Models.ScreenshotsHistory;

namespace CWT.Viewer.Wpf.Converters
{
    public class ScreenshotsDetailsToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is ScreenshotsDetailsModel model && model.TimeSpan != default(TimeSpan))
            {
                return model.TimeSpan.ToString("g");
            }

            return "0";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}