﻿using System;
using System.Globalization;
using System.Windows.Data;
using TranslateMe.WPF;

namespace CWT.Viewer.Wpf.Converters
{
    public class TrTextIdToUpperConverter : TrTextIdConverter, IValueConverter
    {
        public new object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = base.Convert(value, targetType, parameter, culture) as string;
            return result?.ToUpper();
        }
    }
}