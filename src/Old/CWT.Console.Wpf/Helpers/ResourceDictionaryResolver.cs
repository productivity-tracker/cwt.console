﻿using System;
using System.Windows;

namespace CWT.Viewer.Wpf.Helpers
{
    public static class ResourceDictionaryResolver
    {
        public static ResourceDictionary GetResourceDictionary(string uri)
        {
            ResourceDictionary resourceDictionary = null;
            foreach (ResourceDictionary resourceDictionaryScan in Application.Current.Resources.MergedDictionaries)
            {
                if (resourceDictionaryScan.Source == new Uri(uri, UriKind.RelativeOrAbsolute))
                {
                    resourceDictionary = resourceDictionaryScan;
                    break;
                }
            }

            return resourceDictionary;
        }
    }
}