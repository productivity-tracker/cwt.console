﻿using System.Windows.Interactivity;

namespace CWT.Viewer.Wpf.Infrastructure.InteractionRequest
{
    public class InteractionRequestTrigger : EventTrigger
    {
        /// <summary>
        /// Gets the name of the event.
        /// </summary>
        /// <returns>The event name.</returns>
        protected override string GetEventName()
        {
            return "Raised";
        }
    }
}