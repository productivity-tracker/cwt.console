﻿using System;
using System.Windows;
using System.Windows.Interactivity;
using CWT.Viewer.Infrastructure.InteractionRequest;

namespace CWT.Viewer.Wpf.Infrastructure.InteractionRequest
{
    public abstract class TriggerActionBase<T> : TriggerAction<FrameworkElement> where T : class, INotification
    {
        private T _notification;

        private Action _callback;

        protected T Notification => this._notification;

        protected Action Callback => this._callback;

        protected override void Invoke(object parameter)
        {
            if (parameter is InteractionRequestedEventArgs interactionRequestedEventArgs)
            {
                this._notification = interactionRequestedEventArgs.Context as T;
                this._callback = interactionRequestedEventArgs.Callback;
                if (this._notification != null)
                {
                    this.ExecuteAction();
                }
            }
        }

        protected abstract void ExecuteAction();
    }
}