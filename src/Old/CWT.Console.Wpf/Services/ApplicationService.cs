﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using CWT.Viewer.Services;
using CWT.Viewer.Services.Auth;
using CWT.Viewer.Wpf.Views;
using NLog;
using NLog.Layouts;
using NLog.Targets;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CWT.Viewer.Wpf.Services
{
    public sealed class ApplicationService : IApplicationService, IDisposable
    {
        private readonly SigninWindow _signinWindow = new SigninWindow();

        private readonly IAuthenticationService _authenticationService;
        private readonly ILogger _logger;

        private string _logFolder;
        private MainWindow _mainWindow;

        public string LogFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(_logFolder))
                {
                    return _logFolder;
                }

                _logFolder = GetLogFolder();
                return _logFolder;
            }
        }

        public ApplicationService(ILogger logger, IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
            _logger = logger;

            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            _authenticationService.LoginSuccessful += LogOn;
        }

        public void Dispose() => _authenticationService.LoginSuccessful -= LogOn;

        public void CopyToClipboard(string text) => Clipboard.SetText(text);

        public void Exit() => Application.Current.Shutdown();

        public void Restart()
        {
            Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        public void OpenFolder(string folder) => Process.Start(fileName: "explorer.exe", folder);

        public void SignIn()
        {
            Application.Current.MainWindow = _signinWindow;
            Application.Current.MainWindow?.Show();
        }

        public void LogOn()
        {
            _mainWindow = new MainWindow();
            Application.Current.MainWindow = _mainWindow;
            Application.Current.MainWindow?.Show();
            _signinWindow.Close();
        }

        public void LogOut()
        {
            SignIn();
            _mainWindow.Close();
        }

        private static string GetLogFolder()
        {
            var logFilePath = LogManager.Configuration.AllTargets.OfType<FileTarget>()
                                        .Select(selector: x => x.FileName as SimpleLayout)
                                        .Select(selector: x => x.Text)
                                        .FirstOrDefault();

            var baseDir = Assembly.GetExecutingAssembly().Location;
            var logFile = logFilePath?.Replace(oldValue: "{basedir}", baseDir);

            return Path.GetDirectoryName(logFile);
        }
    }
}