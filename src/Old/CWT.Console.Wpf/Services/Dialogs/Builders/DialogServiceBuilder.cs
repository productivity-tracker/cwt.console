﻿using CWT.Viewer.Models;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.ViewModels;
using DSFramework.Extensions;
using DSFramework.GuardToolkit;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace CWT.Viewer.Wpf.Services.Dialogs.Builders
{
    public class DialogServiceBuilder : IDialogServiceBuilder
    {
        private readonly IDictionary<Type, Type> _mappings;
        private readonly ILoggerFactory _loggerFactory;

        public DialogServiceBuilder(ILoggerFactory loggerFactory)
        {
            _loggerFactory = Check.NotNull(loggerFactory, nameof(loggerFactory));
            _mappings = new ConcurrentDictionary<Type, Type>();
        }

        public IDialogServiceBuilder SetMapping<TViewModel, TView>()
            where TViewModel : ICloseableViewModel where TView : IDialog
        {
            if (_mappings.ContainsKey(typeof(TViewModel)))
            {
                throw new ArgumentException($"Type {typeof(TViewModel)} is already mapped to type {typeof(TView)}");
            }

            _mappings.Add(typeof(TViewModel), typeof(TView));

            return this;
        }

        public IDialogService Build()
        {
            var dialogService = new DialogService(_loggerFactory.CreateLogger(typeof(DialogService).ReadableName()), _mappings);
            return dialogService;
        }
    }
}