﻿using CWT.Viewer.Models;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.ViewModels;

namespace CWT.Viewer.Wpf.Services.Dialogs.Builders
{
    public interface IDialogServiceBuilder
    {
        IDialogServiceBuilder SetMapping<TViewModel, TView>()
            where TViewModel : ICloseableViewModel where TView : IDialog;

        IDialogService Build();
    }
}