﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using CWT.Viewer.Models;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.ViewModels;
using CWT.Viewer.Wpf.Views.Controls.Dialogs;
using MaterialDesignThemes.Wpf;
using Microsoft.Extensions.Logging;

namespace CWT.Viewer.Wpf.Services.Dialogs
{
    public class DialogService : IDialogService
    {
        private readonly IDictionary<string, DialogSession> _sessions = new ConcurrentDictionary<string, DialogSession>();
        private readonly IDictionary<Type, Type> _mappings;

        private readonly ILogger _logger;

        public DialogService(ILogger logger, IDictionary<Type, Type> mappings)
        {
            _logger = logger;
            _mappings = mappings;
        }

        public async Task<object> Show<TViewModel>(IDialogIdentifier identifier,
                                                   TViewModel viewModel,
                                                   Action afterOpenedCallback,
                                                   Action afterClosedCallback)
            where TViewModel : IViewModel
        {
            var dialog = Map(viewModel);
            return await BaseShow(identifier, dialog, afterOpenedCallback, afterClosedCallback);
        }

        public async Task<MessageBoxButtons> ShowMessageBox<TViewModel>(IDialogIdentifier identifier,
                                                                        string title,
                                                                        TViewModel viewModel,
                                                                        MessageBoxButtons buttons)
            where TViewModel : IViewModel
        {
            var dialog = Map(viewModel);
            return (MessageBoxButtons) await BaseShow(identifier, new MaterialMessageBox(title, dialog, buttons), null, null);
        }

        public void UpdateContent<TViewModel>(IDialogIdentifier identifier, TViewModel viewModel)
            where TViewModel : IViewModel
        {
            var id = identifier.Identifier;
            if (!_sessions.ContainsKey(identifier.Identifier))
            {
                _logger.LogWarning($"Not found session with id: {id}");
                return;
            }

            _sessions[identifier.Identifier].UpdateContent(viewModel);
        }

        #region Show methods

        public async Task<TResult> Show<TViewModel, TResult>(IDialogIdentifier identifier, TViewModel viewModel)
            where TViewModel : IViewModel
        {
            return (TResult)await Show(identifier, viewModel);
        }

        public async Task<object> Show<TViewModel>(IDialogIdentifier identifier, TViewModel viewModel)
            where TViewModel : IViewModel
        {
            return await Show(identifier, viewModel, null, null);
        }
        
        #endregion

        private async Task<object> BaseShow(IDialogIdentifier identifier, IDialog content, Action afterOpenedCallback, Action afterClosedCallback)
        {
            if (_sessions.ContainsKey(identifier.Identifier))
            {
                return null;
            }

            return await DialogHost.Show(content,
                                         identifier.Identifier,
                                         (_, e) =>
                                         {
                                             _sessions.Add(identifier.Identifier, e.Session);
                                             afterOpenedCallback?.Invoke();
                                         },
                                         (_, e) =>
                                         {
                                             _sessions.Remove(identifier.Identifier);
                                             afterClosedCallback?.Invoke();
                                         });
        }

        private IDialog Map<TViewModel>(TViewModel viewModel)
        {
            if (!_mappings.TryGetValue(typeof(TViewModel), out var viewType))
            {
                var msg = $"Not exist mapping for type: {typeof(TViewModel)}";
#if DEBUG
                throw new ArgumentException(msg);
#endif
                _logger.LogWarning(msg);
                return null;
            }

            var dialog = (IDialog)Activator.CreateInstance(viewType);
            dialog.DataContext = viewModel;

            return dialog;
        }
    }
}