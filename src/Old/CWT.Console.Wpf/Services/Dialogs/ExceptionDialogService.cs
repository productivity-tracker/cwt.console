﻿using CWT.Viewer.Services;
using CWT.Viewer.ViewModels;
using CWT.Viewer.Wpf.Views.Controls.Dialogs;
using MaterialDesignThemes.Wpf;

namespace CWT.Viewer.Wpf.Services.Dialogs
{
    public class ExceptionDialogService : IExceptionDialogService
    {
        private readonly string _identifier;
        private bool _isOpen;

        public ExceptionDialogService(string identifier)
        {
            _identifier = identifier;
        }

        public async void Show(IExceptionViewModel viewModel)
        {
            if (_isOpen)
            {
                return;
            }

            var view = new ExceptionDialog { DataContext = viewModel };
            _isOpen = true;
            await  DialogHost.Show(view, _identifier, ClosingHandler);
        }

        private void ClosingHandler(object sender, DialogClosingEventArgs args)
        {
            _isOpen = false;
        }
    }
}