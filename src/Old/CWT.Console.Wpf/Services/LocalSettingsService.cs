﻿using System;
using System.Linq;
using CWT.Viewer.Services;
using NLog;
using TranslateMe;

namespace CWT.Viewer.Wpf.Services
{
    public class LocalSettingsService : ISettingsService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void SetProperty<T>(string propertyName, T value)
        {
            Settings.Default[propertyName] = value;
            Settings.Default.Save();
        }

        public T GetProperty<T>(string propertyName)
        {
            var result = default(T);

            try
            {
                result = (T)Settings.Default[propertyName];
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

            return result;
        }

        public object LoadSettings(Type type, ISettingsService settingsService)
        {
            try
            {
                var settingsInstance = Activator.CreateInstance(type, settingsService);
                var properties = settingsInstance.GetType().GetProperties().ToList();
                properties.ForEach(propInfo => propInfo.SetValue(settingsInstance, GetProperty<dynamic>(propInfo.Name)));

                return settingsInstance;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw new ArgumentNullException(string.Format(TM.Tr("ErrorWhileLoadSettings"), type.Name),e);
            }
        }
    }
}