﻿using CWT.Viewer.Services;
using CWT.Viewer.Wpf.Theme;
using MaterialDesignColors;
using System.Collections.Generic;

namespace CWT.Viewer.Wpf.Services
{
    public class PaletteService : IPaletteService
    {

        public IEnumerable<Swatch> Swatches => new CustomSwatchesProvider().Swatches;

        public void ChangeBackground(bool isDark)
        {
            var helper = new PaletteHelper();
            helper.SetLightDark(isDark);
        }

        public void ApplyPalette(Swatch swatch)
        {
            var helper = new PaletteHelper();
            helper.ReplacePrimaryColor(swatch);
            helper.ReplaceAccentColor(swatch);
        }

        public void ApplyCurrentPalette()
        {
            var paletteName = Settings.Default.ThemeColor;
            var isDark = Settings.Default.ThemeIsDark;
            var helper = new PaletteHelper();
            helper.ReplacePrimaryColor(paletteName);
            helper.ReplaceAccentColor(paletteName);
            helper.SetLightDark(isDark);
        }
    }
}