﻿using CommonServiceLocator;
using CWT.Viewer.Extensions;
using CWT.Viewer.ViewModels;
using CWT.Viewer.ViewModels.ControlPanel;
using CWT.Viewer.ViewModels.ControlPanel.AgentSettings;
using CWT.Viewer.ViewModels.ControlPanel.Devices.Interfaces;
using CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces;
using CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces;
using CWT.Viewer.ViewModels.ControlPanel.UserAgents.Interfaces;
using CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList;
using CWT.Viewer.ViewModels.Timeline;
using DSFramework.GuardToolkit;

namespace CWT.Viewer.Wpf.Services
{
    public class ViewModelLocator
    {
        private readonly IServiceLocator _serviceLocator;

        public ISignInViewModel SignInViewModel => _serviceLocator.GetInstance<ISignInViewModel>().RequireNotNull();
        public IMainViewModel MainViewModel => _serviceLocator.GetInstance<IMainViewModel>().RequireNotNull();
        public IPaletteViewModel PaletteViewModel => _serviceLocator.GetInstance<IPaletteViewModel>().RequireNotNull();
        public ILanguagesViewModel LanguagesViewModel => _serviceLocator.GetInstance<ILanguagesViewModel>().RequireNotNull();
        public ISettingsViewModel SettingsViewModel => _serviceLocator.GetInstance<ISettingsViewModel>().RequireNotNull();
        public IScreenshotsHistoryViewModel ScreenshotsHistoryViewModel => _serviceLocator.GetInstance<IScreenshotsHistoryViewModel>().RequireNotNull();

        public IControlPanelViewModel ControlPanelViewModel => _serviceLocator.GetInstance<IControlPanelViewModel>().RequireNotNull();
        public IAgentSettingsViewModel AgentSettingsViewModel => _serviceLocator.GetInstance<IAgentSettingsViewModel>().RequireNotNull();
        public IEmployeesViewModel EmployeesViewModel => _serviceLocator.GetInstance<IEmployeesViewModel>().RequireNotNull();
        public IDevicesViewModel DevicesViewModel => _serviceLocator.GetInstance<IDevicesViewModel>().RequireNotNull();
        public IUserAgentsViewModel UserAgentsViewModel => _serviceLocator.GetInstance<IUserAgentsViewModel>().RequireNotNull();
        public IEmployeesTimelineViewModel EmployeesTimelineViewModel => _serviceLocator.GetInstance<IEmployeesTimelineViewModel>().RequireNotNull();
        public IOrganizationalUnitViewModel OrganizationalUnitViewModel => _serviceLocator.GetInstance<IOrganizationalUnitViewModel>().RequireNotNull();

        public ViewModelLocator()
        {
            _serviceLocator = Check.NotNull(ServiceLocator.Current, nameof(ServiceLocator.Current));
        }
    }
}