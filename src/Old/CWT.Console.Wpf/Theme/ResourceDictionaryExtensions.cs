﻿using System;
using System.Windows;
using System.Windows.Media;

namespace CWT.Viewer.Wpf.Theme
{
    public static class ResourceDictionaryExtensions
    {
        public static Color GetThemeColor(this ResourceDictionary resourceDictionary, params string[] keys)
        {
            Color result = GetColor(keys);

            Color GetColor(params string[] keys)
            {
                foreach (string key in keys)
                {
                    if (TryGetColor(key, out Color color))
                    {
                        return color;
                    }
                }
                throw new InvalidOperationException($"Could not locate required resource with key(s) '{string.Join(", ", keys)}'");
            }

            bool TryGetColor(string key, out Color color)
            {
                if (resourceDictionary[key] is SolidColorBrush brush)
                {
                    color = brush.Color;
                    return true;
                }
                color = default;
                return false;
            }

            return result;
        }
    }
}
