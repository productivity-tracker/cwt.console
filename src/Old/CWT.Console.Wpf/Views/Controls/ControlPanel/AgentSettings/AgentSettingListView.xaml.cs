﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace CWT.Viewer.Wpf.Views.Controls.ControlPanel.AgentSettings
{
    public partial class AgentSettingListView : UserControl
    {
        public AgentSettingListView()
        {
            InitializeComponent();
        }

        private void ShowHideRowDetails(object sender, RoutedEventArgs e)
        {
            if (!(sender is ToggleButton expandCollapseButton))
            {
                return;
            }

            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
            {
                if (vis is DataGridRow row)
                {
                    if (row.DetailsVisibility == Visibility.Visible)
                    {
                        row.DetailsVisibility = Visibility.Collapsed;
                        expandCollapseButton.IsChecked = false;
                    }
                    else
                    {
                        row.DetailsVisibility = Visibility.Visible;
                        expandCollapseButton.IsChecked = true;
                    }

                    break;
                }
            }
        }
    }
}