﻿using System;
using System.Windows;
using System.Windows.Controls;
using CWT.Viewer.ViewModels;
using CWT.Viewer.ViewModels.ControlPanel;
using CWT.Viewer.Wpf.Views.Controls.ControlPanel.Devices;
using CWT.Viewer.Wpf.Views.Controls.ControlPanel.Employees;
using CWT.Viewer.Wpf.Views.Controls.ControlPanel.OrganizationalUnits;
using CWT.Viewer.Wpf.Views.Controls.ControlPanel.UserAgents;
using TranslateMe;

namespace CWT.Viewer.Wpf.Views.Controls.ControlPanel
{
    public partial class ControlPanelView : UserControl
    {
        private readonly IControlPanelViewModel _controlPanelViewModel;

        public ControlPanelView()
        {
            InitializeComponent();

            _controlPanelViewModel = (IControlPanelViewModel)DataContext ?? throw new ArgumentNullException(nameof(DataContext));
            InitMenu();
        }

        private void InitMenu()
        {
            _controlPanelViewModel.MenuItems.AddRange(new[]
            {
                new MenuItemViewModel
                {
                    Caption = TM.Tr("Devices"),
                    Icon = "Devices",
                    Content = new DevicesView()
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("UserAgents"),
                    Icon = "UserSearch",
                    Content = new UserAgentsView()
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("AgentSettings"),
                    Icon = "Incognito",
                    Content = new AgentSettings.AgentSettingListView()
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("Employees"),
                    Icon = "AccountCardDetails",
                    Content = new EmployeesView()
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("OrganizationalUnitsStructure"),
                    Icon = "Sitemap",
                    Content = new OrganizationUnitsControl()
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("SystemUsers"),
                    Icon = "SecurityAccount",
                    Content = ""
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("ComponentCoordination"),
                    Icon = "Server",
                    Content = new ServerSettingsView()
                },
                new MenuItemViewModel
                {
                    Caption = TM.Tr("EventsViewer"),
                    Icon = "FileDocumentBox",
                    Content = ""
                }
            });

            MenuItemsListBox.SelectedIndex = 0;
        }

        private void MoveCursorMenu(int index)
        {
            var topMargin = 18;
            if (index != 0)
            {
                topMargin += 51 * index;
            }

            TransitioningContentSlide.OnApplyTemplate();
            ListCursor.Margin = new Thickness(0, topMargin, 0, 0);
        }

        private void MenuItemsListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = MenuItemsListBox.SelectedIndex;
            MoveCursorMenu(index);
        }
    }
}