﻿using System.Windows.Controls;

namespace CWT.Viewer.Wpf.Views.Controls.ControlPanel.Devices
{
    /// <summary>
    /// Interaction logic for DevicesView.xaml
    /// </summary>
    public partial class DevicesView : UserControl
    {
        public DevicesView()
        {
            InitializeComponent();
        }
    }
}
