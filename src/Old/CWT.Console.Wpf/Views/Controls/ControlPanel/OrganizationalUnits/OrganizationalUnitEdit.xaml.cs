﻿using CWT.Viewer.Models;

namespace CWT.Viewer.Wpf.Views.Controls.ControlPanel.OrganizationalUnits
{
    /// <summary>
    /// Interaction logic for OrganizationUnitEdit.xaml
    /// </summary>
    public partial class OrganizationUnitEdit : IDialog
    {
        public OrganizationUnitEdit()
        {
            InitializeComponent();
        }
    }
}
