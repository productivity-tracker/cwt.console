﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CWT.Viewer.Models;
using CWT.Viewer.Models.OrganizationUnits;
using CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces;

namespace CWT.Viewer.Wpf.Views.Controls.ControlPanel.OrganizationalUnits
{
    /// <summary>
    /// Interaction logic for OrganizationUnitsControl.xaml
    /// </summary>
    public partial class OrganizationUnitsControl : UserControl
    {
        public IOrganizationalUnitViewModel ViewModel => DataContext as IOrganizationalUnitViewModel;

        public OrganizationUnitsControl()
        {
            InitializeComponent();
        }

        private void OrganizationUnitsControl_OnLoaded(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// Take Id from CheckBox Uid and transfer value to CheckBoxId struct
        /// </summary>
        /// <param name="sender">The CheckBox clicked.</param>
        /// <param name="e">Parameters associated to the mouse event.</param>
        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var currentCheckBox = (CheckBox)sender;
            CheckBoxId.Id = currentCheckBox.Uid;
        }

        private void TreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (ViewModel == null)
                return;

            ViewModel.SelectedItem = (OrganizationalUnitModel)e.NewValue;
        }
    }
}
