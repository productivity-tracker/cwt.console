﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CWT.Viewer.Wpf.Views.Controls.ControlPanel.UserAgents
{
    /// <summary>
    /// Interaction logic for UserAgentsView.xaml
    /// </summary>
    public partial class UserAgentsView : UserControl
    {
        public UserAgentsView()
        {
            InitializeComponent();
        }
    }
}
