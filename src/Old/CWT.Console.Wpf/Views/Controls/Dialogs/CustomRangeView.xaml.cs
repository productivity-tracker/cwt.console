﻿using System.Windows.Controls;
using CWT.Viewer.Models;

namespace CWT.Viewer.Wpf.Views.Controls.Dialogs
{
    /// <summary>
    /// Interaction logic for CustomRange.xaml
    /// </summary>
    public partial class CustomRangeView : UserControl, IDialog
    {
        public CustomRangeView()
        {
            InitializeComponent();
        }
    }
}
