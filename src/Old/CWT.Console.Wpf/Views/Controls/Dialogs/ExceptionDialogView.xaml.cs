﻿using System.Windows.Controls;

namespace CWT.Viewer.Wpf.Views.Controls.Dialogs
{
    /// <summary>
    /// Interaction logic for ExceptionDialog.xaml
    /// </summary>
    public partial class ExceptionDialog : UserControl
    {
        public ExceptionDialog()
        {
            InitializeComponent();
        }
    }
}
