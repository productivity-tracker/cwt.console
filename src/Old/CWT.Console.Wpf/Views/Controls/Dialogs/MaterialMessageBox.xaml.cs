﻿using CWT.Viewer.Models;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace CWT.Viewer.Wpf.Views.Controls.Dialogs
{
    /// <summary>
    /// Interaction logic for MaterialMessageBox.xaml
    /// </summary>
    public partial class MaterialMessageBox : UserControl, IDialog
    {
        public ObservableCollection<MessageBoxButtons> Buttons { get; } = new ObservableCollection<MessageBoxButtons>();

        public object DialogContent { get; set; }

        public string Title { get; set; }

        public MaterialMessageBox(string title, object content, MessageBoxButtons buttons)
        {
            InitializeComponent();

            DataContext = this;

            if (buttons.HasFlag(MessageBoxButtons.No))
            {
                Buttons.Add(MessageBoxButtons.No);
            }
            if (buttons.HasFlag(MessageBoxButtons.Yes))
            {
                Buttons.Add(MessageBoxButtons.Yes);
            }
            if (buttons.HasFlag(MessageBoxButtons.Cancel))
            {
                Buttons.Add(MessageBoxButtons.Cancel);
            }
            if (buttons.HasFlag(MessageBoxButtons.Ok))
            {
                Buttons.Add(MessageBoxButtons.Ok);
            }
            if (buttons.HasFlag(MessageBoxButtons.Save))
            {
                Buttons.Add(MessageBoxButtons.Save);
            }
            if (buttons.HasFlag(MessageBoxButtons.Apply))
            {
                Buttons.Add(MessageBoxButtons.Apply);
            }

            Title = title;
            DialogContent = content;
        }
    }
}
