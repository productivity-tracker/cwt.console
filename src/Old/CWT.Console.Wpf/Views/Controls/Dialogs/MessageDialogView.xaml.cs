﻿using System.Windows.Controls;

namespace CWT.Viewer.Wpf.Views.Controls.Dialogs
{
    /// <summary>
    /// Interaction logic for MessageDialog.xaml
    /// </summary>
    public partial class MessageDialog : UserControl
    {
        public MessageDialog()
        {
            InitializeComponent();
        }
    }
}
