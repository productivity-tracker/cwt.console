﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;
using CWT.Viewer.Wpf.Converters;
using CWT.Viewer.Wpf.Infrastructure.InteractionRequest;

namespace CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory.Actions
{
    public class AddDateColumnAction : TriggerActionBase<AddDateColumnNotification>
    {
        protected override void ExecuteAction()
        {
            if (!(AssociatedObject is ScreenshotsHistoryDashboardView screenshotsHistoryView))
            {
                return;
            }

            var newColumn = new DataGridTextColumn
            {
                Header = Notification.Header,
                Binding = new Binding
                {
                    Path = new PropertyPath(Notification.Binding),
                    Mode = BindingMode.TwoWay,
                    Converter = new ScreenshotsDetailsToStringConverter(),
                },
                CanUserSort = false
            };

            screenshotsHistoryView.ScreenshotsHistoryTable.Columns.Add(newColumn);
        }
    }
}