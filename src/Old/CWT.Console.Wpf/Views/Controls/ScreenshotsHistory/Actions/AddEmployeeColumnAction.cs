﻿using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;
using CWT.Viewer.Wpf.Infrastructure.InteractionRequest;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using TranslateMe;

namespace CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory.Actions
{
    public class AddEmployeeColumnAction : TriggerActionBase<AddEmployeeColumnNotification>
    {
        protected override void ExecuteAction()
        {
            if (!(AssociatedObject is ScreenshotsHistoryDashboardView screenshotsHistoryView))
            {
                return;
            }

            var dataTemplate = new DataTemplate();
            var displayName = new FrameworkElementFactory(typeof(TextBlock));
            displayName.SetBinding(TextBlock.TextProperty, new Binding(Notification.Binding));
            displayName.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center);
            displayName.SetValue(DockPanel.DockProperty, Dock.Left);

            var dockPanelFactory = new FrameworkElementFactory(typeof(DockPanel));
            dockPanelFactory.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center);

            var checkBoxFactory = new FrameworkElementFactory(typeof(RadioButton));
            checkBoxFactory.SetBinding(ToggleButton.IsCheckedProperty, new Binding("Employee.IsOnline"));
            checkBoxFactory.SetValue(FrameworkElement.ToolTipProperty, TM.Tr("State"));
            checkBoxFactory.SetValue(UIElement.IsEnabledProperty, false);
            checkBoxFactory.SetValue(FrameworkElement.MarginProperty, new Thickness(5, 0, 0, 0));
            checkBoxFactory.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Right);
            checkBoxFactory.SetValue(DockPanel.DockProperty, Dock.Right);

            dockPanelFactory.AppendChild(displayName);
            dockPanelFactory.AppendChild(checkBoxFactory);

            dataTemplate.VisualTree = dockPanelFactory;

            var templateColumn = new DataGridTemplateColumn
            {
                Header = Notification.Header,
                CanUserSort = true,
                CellTemplate = dataTemplate,
                MinWidth = 150
            };

            screenshotsHistoryView.ScreenshotsHistoryTable.Columns.Add(templateColumn);

        }
    }
}