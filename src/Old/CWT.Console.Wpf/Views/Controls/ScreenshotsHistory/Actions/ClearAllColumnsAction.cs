﻿using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;
using CWT.Viewer.Wpf.Infrastructure.InteractionRequest;

namespace CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory.Actions
{
    public class ClearAllColumnsAction : TriggerActionBase<ClearAllColumnsNotification>
    {
        protected override void ExecuteAction()
        {
            if (!(AssociatedObject is ScreenshotsHistoryDashboardView screenshotsHistoryView))
            {
                return;
            }

            screenshotsHistoryView.ScreenshotsHistoryTable.Columns.Clear();
        }
    }
}