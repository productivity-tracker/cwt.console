﻿using CommonServiceLocator;
using CWT.Viewer.Infrastructure;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;
using CWT.Viewer.Wpf.Infrastructure.InteractionRequest;
using DSFramework.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Windows.Threading;
using CWT.Console.Infrastructure;

namespace CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory.Actions
{
    public class OpenTabAction : TriggerActionBase<OpenScreenshotsHistoryNotification>
    {
        protected override void ExecuteAction()
        {
            if (!(AssociatedObject is ScreenshotHistoryTabsView tabsView))
            {
                return;
            }

            var serviceLocator = ServiceLocator.Current;
            var logger = serviceLocator.GetRequiredService<ILoggerFactory>().CreateLogger(typeof(ScreenshotsHistoryPlayerViewModel).ReadableName());
            var dialogService = serviceLocator.GetRequiredService<IDialogService>();
            var notificationMessageQueue = serviceLocator.GetRequiredService<INotificationMessageQueue>();
            var vm = new ScreenshotsHistoryPlayerViewModel(logger,
                                                           Notification.EmployeeId,
                                                           Notification.Date,
                                                           Dispatcher.CurrentDispatcher,
                                                           dialogService,
                                                           notificationMessageQueue);

            tabsView.InitialTabablzControl.AddToSource(new ViewContainer(Notification.ScreenshotHistoryId,
                                                                         Notification.Header,
                                                                         new ScreenshotsHistoryPlayerView
                                                                         { DataContext = vm }));
        }
    }
}