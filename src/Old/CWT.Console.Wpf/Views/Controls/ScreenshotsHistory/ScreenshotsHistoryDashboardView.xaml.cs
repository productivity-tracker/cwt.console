﻿using CWT.Viewer.Models.ScreenshotsHistory;
using CWT.Viewer.ViewModels.ScreenshotsHistory;
using System.Windows.Controls;
using System.Windows.Input;
using CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList;

namespace CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory
{
    public partial class ScreenshotsHistoryDashboardView : UserControl
    {
        public ScreenshotsHistoryDashboardView()
        {
            InitializeComponent();
        }

        private void ScreenshotsHistoryTable_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cells = ScreenshotsHistoryTable.SelectedCells;
            foreach (var cell in cells)
            {
                var date = cell.Column.Header as string;
                if (cell.Item is ScreenshotsHistoryModel item && !string.IsNullOrWhiteSpace(date))
                {
                    var employeeName = item.Employee?.DisplayName;
                    var employeeId = item.Employee?.Id;
                    if (item.Items.TryGetValue(date, out var model))
                    {
                        var historyId = model.ScreenshotHistoryId;
                        if (DataContext is IScreenshotsHistoryViewModel vm)
                        {
                            var screenshotHistoryArgs = new OpenHistoryCommandArgs(employeeName, employeeId, date, historyId);
                            vm.OpenHistoryCommand.Execute(screenshotHistoryArgs);
                        }
                    }
                }
            }
        }
    }
}