﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory
{
    public partial class ScreenshotsHistoryPlayerView : UserControl
    {
        private Point? _lastCenterPositionOnTarget;
        private Point? _lastDragPoint;
        private Point? _lastMousePositionOnTarget;

        public ScreenshotsHistoryPlayerView()
        {
            InitializeComponent();

            ImageScrollViewer.ScrollChanged += OnScrollViewerScrollChanged;
            ImageScrollViewer.MouseLeftButtonUp += OnMouseLeftButtonUp;
            ImageScrollViewer.PreviewMouseLeftButtonUp += OnMouseLeftButtonUp;
            ImageScrollViewer.PreviewMouseWheel += OnPreviewMouseWheel;
            ImageScrollViewer.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
            ImageScrollViewer.MouseMove += OnMouseMove;
            ZoomSlider.ValueChanged += OnZoomValueChanged;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!_lastDragPoint.HasValue)
            {
                return;
            }

            var posNow = e.GetPosition(ImageScrollViewer);
            var dX = posNow.X - _lastDragPoint.Value.X;
            var dY = posNow.Y - _lastDragPoint.Value.Y;

            _lastDragPoint = posNow;

            ImageScrollViewer.ScrollToHorizontalOffset(ImageScrollViewer.HorizontalOffset - dX);
            ImageScrollViewer.ScrollToVerticalOffset(ImageScrollViewer.VerticalOffset - dY);
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var mousePos = e.GetPosition(ImageScrollViewer);
            //make sure we still can use the scrollbars
            if (mousePos.X <= ImageScrollViewer.ViewportWidth && mousePos.Y < ImageScrollViewer.ViewportHeight)
            {
                ImageScrollViewer.Cursor = Cursors.SizeAll;
                _lastDragPoint = mousePos;
                Mouse.Capture(ImageScrollViewer);
            }
        }

        private void OnPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            _lastMousePositionOnTarget = Mouse.GetPosition(ImageGridView);
            if (e.Delta > 0)
            {
                ZoomSlider.Value += 1;
                if (ZoomSlider.Value > 1)
                {
                    ImageScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                    ImageScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
                }
            }

            if (e.Delta < 0)
            {
                ZoomSlider.Value -= 1;
                if (ZoomSlider.Value == 1)
                {
                    ImageScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    ImageScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                }
            }

            e.Handled = true;
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ImageScrollViewer.Cursor = Cursors.Arrow;
            ImageScrollViewer.ReleaseMouseCapture();
            _lastDragPoint = null;
        }

        private void OnZoomValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ScaleTransform.ScaleX = e.NewValue;
            ScaleTransform.ScaleY = e.NewValue;

            var centerOfViewport = new Point(ImageScrollViewer.ViewportWidth / 2, ImageScrollViewer.ViewportHeight / 2);
            _lastCenterPositionOnTarget = ImageScrollViewer.TranslatePoint(centerOfViewport, ImageGridView);
        }

        private void OnScrollViewerScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.ExtentHeightChange == 0 && e.ExtentWidthChange == 0)
            {
                return;
            }

            Point? targetBefore = null;
            Point? targetNow = null;

            if (!_lastMousePositionOnTarget.HasValue)
            {
                if (_lastCenterPositionOnTarget.HasValue)
                {
                    var centerOfViewport = new Point(ImageScrollViewer.ViewportWidth / 2, ImageScrollViewer.ViewportHeight / 2);
                    var centerOfTargetNow = ImageScrollViewer.TranslatePoint(centerOfViewport, ImageGridView);

                    targetBefore = _lastCenterPositionOnTarget;
                    targetNow = centerOfTargetNow;
                }
            }
            else
            {
                targetBefore = _lastMousePositionOnTarget;
                targetNow = Mouse.GetPosition(ImageGridView);

                _lastMousePositionOnTarget = null;
            }

            if (!targetBefore.HasValue)
            {
                return;
            }

            var dXInTargetPixels = targetNow.Value.X - targetBefore.Value.X;
            var dYInTargetPixels = targetNow.Value.Y - targetBefore.Value.Y;

            var multiplicatorX = e.ExtentWidth / ImageGridView.Width;
            var multiplicatorY = e.ExtentHeight / ImageGridView.Height;

            var newOffsetX = ImageScrollViewer.HorizontalOffset - dXInTargetPixels * multiplicatorX;
            var newOffsetY = ImageScrollViewer.VerticalOffset - dYInTargetPixels * multiplicatorY;

            if (double.IsNaN(newOffsetX) || double.IsNaN(newOffsetY))
            {
                return;
            }

            ImageScrollViewer.ScrollToHorizontalOffset(newOffsetX);
            ImageScrollViewer.ScrollToVerticalOffset(newOffsetY);
        }

        private void OnImageListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ListBox listBox)
            {
                listBox.ScrollIntoView(listBox.SelectedItem);
            }
        }

        private void OnImageListBoxPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta < 0)
            {
                ScrollBar.LineRightCommand.Execute(null, e.OriginalSource as IInputElement);
            }
            else
            {
                ScrollBar.LineLeftCommand.Execute(null, e.OriginalSource as IInputElement);
            }

            e.Handled = true;
        }

        private void OnImageListBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                e.Handled = true;
            }
            else if (e.Key == Key.Down)
            {
                e.Handled = true;
            }
        }

        private void SetSelectedImageToSliderValue(object sender, MouseEventArgs e)
        {
            var s = sender as Slider;
            if (s?.DataContext is IScreenshotsHistoryPlayerViewModel vm && vm.ImageListCollection?.Count > 0)
            {
                vm.SelectedImage = vm.ImageListCollection[(int)s.Value];
            }
        }
    }
}