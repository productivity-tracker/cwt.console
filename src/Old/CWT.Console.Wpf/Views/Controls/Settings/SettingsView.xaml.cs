﻿using System.Windows.Controls;
using CWT.Viewer.Models;

namespace CWT.Viewer.Wpf.Views.Controls.Settings
{
    /// <summary>
    /// Interaction logic for SettingsDialog.xaml
    /// </summary>
    public partial class SettingsView : UserControl, IDialog
    {
        public SettingsView()
        {
            InitializeComponent();
        }
    }
}
