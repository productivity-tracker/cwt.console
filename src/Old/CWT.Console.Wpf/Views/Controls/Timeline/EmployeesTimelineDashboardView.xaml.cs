﻿using System.Windows.Controls;
using System.Windows.Input;
using CWT.Viewer.Infrastructure.MappedValue;
using CWT.Viewer.Models.Employees;
using CWT.Viewer.Models.Timeline;
using CWT.Viewer.ViewModels.Timeline;

namespace CWT.Viewer.Wpf.Views.Controls.Timeline
{
    /// <summary>
    /// Interaction logic for EmployeesTimeTableView.xaml
    /// </summary>
    public partial class EmployeesTimelineView
    {
        public EmployeesTimelineView()
        {
            InitializeComponent();
        }

        private void TimelineSchedule_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!(sender is DataGrid dataGrid))
            {
                return;
            }

            var cells = dataGrid.SelectedCells;
            foreach (var cell in cells)
            {
                if (!(cell.Column.Header is DateModel dateModel))
                {
                    return;
                }

                if (!(cell.Item is EmployeeModel employeeModel))
                {
                    continue;
                }

                var employeeId = employeeModel.EntityId;
                var date = dateModel.Date;
                if (DataContext is IEmployeesTimelineViewModel vm)
                {
                    var timelineKey = new EmployeeTimelineContext.TimelineKey(date, employeeId);
                    vm.OpenTimelineCommand.Execute(timelineKey);
                }
            }
        }
    }
}
