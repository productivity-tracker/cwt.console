﻿using CWT.Viewer.Models;

namespace CWT.Viewer.Wpf.Views.Controls.Timeline
{
    /// <summary>
    /// Interaction logic for TimelineView.xaml
    /// </summary>
    public partial class TimelineView : IDialog
    {
        public TimelineView()
        {
            InitializeComponent();
        }
    }
}
