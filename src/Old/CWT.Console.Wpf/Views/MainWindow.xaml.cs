﻿using CWT.Viewer.Infrastructure;
using CWT.Viewer.ViewModels;
using CWT.Viewer.ViewModels.ScreenshotsHistory;
using CWT.Viewer.Wpf.Views.Controls.ScreenshotsHistory;
using CWT.Viewer.Wpf.Views.Controls.Timeline;
using System;
using System.Deployment.Application;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using CWT.Console.Infrastructure;
using TranslateMe;
using ControlPanelView = CWT.Viewer.Wpf.Views.Controls.ControlPanel.ControlPanelView;

namespace CWT.Viewer.Wpf.Views
{
    public partial class MainWindow
    {
        private readonly IMainViewModel _mainViewModel;

        public bool Hidden { get; private set; } = true;

        public MainWindow()
        {
            InitializeComponent();

            _mainViewModel = (IMainViewModel)DataContext ?? throw new ArgumentNullException(nameof(DataContext));

            MainWin.Title = $"{TM.Tr(_mainViewModel.Title)} v{GetRunningVersion()}";
            InitMenu();
        }

        private Version GetRunningVersion()
        {
            Version publishVersion;

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                var cd = ApplicationDeployment.CurrentDeployment;
                publishVersion = cd.CurrentVersion;
                //publishVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }
            else
            {
                //version = typeof(ThisAddIn).Assembly.GetName().Version;
                publishVersion = Assembly.GetExecutingAssembly().GetName().Version;
            }

            return publishVersion;
        }

        private void OnMainMenuBtnClick(object sender, RoutedEventArgs e)
        {
            Storyboard sb;

            if (Hidden)
            {
                sb = Resources["SbShowLeftMenu"] as Storyboard;
                DarkBack.Visibility = Visibility.Visible;
            }
            else
            {
                sb = Resources["SbHideLeftMenu"] as Storyboard;
                DarkBack.Visibility = Visibility.Hidden;
            }

            sb?.Begin(MainLeftMenuPnl);
            Hidden = !Hidden;
        }

        private void MoveCursorMenu(int index)
        {
            TransitioningContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, 51 * index, 0, 0);
        }

        private void InitMenu()
        {
            _mainViewModel.MenuItems.AddRange(new[]
            {
                new MenuItemViewModel
                {
                    Caption = "Timeline",
                    Icon = "Timetable",
                    Content = new TimelineTabsView()
                },
                new MenuItemViewModel
                {
                    Caption = "ScreenshotsHistory",
                    Icon = "ImagesOutline",
                    Content = new ScreenshotHistoryTabsView
                    {
                        DataContext = new ScreenshotsHistoryTabsViewModel(new ViewContainer(TM.Tr("History"),
                                                                                            new ScreenshotsHistoryDashboardView()))
                    }
                },
                new MenuItemViewModel
                {
                    Caption = "ControlPanel",
                    Icon = "CardBulletedSettingsOutline",
                    Content = new ControlPanelView()
                }
            });

            ListBoxMenu.SelectedIndex = 0;
        }

        private void ListBoxMenu_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = ListBoxMenu.SelectedIndex;
            MoveCursorMenu(index);
        }
    }
}