﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.OrganizationalUnits;
using CWT.Sdk.Facades.Base;

namespace CWT.Viewer.Facades
{
    public interface IOrganizationalUnitCachedFacadeProxy : IGenericFacade<OrganizationalUnitDto, EntityHolderOfOrganizationalUnitDto, WriteCommandOfOrganizationalUnitDto>
    {
        void Init();
        EntityHolderOfOrganizationalUnitDto SearchByEmployeeId(string employeeId);
    }
}