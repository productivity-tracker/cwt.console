﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.OrganizationalUnits;
using CWT.Sdk.Facades.Base;
using DSFramework.Caching;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CWT.Viewer.Facades
{
    public class OrganizationalUnitCachedFacadeProxy
        : CachedGenericFacade<OrganizationalUnitDto, EntityHolderOfOrganizationalUnitDto, WriteCommandOfOrganizationalUnitDto>,
          IOrganizationalUnitCachedFacadeProxy
    {
        private const string EMPLOYEE_TAG = "Employee";

        public OrganizationalUnitCachedFacadeProxy(
            IGenericFacade<OrganizationalUnitDto, EntityHolderOfOrganizationalUnitDto, WriteCommandOfOrganizationalUnitDto> facade)
            : base(facade)
        {
        }

        public void Init()
        {
            //GetAll().GetAwaiter();
        }

        public EntityHolderOfOrganizationalUnitDto SearchByEmployeeId(string employeeId)
        {
            var result = Cache.Select(new CacheTag(EMPLOYEE_TAG, employeeId));
            return result?.FirstOrDefault().Value;
        }

        //public override async Task<ICollection<EntityHolderOfOrganizationalUnitDto>> GetAll()
        //{
        //    var entities = await base.GetAll();

        //    foreach (var entity in entities)
        //    {
        //        var tags = entity.Data.Employees.Select(employeeId => new CacheTag(EMPLOYEE_TAG, employeeId)).ToArray();

        //        Cache.Add(entity.Id, entity, tags);
        //    }

        //    return entities;
        //}
    }
}