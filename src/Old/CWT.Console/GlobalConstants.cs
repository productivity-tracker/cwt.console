﻿namespace CWT.Viewer
{
    public static class GlobalConstants
    {
        public const string EXCEPTION_DIALOG_HOST_ID = "ExceptionDialogHost";
        public const string MAIN_DIALOG_HOST_ID = "MainDialogHost";
    }
}