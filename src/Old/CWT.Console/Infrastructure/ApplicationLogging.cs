﻿using DSFramework.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace CWT.Viewer.Infrastructure
{
    public class ApplicationLogging
    {
        private static ILoggerFactory _factory;

        public static ILoggerFactory LoggerFactory => _factory ??= new LoggerFactory();

        public static void ConfigureLogger(ILoggerFactory factory)
        {
            _factory = factory;
        }

        public ILogger CreateLogger()
        {
            var frame = new StackFrame(1, false);

            var declaringType = frame.GetMethod().ReflectedType ?? GetType();

            if (declaringType == null)
            {
                throw new ApplicationException("Logger should be instantiated from somewhere...");
            }

            var readableName = declaringType.ReadableName();
            return LoggerFactory.CreateLogger(readableName);
        }
    }
}