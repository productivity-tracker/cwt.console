﻿using System;

namespace CWT.Viewer.Infrastructure.Commands
{
    /// <summary>
    ///     A command wich accepts no parameter - assumes the view model will do the work
    /// </summary>
    public class RelayCommand : RelayCommandBase
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute ?? (() => true);
        }

        public override bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public override void Execute(object parameter)
        {
            _execute();
        }
    }

    public class RelayCommand<TParam> : RelayCommandBase
    {
        private readonly Action<TParam> _execute;
        private readonly Predicate<TParam> _canExecute;

        /// <summary>
        ///     Initializes a new instance of <see cref="DelegateCommand{T}" />.
        /// </summary>
        /// <param name="execute">
        ///     Delegate to execute when Execute is called on the command.  This can be null to just hook up a
        ///     CanExecute delegate.
        /// </param>
        /// <remarks><seealso cref="CanExecute" /> will always return true.</remarks>
        public RelayCommand(Action<TParam> execute) : this(execute, null) { }

        /// <summary>
        ///     Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<TParam> execute, Predicate<TParam> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        /// <summary>
        ///     Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="parameter">
        ///     Data used by the command.  If the command does not require data to be passed, this object can
        ///     be set to null.
        /// </param>
        /// <returns>
        ///     true if this command can be executed; otherwise, false.
        /// </returns>
        public override bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke((TParam)parameter) ?? true;
        }

        /// <summary>
        ///     Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">
        ///     Data used by the command. If the command does not require data to be passed, this object can be
        ///     set to <see langword="null" />.
        /// </param>
        public override void Execute(object parameter)
        {
            _execute((TParam)parameter);
        }
    }
}