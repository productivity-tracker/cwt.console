﻿using System;
using System.Threading.Tasks;

namespace CWT.Viewer.Infrastructure.Commands
{
    public class RelayCommandAsync : RelayCommandBase
    {
        private readonly Func<Task> _execute;
        private readonly Predicate<object> _canExecute;
        private bool _isExecuting;

        public RelayCommandAsync(Func<Task> execute) : this(execute, null) { }

        public RelayCommandAsync(Func<Task> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public override bool CanExecute(object parameter)
        {
            if (!_isExecuting && _canExecute == null)
            {
                return true;
            }

            return !_isExecuting && _canExecute(parameter);
        }

        public override async void Execute(object parameter)
        {
            _isExecuting = true;
            try
            {
                await _execute();
            }
            finally
            {
                _isExecuting = false;
            }
        }
    }
}