﻿using System;
using System.Windows.Input;

namespace CWT.Viewer.Infrastructure.Commands
{
    public abstract class RelayCommandBase : ICommand
    {
        public abstract bool CanExecute(object parameter);

        public abstract void Execute(object parameter);

        /// <summary>
        ///     Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public static void Refresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}