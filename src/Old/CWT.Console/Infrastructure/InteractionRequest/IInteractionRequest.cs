﻿using System;

namespace CWT.Viewer.Infrastructure.InteractionRequest
{
    public interface IInteractionRequest
    {
        event EventHandler<InteractionRequestedEventArgs> Raised;
    }
}