﻿using System;

namespace CWT.Viewer.Infrastructure.InteractionRequest
{
    public class InteractionRequest<T> : IInteractionRequest
        where T : INotification
    {
        /// <summary>
        /// The raised.
        /// </summary>
        public event EventHandler<InteractionRequestedEventArgs> Raised;

        /// <summary>
        /// Raises the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Raise(T context)
        {
            this.Raise(context, c => { });
        }

        /// <summary>
        /// Raises the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="callback">The callback.</param>
        public void Raise(T context, Action<T> callback)
        {
            var handler = this.Raised;
            handler?.Invoke(this, new InteractionRequestedEventArgs(context, () => callback(context)));
        }
    }
}