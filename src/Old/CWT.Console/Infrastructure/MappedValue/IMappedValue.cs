﻿namespace CWT.Viewer.Infrastructure.MappedValue
{
    public interface IMappedValue<TRow, TColumn, TValue>
    {
        TColumn ColumnBinding { get; set; }
        TRow RowBinding { get; set; }
        TValue Value { get; set; }
    }
}