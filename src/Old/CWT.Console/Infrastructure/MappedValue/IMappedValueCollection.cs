﻿namespace CWT.Viewer.Infrastructure.MappedValue
{
    public interface IMappedValueCollection<out TMapperValue, in TRow, in TColumn>
    {
        TMapperValue GetOrAdd(TColumn columnBinding, TRow rowBinding);
        void RemoveByColumn(TColumn columnBinding);
        void RemoveByRow(TRow rowBinding);
    }
}