﻿using System.Collections.ObjectModel;
using System.Linq;

namespace CWT.Viewer.Infrastructure.MappedValue
{
    public class MappedValueCollection : ObservableCollection<MappedValue>
    {
        public bool Exist(object columnBinding, object rowBinding)
        {
            return this.Count(x => x.RowBinding == rowBinding && x.ColumnBinding == columnBinding) > 0;
        }

        public MappedValue GetOrAdd(object columnBinding, object rowBinding)
        {
            if (Exist(columnBinding, rowBinding))
            {
                return this.Single(x => x.RowBinding == rowBinding && x.ColumnBinding == columnBinding);
            }

            var value = new MappedValue
            {
                ColumnBinding = columnBinding,
                RowBinding = rowBinding
            };
            Add(value);
            return value;
        }

        public void RemoveByColumn(object columnBinding)
        {
            foreach (var item in this.Where(x => x.ColumnBinding == columnBinding).ToList())
            {
                Remove(item);
            }
        }

        public void RemoveByRow(object rowBinding)
        {
            foreach (var item in this.Where(x => x.RowBinding == rowBinding).ToList())
            {
                Remove(item);
            }
        }
    }
}