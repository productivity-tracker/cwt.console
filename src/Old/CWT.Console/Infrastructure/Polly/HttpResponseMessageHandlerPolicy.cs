﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using System;
using System.Net.Http;

namespace CWT.Viewer.Infrastructure.Polly
{
    public static class HttpResponseMessageHandlerPolicy
    {
        public static IAsyncPolicy<HttpResponseMessage> DefaultRetryPolicy(IServiceProvider services, HttpRequestMessage request)
        {
            return HttpPolicyExtensions
                   .HandleTransientHttpError()
                   .Or<TimeoutRejectedException>()
                   .WaitAndRetryAsync(3,
                                      retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                                      (outcome, timespan, retryAttempt, context) =>
                                      {
                                          var logger = services.GetRequiredService<ILoggerFactory>().CreateLogger(nameof(HttpResponseMessageHandlerPolicy));

                                          if (outcome.Exception != null)
                                          {
                                              logger.LogError(outcome.Exception,
                                                              "An exception occurred on retry {RetryAttempt} for {Request}",
                                                              retryAttempt,
                                                              request);
                                          }
                                          else
                                          {
                                              logger.LogError("A non success code {StatusCode} was received on retry {RetryAttempt} for {Request}",
                                                            (int)outcome.Result.StatusCode,
                                                            retryAttempt,
                                                            request);
                                          }
                                      });
        }

        public static IAsyncPolicy<HttpResponseMessage> DefaultRetryPolicy(int retryNumber,
                                                                           HttpRequestMessage request)
        {
            return HttpPolicyExtensions
                   .HandleTransientHttpError()
                   .Or<TimeoutRejectedException>()
                   .WaitAndRetryAsync(retryNumber,
                                      retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                                      (outcome, timespan, retryAttempt, context) =>
                                      {
                                          var logger = new ApplicationLogging().CreateLogger();
                                          if (outcome.Exception != null)
                                          {
                                              logger.LogError(outcome.Exception,
                                                              $"An exception occurred on retry {retryAttempt} for {request.RequestUri}.");
                                          }
                                          else
                                          {
                                              logger.LogError($"A non success code {(int)outcome.Result.StatusCode} was received on retry {retryAttempt} for {request.RequestUri}. ");
                                          }
                                      });
        }

        public static IAsyncPolicy<HttpResponseMessage> Timeout(int seconds = 10)
        {
            return Policy.TimeoutAsync<HttpResponseMessage>(TimeSpan.FromSeconds(seconds));
        }
    }
}
