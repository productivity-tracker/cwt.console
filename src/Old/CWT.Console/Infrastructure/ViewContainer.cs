﻿using System;

namespace CWT.Console.Infrastructure
{
    public class ViewContainer
    {
        public string Id { get; }
        public string Title { get; }
        public object Content { get; }

        public ViewContainer(string title, object content)
        {
            Id = Guid.NewGuid().ToString("N");
            Title = title;
            Content = content;
        }

        public ViewContainer(string id, string title, object content) : this(title, content)
        {
            Id = id;
            Title = title;
            Content = content;
        }
    }
}