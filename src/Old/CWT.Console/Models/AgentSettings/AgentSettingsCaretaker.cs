﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace CWT.Viewer.Models.AgentSettings
{
    public class AgentSettingsCaretaker
    {
        private readonly IDictionary<string, AgentSettingsModel> _store;

        public AgentSettingsCaretaker()
        {
            _store = new ConcurrentDictionary<string, AgentSettingsModel>();
        }

        public void Backup(AgentSettingsModel settings)
        {
            _store[settings.Id] = settings;
        }

        public AgentSettingsModel Restore(string settingsId)
        {
            return _store.TryGetValue(settingsId, out var settings) ? settings : default;
        }
    }
}