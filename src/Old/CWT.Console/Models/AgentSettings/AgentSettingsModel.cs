﻿using System;
using DynamicData.Binding;

namespace CWT.Viewer.Models.AgentSettings
{
    public class AgentSettingsModel : AbstractNotifyPropertyChanged, IEquatable<AgentSettingsModel>, IMemento<AgentSettingsModel>
    {
        private string _bucketName;
        private bool _captureDesktop;
        private EntityDetails _details;
        private FrameFrequency _frameFrequency;
        private string _id;
        private ImageFormat _imageFormat;
        private bool _isDefault;
        private string _name;
        private string _observedProcesses;
        private long _quality;
        private Period _screenshotHistoryPeriod;
        private TimeSpan _screenshotsFrequency;
        private TimeSpan _screenshotsHistoryTtl;
        private StorageModel _storage;

        public string Id
        {
            get => _id;
            set => SetAndRaise(ref _id, value);
        }

        public string Name
        {
            get => _name;
            set => SetAndRaise(ref _name, value);
        }

        public bool IsDefault
        {
            get => _isDefault;
            set => SetAndRaise(ref _isDefault, value);
        }

        public StorageModel Storage
        {
            get => _storage;
            set => SetAndRaise(ref _storage, value);
        }

        public string BucketName
        {
            get => _bucketName;
            set => SetAndRaise(ref _bucketName, value);
        }

        public bool CaptureDesktop
        {
            get => _captureDesktop;
            set => SetAndRaise(ref _captureDesktop, value);
        }

        public TimeSpan ScreenshotsFrequency
        {
            get => _screenshotsFrequency;
            set => SetAndRaise(ref _screenshotsFrequency, value);
        }

        public FrameFrequency FrameFrequency
        {
            get => _frameFrequency;
            set => SetAndRaise(ref _frameFrequency, value);
        }

        public string ObservedProcesses
        {
            get => _observedProcesses;
            set => SetAndRaise(ref _observedProcesses, value);
        }

        public ImageFormat ImageFormat
        {
            get => _imageFormat;
            set => SetAndRaise(ref _imageFormat, value);
        }

        public long Quality
        {
            get => _quality;
            set => SetAndRaise(ref _quality, value);
        }

        public TimeSpan ScreenshotsHistoryTtl
        {
            get => _screenshotsHistoryTtl;
            set => SetAndRaise(ref _screenshotsHistoryTtl, value);
        }

        public Period ScreenshotHistoryPeriod
        {
            get => _screenshotHistoryPeriod;
            set => SetAndRaise(ref _screenshotHistoryPeriod, value);
        }

        public EntityDetails Details
        {
            get => _details;
            set => SetAndRaise(ref _details, value);
        }

        public AgentSettingsModel()
        {
        }

        public AgentSettingsModel(string id,
                                  string bucketName,
                                  bool captureDesktop,
                                  EntityDetails details,
                                  FrameFrequency frameFrequency,
                                  ImageFormat imageFormat,
                                  bool isDefault,
                                  string name,
                                  string observedProcesses,
                                  long quality,
                                  Period screenshotHistoryPeriod,
                                  TimeSpan screenshotsFrequency,
                                  TimeSpan screenshotsHistoryTtl,
                                  StorageModel storage)
        {
            _bucketName = bucketName;
            _captureDesktop = captureDesktop;
            _details = details;
            _frameFrequency = frameFrequency;
            _id = id;
            _imageFormat = imageFormat;
            _isDefault = isDefault;
            _name = name;
            _observedProcesses = observedProcesses;
            _quality = quality;
            _screenshotHistoryPeriod = screenshotHistoryPeriod;
            _screenshotsFrequency = screenshotsFrequency;
            _screenshotsHistoryTtl = screenshotsHistoryTtl;
            _storage = storage;
        }

        public static AgentSettingsModel Default()
        {
            return new AgentSettingsModel(null,
                                          string.Empty,
                                          false,
                                          null,
                                          FrameFrequency.Fpm,
                                          ImageFormat.Jpg,
                                          false,
                                          null,
                                          null,
                                          70L,
                                          Period.Day,
                                          TimeSpan.FromSeconds(10),
                                          TimeSpan.FromDays(10),
                                          null);
        }

        public bool Equals(AgentSettingsModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(_bucketName, other._bucketName) && _captureDesktop == other._captureDesktop && Equals(_details, other._details) &&
                   _frameFrequency == other._frameFrequency && _imageFormat == other._imageFormat && _isDefault == other._isDefault &&
                   string.Equals(_name, other._name) && string.Equals(_observedProcesses, other._observedProcesses) && _quality == other._quality &&
                   _screenshotHistoryPeriod == other._screenshotHistoryPeriod && _screenshotsHistoryTtl.Equals(other._screenshotsHistoryTtl) &&
                   _screenshotsFrequency.Equals(other._screenshotsFrequency) && Equals(_storage, other._storage) && string.Equals(_id, other._id);
        }

        public AgentSettingsModel SaveState()
        {
            var clone = new AgentSettingsModel(Id,
                                               BucketName,
                                               CaptureDesktop,
                                               Details.SaveState(),
                                               FrameFrequency,
                                               ImageFormat,
                                               IsDefault,
                                               Name,
                                               ObservedProcesses,
                                               Quality,
                                               ScreenshotHistoryPeriod,
                                               ScreenshotsFrequency,
                                               ScreenshotsHistoryTtl,
                                               Storage);

            return clone;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((AgentSettingsModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _bucketName != null ? _bucketName.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ _captureDesktop.GetHashCode();
                hashCode = (hashCode * 397) ^ (_details != null ? _details.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)_frameFrequency;
                hashCode = (hashCode * 397) ^ (int)_imageFormat;
                hashCode = (hashCode * 397) ^ _isDefault.GetHashCode();
                hashCode = (hashCode * 397) ^ (_name != null ? _name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (_observedProcesses != null ? _observedProcesses.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _quality.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)_screenshotHistoryPeriod;
                hashCode = (hashCode * 397) ^ _screenshotsHistoryTtl.GetHashCode();
                hashCode = (hashCode * 397) ^ _screenshotsFrequency.GetHashCode();
                hashCode = (hashCode * 397) ^ (_storage != null ? _storage.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (_id != null ? _id.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(AgentSettingsModel left, AgentSettingsModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AgentSettingsModel left, AgentSettingsModel right)
        {
            return !Equals(left, right);
        }
    }
}