﻿using System;
using DynamicData.Binding;

namespace CWT.Viewer.Models.AgentSettings
{
    public class StorageModel : AbstractNotifyPropertyChanged, IEquatable<StorageModel>
    {
        private string _id;
        private string _name;

        public string Id
        {
            get => _id;
            set => SetAndRaise(ref _id, value);
        }

        public string Name
        {
            get => _name;
            set => SetAndRaise(ref _name, value);
        }

        public StorageModel(string id, string name)
        {
            _id = id;
            _name = name;
        }

        public bool Equals(StorageModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(_name, other._name) && string.Equals(_id, other._id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((StorageModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_name != null ? _name.GetHashCode() : 0) * 397) ^ (_id != null ? _id.GetHashCode() : 0);
            }
        }

        public static bool operator==(StorageModel left, StorageModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(StorageModel left, StorageModel right)
        {
            return !Equals(left, right);
        }
    }
}