﻿using System;

namespace CWT.Viewer.Models.Devices
{
    public class DeviceModel : IEquatable<DeviceModel>
    {
        public string EntityId { get; set; }
        public string DeviceName { get; set; }
        public string Domain { get; set; }
        public string IpAddress { get; set; }
        public string OperatingSystem { get; set; }
        public string AgentVersion { get; set; }
        public bool IsOnline { get; set; }
        public DateTime LastConnectionDate { get; set; }
        public string LastLoggedUserAgent { get; set; }

        public DeviceModel()
        {
        }

        public DeviceModel(
            string entityId,
            string deviceName,
            string domain,
            string ipAddress,
            string operatingSystem,
            string agentVersion,
            bool isOnline,
            DateTime lastConnectionDate,
            string lastLoggedUserAgent)
        {
            EntityId = entityId;
            DeviceName = deviceName;
            Domain = domain;
            IpAddress = ipAddress;
            OperatingSystem = operatingSystem;
            AgentVersion = agentVersion;
            IsOnline = isOnline;
            LastConnectionDate = lastConnectionDate;
            LastLoggedUserAgent = lastLoggedUserAgent;
        }

        public bool Equals(DeviceModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(EntityId, other.EntityId) && string.Equals(DeviceName, other.DeviceName) && string.Equals(Domain, other.Domain) &&
                   string.Equals(IpAddress, other.IpAddress) && string.Equals(OperatingSystem, other.OperatingSystem) &&
                   string.Equals(AgentVersion, other.AgentVersion) && IsOnline == other.IsOnline &&
                   LastConnectionDate.Equals(other.LastConnectionDate) && string.Equals(LastLoggedUserAgent, other.LastLoggedUserAgent);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((DeviceModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EntityId != null ? EntityId.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (DeviceName != null ? DeviceName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Domain != null ? Domain.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IpAddress != null ? IpAddress.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (OperatingSystem != null ? OperatingSystem.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (AgentVersion != null ? AgentVersion.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsOnline.GetHashCode();
                hashCode = (hashCode * 397) ^ LastConnectionDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (LastLoggedUserAgent != null ? LastLoggedUserAgent.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator==(DeviceModel left, DeviceModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(DeviceModel left, DeviceModel right)
        {
            return !Equals(left, right);
        }
    }
}