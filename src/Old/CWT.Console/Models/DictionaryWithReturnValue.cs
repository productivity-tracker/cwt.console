﻿using System.Collections.Generic;

namespace CWT.Viewer.Models
{
    public class DictionaryWithReturnValue<TKey, TValue> : Dictionary<TKey, TValue>
    {
        /// <summary>
        /// Gets or sets the value for the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The value, if it has been previously set; otherwise default(TValue)</returns>
        public new TValue this[TKey key]
        {
            get => TryGetValue(key, out var value) ? value : default(TValue);
            set => base[key] = value;
        }
    }
}