﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace CWT.Viewer.Models
{
    public class DictionaryWrapper<TValue> : DynamicObject, INotifyPropertyChanged
    {
        readonly Dictionary<string, TValue> _dictionary;

        public DictionaryWrapper(Dictionary<string, TValue> dictionary)
        {
            _dictionary = dictionary;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            TValue value;
            if (!_dictionary.TryGetValue(binder.Name, out value))
            {
                value = default(TValue);
                _dictionary.Add(binder.Name, value);
            }
            result = value;
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _dictionary[binder.Name] = (TValue)value;
            OnPropertyChanged(binder.Name);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}