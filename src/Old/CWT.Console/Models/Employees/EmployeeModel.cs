﻿using System;
using System.Collections.Generic;
using DynamicData.Binding;

namespace CWT.Viewer.Models.Employees
{
    public class EmployeeModel : AbstractNotifyPropertyChanged, IEquatable<EmployeeModel>, IMemento<EmployeeModel>, IHaveDefault<EmployeeModel>
    {
        public string EntityId { get; }
        public string Name { get; }
        public string JobTitle { get; }
        public bool IsOnline { get; }
        public DateTime LastLoggedOnDate { get; }
        public string SettingsName { get; }
        public IEnumerable<string> OrganizationalUnitIds { get; }
        public string Notes { get; }

        public EmployeeModel() { }

        public EmployeeModel(string entityId,
                             string name,
                             string jobTitle,
                             bool isOnline,
                             DateTime lastLoggedOnDate,
                             string settingsName,
                             IEnumerable<string> organizationalUnitIds,
                             string notes)
        {
            EntityId = entityId;
            Name = name;
            JobTitle = jobTitle;
            IsOnline = isOnline;
            LastLoggedOnDate = lastLoggedOnDate;
            SettingsName = settingsName;
            OrganizationalUnitIds = organizationalUnitIds;
            Notes = notes;
        }

        public bool Equals(EmployeeModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(EntityId, other.EntityId) && string.Equals(Name, other.Name) && string.Equals(JobTitle, other.JobTitle) &&
                   IsOnline == other.IsOnline && LastLoggedOnDate.Equals(other.LastLoggedOnDate) &&
                   string.Equals(SettingsName, other.SettingsName) && Equals(OrganizationalUnitIds, other.OrganizationalUnitIds) &&
                   string.Equals(Notes, other.Notes);
        }

        public EmployeeModel GetDefault()
        {
            return new EmployeeModel();
        }

        public EmployeeModel SaveState()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((EmployeeModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EntityId != null ? EntityId.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (JobTitle != null ? JobTitle.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsOnline.GetHashCode();
                hashCode = (hashCode * 397) ^ LastLoggedOnDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (SettingsName != null ? SettingsName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (OrganizationalUnitIds != null ? OrganizationalUnitIds.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Notes != null ? Notes.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator==(EmployeeModel left, EmployeeModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(EmployeeModel left, EmployeeModel right)
        {
            return !Equals(left, right);
        }
    }
}