﻿using System;
using DynamicData.Binding;

namespace CWT.Viewer.Models
{
    public class EntityDetails : AbstractNotifyPropertyChanged, IEquatable<EntityDetails>, IMemento<EntityDetails>
    {
        private string _createdBy;
        private DateTime _createdDate;
        private string _modifiedBy;
        private DateTime? _modifiedDate;

        public EntityDetails(string createdBy, DateTime createdDate, string modifiedBy, DateTime? modifiedDate)
        {
            _createdBy = createdBy;
            _createdDate = createdDate;
            _modifiedBy = modifiedBy;
            _modifiedDate = modifiedDate;
        }

        public DateTime CreatedDate
        {
            get => _createdDate;
            set => SetAndRaise(ref _createdDate, value);
        }

        public DateTime? ModifiedDate
        {
            get => _modifiedDate;
            set => SetAndRaise(ref _modifiedDate, value);
        }

        public string CreatedBy
        {
            get => _createdBy;
            set => SetAndRaise(ref _createdBy, value);
        }

        public string ModifiedBy
        {
            get => _modifiedBy;
            set => SetAndRaise(ref _modifiedBy, value);
        }

        public bool Equals(EntityDetails other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return _createdDate.Equals(other._createdDate) && _modifiedDate.Equals(other._modifiedDate) &&
                   string.Equals(_createdBy, other._createdBy) && string.Equals(_modifiedBy, other._modifiedBy);
        }

        public EntityDetails SaveState()
        {
            return new EntityDetails(CreatedBy, CreatedDate, ModifiedBy, ModifiedDate);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((EntityDetails)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _createdDate.GetHashCode();
                hashCode = (hashCode * 397) ^ _modifiedDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (_createdBy != null ? _createdBy.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (_modifiedBy != null ? _modifiedBy.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator==(EntityDetails left, EntityDetails right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(EntityDetails left, EntityDetails right)
        {
            return !Equals(left, right);
        }
    }
}