﻿using System;

namespace CWT.Viewer.Models
{
    public enum FrameFrequency
    {
        Fps,
        Fpm,
        Fph
    }

    public static class FrameFrequencyExtensions
    {
        public static Period ToPeriod(this FrameFrequency frequency)
        {
            switch (frequency)
            {
                case FrameFrequency.Fph: return Period.Hour;
                case FrameFrequency.Fpm: return Period.Minute;
                case FrameFrequency.Fps: return Period.Second;
                default:                 return default;
            }
        }

        public static double TotalMilliseconds(this FrameFrequency frequency)
        {
            switch (frequency)
            {
                case FrameFrequency.Fph: return TimeSpan.FromHours(1).TotalMilliseconds;
                case FrameFrequency.Fpm: return TimeSpan.FromMinutes(1).TotalMilliseconds;
                case FrameFrequency.Fps: return TimeSpan.FromSeconds(1).TotalMilliseconds;
                default:                 return default;
            }
        }
        public static ushort GetFrameCount(this FrameFrequency frequency, TimeSpan timeSpan)
        {
            var totalMilliseconds = frequency.TotalMilliseconds();
            var count = totalMilliseconds / timeSpan.TotalMilliseconds;

            return count > 0 ? (ushort)count : (ushort)0;
        }

        public static TimeSpan GetSleepDuration(this FrameFrequency frequency, int frameCount)
        {
            var totalMilliseconds = frequency.TotalMilliseconds();
            var duration = TimeSpan.FromMilliseconds(totalMilliseconds / frameCount);
            return duration;
        }
    }
}