﻿namespace CWT.Viewer.Models
{
    public interface IHaveDefault<out T>
    where T : new()
    {
        T GetDefault();
    }
}