﻿namespace CWT.Viewer.Models
{
    public interface IMemento<out T>
    {
        T SaveState();
    }
}