﻿namespace CWT.Viewer.Models
{
    public enum ImageFormat
    {
        Jpg,
        Png,
        Gif,
        Bmp
    }
}