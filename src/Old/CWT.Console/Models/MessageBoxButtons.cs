﻿using System;

namespace CWT.Viewer.Models
{
    [Flags]
    public enum MessageBoxButtons : byte
    {
        /// <summary>
        /// No button was pressed.
        /// This button didn't showed in messagebox.
        /// </summary>
        Nothing = 0,

        /// <summary>
        /// OK button.
        /// </summary>
        Ok = 1,

        /// <summary>
        /// CANCEL button.
        /// </summary>
        Cancel = 2,

        /// <summary>
        /// YES button.
        /// </summary>
        Yes = 4,

        /// <summary>
        /// NO button.
        /// </summary>
        No = 8,

        /// <summary>
        /// SAVE button.
        /// </summary>
        Save = 16,

        /// <summary>
        /// APPLY button.
        /// </summary>
        Apply = 32,

        /// <summary>
        /// YES and NO buttons.
        /// </summary>
        YesNo = Yes | No,

        /// <summary>
        /// OK and CANCEL buttons.
        /// </summary>
        OkCancel = Ok | Cancel,

        /// <summary>
        /// OK and CANCEL buttons.
        /// </summary>
        SaveCancel = Save | Cancel,

        /// <summary>
        /// APPLY and CANCEL buttons.
        /// </summary>
        ApplyCancel = Apply | Cancel
    }
}