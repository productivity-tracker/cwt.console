﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CWT.Viewer.Models
{
    public class Node : INotifyPropertyChanged, IEquatable<Node>
    {
        private bool? _isChecked;
        private bool _isExpanded;
        private string _name;

        public ObservableCollection<Node> Parent { get; } = new ObservableCollection<Node>();
        public ObservableCollection<Node> Children { get; } = new ObservableCollection<Node>();

        public string Id { get; private set; }

        public bool IsRoot { get; private set; }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        public bool? IsChecked
        {
            get => _isChecked;
            set
            {
                _isChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                _isExpanded = value;
                RaisePropertyChanged(nameof(IsExpanded));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Node(string name, string id = null, bool isRoot = false)
        {
            Name = name;
            Id = id ?? Guid.NewGuid().ToString("N");
            IsRoot = isRoot;
            IsExpanded = true;
        }

        public virtual Node GetRootNode()
        {
            if (IsRoot)
            {
                return this;
            }

            Node rootNode = null;
            foreach (var parentNode in Parent)
            {
                if (parentNode.IsRoot)
                {
                    rootNode = parentNode;
                    break;
                }
                rootNode = GetRootNode();
            }

            return rootNode;
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            const int countCheck = 0;
            if (propertyName == nameof(IsChecked))
            {
                if (Id == CheckBoxId.Id && Parent.Count == 0 && Children.Count != 0)
                {
                    CheckedTreeParent(Children, IsChecked);
                }

                if (Id == CheckBoxId.Id && Parent.Count > 0 && Children.Count > 0)
                {
                    CheckedTreeChildMiddle(Parent, Children, IsChecked);
                }

                if (Id == CheckBoxId.Id && Parent.Count > 0 && Children.Count == 0)
                {
                    CheckedTreeChild(Parent, countCheck);
                }
            }
        }

        private void CheckedTreeChildMiddle(ObservableCollection<Node> itemsParent, ObservableCollection<Node> itemsChild, bool? isChecked)
        {
            const int countCheck = 0;
            CheckedTreeParent(itemsChild, isChecked);
            CheckedTreeChild(itemsParent, countCheck);
        }

        private void CheckedTreeParent(ObservableCollection<Node> items, bool? isChecked)
        {
            foreach (var item in items)
            {
                item.IsChecked = isChecked;
                if (item.Children.Count != 0)
                {
                    CheckedTreeParent(item.Children, isChecked);
                }
            }
        }

        private void CheckedTreeChild(ObservableCollection<Node> items, int countCheck)
        {
            var isNull = false;
            foreach (var paren in items)
            {
                foreach (var child in paren.Children)
                {
                    if (child.IsChecked == true || child.IsChecked == null)
                    {
                        countCheck++;
                        if (child.IsChecked == null)
                        {
                            isNull = true;
                        }
                    }
                }

                if (countCheck != paren.Children.Count && countCheck != 0)
                {
                    paren.IsChecked = null;
                }
                else if (countCheck == 0)
                {
                    paren.IsChecked = false;
                }
                else if (countCheck == paren.Children.Count && isNull)
                {
                    paren.IsChecked = null;
                }
                else if (countCheck == paren.Children.Count && !isNull)
                {
                    paren.IsChecked = true;
                }

                if (paren.Parent.Count != 0)
                {
                    CheckedTreeChild(paren.Parent, 0);
                }
            }
        }

        #region Equality members
        
        public bool Equals(Node other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _isChecked == other._isChecked && _isExpanded == other._isExpanded && string.Equals(_name, other._name) && Equals(Parent, other.Parent) && Equals(Children, other.Children) && string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Node)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _isChecked.GetHashCode();
                hashCode = (hashCode * 397) ^ _isExpanded.GetHashCode();
                hashCode = (hashCode * 397) ^ (_name != null ? _name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Parent != null ? Parent.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Children != null ? Children.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Id != null ? Id.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Node left, Node right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Node left, Node right)
        {
            return !Equals(left, right);
        }

        #endregion
    }

    public struct CheckBoxId
    {
        public static string Id { get; set; }
    }
}