﻿using System;

namespace CWT.Viewer.Models.OrganizationUnits
{
    public class OrganizationalUnitModel : Node, IHaveDefault<OrganizationalUnitModel>, IMemento<OrganizationalUnitModel>, IEquatable<OrganizationalUnitModel>
    {
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OrganizationalUnitModel)obj);
        }

        public OrganizationalUnitModel() : base(string.Empty) 
        {
            
        }

        public OrganizationalUnitModel(Node node) : this(node.Name, node.Id, node.IsRoot)
        {

        }

        public OrganizationalUnitModel(string name, string id = null, bool isRoot = false) : base(name, id, isRoot)
        {

        }

        public OrganizationalUnitModel GetDefault()
        {
            return new OrganizationalUnitModel();
        }

        public OrganizationalUnitModel SaveState()
        {
            return new OrganizationalUnitModel(Name, Id, IsRoot);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool Equals(OrganizationalUnitModel other)
        {
            return base.Equals(other);
        }
        
        public static bool operator==(OrganizationalUnitModel left, OrganizationalUnitModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(OrganizationalUnitModel left, OrganizationalUnitModel right)
        {
            return !Equals(left, right);
        }
    }
}