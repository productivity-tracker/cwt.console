﻿using System;

namespace CWT.Viewer.Models
{
    public enum Period
    {
        Second,
        Minute,
        Hour,
        Day
    }

    public static class PeriodExtension
    {
        public static TimeSpan GetTimeSpan(this Period period, int interval)
        {
            switch (period)
            {
                case Period.Second: return TimeSpan.FromSeconds(interval);
                case Period.Minute: return TimeSpan.FromMinutes(interval);
                case Period.Hour:   return TimeSpan.FromHours(interval);
                case Period.Day:    return TimeSpan.FromDays(interval);
                default:            return default;
            }
        }

        public static Period GetPeriod(this TimeSpan time)
        {
            var period = Period.Second;

            if (time.Days > 0)
            {
                period = Period.Day;
            }
            if (time.Hours > 0)
            {
                period = Period.Hour;
            }
            if (time.Minutes > 0)
            {
                period = Period.Minute;
            }
            if (time.Seconds > 0)
            {
                period = Period.Second;
            }

            return period;
        }

        public static FrameFrequency ToFrequency(this Period period)
        {
            switch (period)
            {
                case Period.Minute: return FrameFrequency.Fph;
                case Period.Second: return FrameFrequency.Fpm;
                default:            return default;
            }
        }
    }
}