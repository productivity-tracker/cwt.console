﻿using DSFramework.Extensions;
using DynamicData.Binding;
using System;

namespace CWT.Viewer.Models.ScreenshotsHistory
{
    public class EmployeeModel : AbstractNotifyPropertyChanged, IEquatable<EmployeeModel>
    {
        private DateTime _lastActivity;

        public string Id { get; }
        public string AgentId { get; }
        public string DisplayName { get; }
        public string OrganizationalUnit { get; }

        public DateTime LastActivity
        {
            get => _lastActivity;
            set => SetAndRaise(ref _lastActivity, value);
        }

        public bool IsOnline { get; set; } = true;

        //public bool IsOnline => DateTime.UtcNow - LastActivity < TimeSpan.FromMinutes(1);

        public EmployeeModel(DateTime lastActivity,
                             string id,
                             string agentId,
                             string displayName,
                             string organizationalUnit)
        {
            _lastActivity = lastActivity;
            Id = id;
            AgentId = agentId;
            DisplayName = displayName;
            OrganizationalUnit = organizationalUnit;
        }

        public bool Equals(EmployeeModel other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return _lastActivity.Equals(other._lastActivity) && string.Equals(Id, other.Id) && string.Equals(AgentId, other.AgentId) &&
                   string.Equals(DisplayName, other.DisplayName) && string.Equals(OrganizationalUnit, other.OrganizationalUnit);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((EmployeeModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                if (!AgentId.IsNullOrWhiteSpace())
                {
                    hashCode = (hashCode * 397) ^ _lastActivity.GetHashCode();
                }

                if (!AgentId.IsNullOrWhiteSpace())
                {
                    hashCode = (hashCode * 397) ^ AgentId.GetHashCode();
                }

                if (!DisplayName.IsNullOrWhiteSpace())
                {
                    hashCode = (hashCode * 397) ^ (DisplayName != null ? DisplayName.GetHashCode() : 0);
                }

                if (!OrganizationalUnit.IsNullOrWhiteSpace())
                {
                    hashCode = (hashCode * 397) ^ (OrganizationalUnit != null ? OrganizationalUnit.GetHashCode() : 0);
                }

                return hashCode;
            }
        }

        public static bool operator ==(EmployeeModel left, EmployeeModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EmployeeModel left, EmployeeModel right)
        {
            return !Equals(left, right);
        }
    }
}