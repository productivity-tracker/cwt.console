﻿using System;
using DynamicData.Binding;

namespace CWT.Viewer.Models.ScreenshotsHistory
{
    public class ImageModel : AbstractNotifyPropertyChanged
    {
        private string _employeeId;
        private Uri _imageUrl;
        private DateTime _timestamp;

        public Uri ImageUrl
        {
            get
            {
                if (_imageUrl == null) _imageUrl = new Uri(string.Empty, UriKind.RelativeOrAbsolute);
                return _imageUrl;
            }
            set => SetAndRaise(ref _imageUrl, value);
        }

        public Guid Id { get; }

        public DateTime Timestamp
        {
            get => _timestamp;
            set => SetAndRaise(ref _timestamp, value);
        }

        public string EmployeeId
        {
            get => _employeeId;
            set => SetAndRaise(ref _employeeId, value);
        }

        public string Time => $"{Timestamp:hh:mm:ss}";

        public ImageModel()
        {
            Id = Guid.NewGuid();
        }
    }
}