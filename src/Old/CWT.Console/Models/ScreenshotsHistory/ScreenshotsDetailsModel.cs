﻿using DSFramework.Timing;
using DynamicData.Binding;
using System;

namespace CWT.Viewer.Models.ScreenshotsHistory
{
    public class ScreenshotsDetailsModel : AbstractNotifyPropertyChanged, IEquatable<ScreenshotsDetailsModel>
    {
        private TimeSpan _firstRecordTs;
        private TimeSpan _lastRecordTs;
        private TimeSpan _timespan;

        public string ScreenshotHistoryId { get; set; }

        public TimeSpan FirstRecordTs
        {
            get => _firstRecordTs;
            set => SetAndRaise(ref _firstRecordTs, value);
        }

        public TimeSpan LastRecordTs
        {
            get => _lastRecordTs;
            set => SetAndRaise(ref _lastRecordTs, value);
        }

        public TimeSpan TimeSpan
        {
            get => _timespan;
            set => SetAndRaise(ref _timespan, value);
        }

        public DateTime CurrentDate { get; set; }
        public long DayOfYear => CurrentDate.DayOfYear;

        public ScreenshotsDetailsModel(TimeSpan firstRecordTs,
                                       TimeSpan lastRecordTs,
                                       TimeSpan timespan,
                                       string screenshotHistoryId,
                                       Date date)
        {
            _firstRecordTs = firstRecordTs;
            _lastRecordTs = lastRecordTs;
            _timespan = timespan;
            ScreenshotHistoryId = screenshotHistoryId;
            CurrentDate = (DateTime)date;
        }

        public bool Equals(ScreenshotsDetailsModel other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return _firstRecordTs.Equals(other._firstRecordTs) && _lastRecordTs.Equals(other._lastRecordTs) && _timespan.Equals(other._timespan) &&
                   string.Equals(ScreenshotHistoryId, other.ScreenshotHistoryId) && CurrentDate.Equals(other.CurrentDate);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((ScreenshotsDetailsModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _firstRecordTs.GetHashCode();
                hashCode = (hashCode * 397) ^ _lastRecordTs.GetHashCode();
                hashCode = (hashCode * 397) ^ _timespan.GetHashCode();
                hashCode = (hashCode * 397) ^ (ScreenshotHistoryId != null ? ScreenshotHistoryId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ CurrentDate.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ScreenshotsDetailsModel left, ScreenshotsDetailsModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ScreenshotsDetailsModel left, ScreenshotsDetailsModel right)
        {
            return !Equals(left, right);
        }
    }
}