﻿using DSFramework.Extensions;
using DynamicData.Binding;
using System;

namespace CWT.Viewer.Models.ScreenshotsHistory
{
    public class ScreenshotsHistoryModel : AbstractNotifyPropertyChanged, IEquatable<ScreenshotsHistoryModel>
    {
        private EmployeeModel _employee;
        private DictionaryWithReturnValue<string, ScreenshotsDetailsModel> _items;

        public EmployeeModel Employee
        {
            get => _employee;
            set => SetAndRaise(ref _employee, value);
        }

        public DictionaryWithReturnValue<string, ScreenshotsDetailsModel> Items
        {
            get => _items;
            set => SetAndRaise(ref _items, value);
        }

        public ScreenshotsHistoryModel(EmployeeModel employee, DictionaryWithReturnValue<string, ScreenshotsDetailsModel> items)
        {
            _employee = employee;
            _items = items;
        }

        public bool Equals(ScreenshotsHistoryModel other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Equals(_employee, other._employee) && _items.DictionaryEqual(other._items);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((ScreenshotsHistoryModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_employee != null ? _employee.GetHashCode() : 0) * 397) ^ (_items != null ? _items.GetHashCode() : 0);
            }
        }

        public static bool operator ==(ScreenshotsHistoryModel left, ScreenshotsHistoryModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ScreenshotsHistoryModel left, ScreenshotsHistoryModel right)
        {
            return !Equals(left, right);
        }
    }
}