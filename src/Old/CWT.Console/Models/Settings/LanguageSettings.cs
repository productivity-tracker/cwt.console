﻿using CWT.Viewer.Services;

namespace CWT.Viewer.Models.Settings
{
    public class LanguageSettings : SettingsBase
    {
        private string _language;
        public string Language
        {
            get => _language;
            set
            {
                _language = value;
                OnPropertyChanged(_language);
            }
        }

        public LanguageSettings(ISettingsService settingsService) : base(settingsService)
        {
        }
    }
}