﻿using CWT.Viewer.Services;

namespace CWT.Viewer.Models.Settings
{
    public class PaletteSettings : SettingsBase
    {
        private string _themeColor;
        private bool _themeIsDark;

        public string ThemeColor
        {
            get => _themeColor;
            set
            {
                _themeColor = value;
                OnPropertyChanged(_themeColor);
            }
        }
        public bool ThemeIsDark
        {
            get => _themeIsDark;
            set
            {
                _themeIsDark = value;
                OnPropertyChanged(_themeIsDark);
            }
        }

        public PaletteSettings(ISettingsService settingsService) : base(settingsService)
        {
        }
    }
}