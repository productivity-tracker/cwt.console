﻿using System.Runtime.CompilerServices;
using CWT.Viewer.Services;

namespace CWT.Viewer.Models.Settings
{
    public class SettingsBase
    {
        private readonly ISettingsService _settingsService;

        public SettingsBase(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        protected void OnPropertyChanged<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (string.IsNullOrEmpty(propertyName))
                return;

            _settingsService.SetProperty(propertyName, newValue);
        }
    }
}