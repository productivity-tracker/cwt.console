﻿using CWT.Viewer.Services;

namespace CWT.Viewer.Models.Settings
{
    public class SignInSettings : SettingsBase
    {
        private bool _rememberUsername;
        private string _username;
        public bool RememberUsername
        {
            get => _rememberUsername;
            set
            {
                _rememberUsername = value;
                OnPropertyChanged(_rememberUsername);
            }
        }

        public string Username
        {
            get => _username;
            set
            {
                _username = value;
                OnPropertyChanged(_username);
            }
        }

        public SignInSettings(ISettingsService settingsService) : base(settingsService)
        {
        }
    }
}