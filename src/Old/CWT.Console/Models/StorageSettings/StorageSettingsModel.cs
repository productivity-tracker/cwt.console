﻿using System;
using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.StorageSettings;
using DynamicData.Binding;

namespace CWT.Viewer.Models.StorageSettings
{
    public class StorageSettingsModel : AbstractNotifyPropertyChanged, IEquatable<StorageSettingsModel>
    {
        private string _id;
        private string _name;
        private ConnectionStatusDto _status;
        private StorageTypeDto _type;
        private string _endpoint;
        private double _usage;
        private double _capacity;

        public string Id
        {
            get => _id;
            set => SetAndRaise(ref _id, value);
        }

        public string Name
        {
            get => _name;
            set => SetAndRaise(ref _name, value);
        }

        public StorageTypeDto Type
        {
            get => _type;
            set => SetAndRaise(ref _type, value);
        }

        public string Endpoint
        {
            get => _endpoint;
            set => SetAndRaise(ref _endpoint, value);
        }

        public ConnectionStatusDto Status
        {
            get => _status;
            set => SetAndRaise(ref _status, value);
        }

        public double Usage
        {
            get => _usage;
            set => SetAndRaise(ref _usage, value);
        }

        public double Capacity
        {
            get => _capacity;
            set => SetAndRaise(ref _capacity, value);
        }

        public bool Equals(StorageSettingsModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(_id, other._id) && string.Equals(_name, other._name) && _usage.Equals(other._usage) &&
                   string.Equals(_endpoint, other._endpoint) && _capacity.Equals(other._capacity) && _type == other._type &&
                   _status == other._status;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((StorageSettingsModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _id != null ? _id.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (_name != null ? _name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _usage.GetHashCode();
                hashCode = (hashCode * 397) ^ (_endpoint != null ? _endpoint.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _capacity.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)_type;
                hashCode = (hashCode * 397) ^ (int)_status;
                return hashCode;
            }
        }

        public static bool operator==(StorageSettingsModel left, StorageSettingsModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(StorageSettingsModel left, StorageSettingsModel right)
        {
            return !Equals(left, right);
        }
    }
}