﻿using System;
using DSFramework.Timing;
using DynamicData.Binding;

namespace CWT.Viewer.Models.Timeline
{
    public class DateModel : AbstractNotifyPropertyChanged, IEquatable<DateModel>
    {
        public Date Date { get; }

        public string DisplayText => Date.ToString();

        public DateModel(Date date)
        {
            Date = date;
        }

        public bool Equals(DateModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date.Equals(other.Date);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DateModel)obj);
        }

        public override int GetHashCode()
        {
            return Date.GetHashCode();
        }

        public static bool operator==(DateModel left, DateModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(DateModel left, DateModel right)
        {
            return !Equals(left, right);
        }
    }
}