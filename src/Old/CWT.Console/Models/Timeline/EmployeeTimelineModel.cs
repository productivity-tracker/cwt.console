﻿using CWT.Viewer.Models.Employees;

namespace CWT.Viewer.Models.Timeline
{
    public class EmployeeTimelineModel : TimelineModel
    {
        private EmployeeModel _employee;

        public EmployeeModel Employee
        {
            get => _employee;
            set => SetAndRaise(ref _employee, value);
        }

        public EmployeeTimelineModel(EmployeeModel employee, DictionaryWithReturnValue<string, Timeline> timelines) 
            : base(timelines)
        {
            Employee = employee;
        }
    }
}