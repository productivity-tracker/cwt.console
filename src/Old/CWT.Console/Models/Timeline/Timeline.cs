﻿using DSFramework.Timing;
using DynamicData.Binding;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CWT.Viewer.Models.Timeline
{
    public class Timeline : AbstractNotifyPropertyChanged, IEquatable<Timeline>
    {
        private Date _date;
        private DateTimeRange _dayTimeRangeCommon;
        private IList<DateTimeRange> _timeRanges;

        public Date Date
        {
            get => _date;
            set => SetAndRaise(ref _date, value);
        }

        public DateTimeRange DayTimeRangeCommon
        {
            get => _dayTimeRangeCommon;
            set
            {
                SetAndRaise(ref _dayTimeRangeCommon, value);
                Date = _dayTimeRangeCommon.StartTime.Date;
            }
        }

        public IList<DateTimeRange> TimeRanges
        {
            get => _timeRanges;
            set
            {
                SetAndRaise(ref _timeRanges, value);
                DayTimeRangeCommon = GetTimeRange(_timeRanges);
            }
        }

        public Timeline([NotNull] IEnumerable<DateTimeRange> timeRanges)
        {
            TimeRanges = timeRanges.ToList();
        }

        private static DateTimeRange GetTimeRange([NotNull] IEnumerable<DateTimeRange> timeRanges)
        {
            var inputRanges = timeRanges.OrderBy(d => d.StartTime).ToList();
            if (!inputRanges.Any())
            {
                return null;
            }

            var startTime = inputRanges.First().StartTime;
            var endTime = inputRanges.Last().EndTime;

            return new DateTimeRange(startTime, endTime);
        }
        
        #region Equality Members

        public bool Equals(Timeline other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Date.Equals(other.Date) && Equals(DayTimeRangeCommon, other.DayTimeRangeCommon);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((Timeline)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Date.GetHashCode() * 397) ^ (DayTimeRangeCommon != null ? DayTimeRangeCommon.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Timeline left, Timeline right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Timeline left, Timeline right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}