﻿using DynamicData.Binding;
using System;

namespace CWT.Viewer.Models.Timeline
{
    public class TimelineModel : AbstractNotifyPropertyChanged, IEquatable<TimelineModel>
    {
        private DictionaryWithReturnValue<string, Timeline> _timelines;

        public DictionaryWithReturnValue<string, Timeline> Timelines
        {
            get => _timelines;
            set => SetAndRaise(ref _timelines, value);
        }

        public TimelineModel(DictionaryWithReturnValue<string, Timeline> timelines)
        {
            _timelines = timelines;
        }

        #region Equality Members

        public bool Equals(TimelineModel other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Equals(_timelines, other._timelines);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((TimelineModel)obj);
        }

        public override int GetHashCode()
        {
            return _timelines != null ? _timelines.GetHashCode() : 0;
        }

        public static bool operator ==(TimelineModel left, TimelineModel right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TimelineModel left, TimelineModel right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}