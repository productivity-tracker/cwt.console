﻿using System;

namespace CWT.Viewer.Models.UserAgents
{
    public class UserAgentModel : IEquatable<UserAgentModel>
    {
        public string EntityId { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public string Domain { get; set; }
        public string UniqueName { get; set; }
        public bool IsOnline { get; set; }
        public DateTime LastLoggedOnDate { get; set; }
        public string EmployeeName { get; set; }
        public bool IsNew { get; set; }

        public UserAgentModel()
        {
            
        }

        public UserAgentModel(
            string entityId,
            string displayName,
            string userName,
            string domain,
            string uniqueName,
            bool isOnline,
            DateTime lastLoggedOnDate,
            string employeeName,
            bool isNew)
        {
            EntityId = entityId;
            DisplayName = displayName;
            UserName = userName;
            Domain = domain;
            UniqueName = uniqueName;
            IsOnline = isOnline;
            LastLoggedOnDate = lastLoggedOnDate;
            EmployeeName = employeeName;
            IsNew = isNew;
        }

        public bool Equals(UserAgentModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return string.Equals(EntityId, other.EntityId) && string.Equals(DisplayName, other.DisplayName) &&
                   string.Equals(UserName, other.UserName) && string.Equals(Domain, other.Domain) && string.Equals(UniqueName, other.UniqueName) &&
                   IsOnline == other.IsOnline && LastLoggedOnDate.Equals(other.LastLoggedOnDate) &&
                   string.Equals(EmployeeName, other.EmployeeName) && IsNew == other.IsNew;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((UserAgentModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EntityId != null ? EntityId.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (DisplayName != null ? DisplayName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Domain != null ? Domain.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (UniqueName != null ? UniqueName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsOnline.GetHashCode();
                hashCode = (hashCode * 397) ^ LastLoggedOnDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (EmployeeName != null ? EmployeeName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsNew.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator==(UserAgentModel left, UserAgentModel right)
        {
            return Equals(left, right);
        }

        public static bool operator!=(UserAgentModel left, UserAgentModel right)
        {
            return !Equals(left, right);
        }
    }
}