﻿namespace CWT.Viewer.Services.Auth
{
    public class AuthUser
    {
        public string UserName { get; private set; }
        public string DisplayName { get; private set; }
        public string Email { get; private set; }
        public string AccessToken { get; private set; }
        public string[] Roles { get; private set; }
        public string[] Permissions { get; private set; }
    }
}