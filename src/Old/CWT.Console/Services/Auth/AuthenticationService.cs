﻿using System;
using System.Threading.Tasks;

namespace CWT.Viewer.Services.Auth
{
    public class AuthenticationService : IAuthenticationService
    {
        public event Action LoginSuccessful;
        public async Task LogIn(UserCredential userCredential)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            LoginSuccessful?.Invoke();
        }
    }
}