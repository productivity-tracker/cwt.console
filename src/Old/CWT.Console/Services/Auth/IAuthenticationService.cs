﻿using System;
using System.Threading.Tasks;

namespace CWT.Viewer.Services.Auth
{
    public interface IAuthenticationService : IService
    {
        event Action LoginSuccessful;

        Task LogIn(UserCredential userCredential);
    }
}