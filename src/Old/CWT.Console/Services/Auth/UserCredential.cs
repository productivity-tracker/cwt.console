﻿using System;

namespace CWT.Viewer.Services.Auth
{
    public class UserCredential : IDisposable
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public UserCredential(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public void Dispose()
        {
            Username = null;
            Password = null;
        }
    }
}