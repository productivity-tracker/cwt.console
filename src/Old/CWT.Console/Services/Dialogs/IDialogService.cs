﻿using System;
using System.Threading.Tasks;
using CWT.Viewer.Models;
using CWT.Viewer.ViewModels;

namespace CWT.Viewer.Services.Dialogs
{
    public interface IDialogService
    {
        Task<object> Show<TViewModel>(IDialogIdentifier identifier, TViewModel viewModel, Action afterOpenedCallback, Action afterClosedCallback)
            where TViewModel : IViewModel;

        Task<MessageBoxButtons> ShowMessageBox<TViewModel>(IDialogIdentifier identifier, string title, TViewModel viewModel, MessageBoxButtons buttons)
            where TViewModel : IViewModel;

        void UpdateContent<TViewModel>(IDialogIdentifier identifier, TViewModel viewModel)
            where TViewModel : IViewModel;

        Task<TResult> Show<TViewModel, TResult>(IDialogIdentifier identifier, TViewModel viewModel)
            where TViewModel : IViewModel;

        Task<object> Show<TViewModel>(IDialogIdentifier identifier, TViewModel viewModel)
            where TViewModel : IViewModel;
    }
}