﻿using CWT.Viewer.ViewModels;

namespace CWT.Viewer.Services
{
    public interface IExceptionDialogService : IService
    {
        void Show(IExceptionViewModel viewModel);
    }
}