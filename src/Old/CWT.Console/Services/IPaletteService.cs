﻿using System.Collections.Generic;
using MaterialDesignColors;

namespace CWT.Viewer.Services
{
    public interface IPaletteService : IService
    {
        IEnumerable<Swatch> Swatches { get; }
        void ChangeBackground(bool isDark);
        void ApplyPalette(Swatch swatch);
        void ApplyCurrentPalette();
    }
}