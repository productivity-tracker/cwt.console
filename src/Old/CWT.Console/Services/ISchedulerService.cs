﻿using System.Reactive.Concurrency;

namespace CWT.Viewer.Services
{
    public interface ISchedulerService : IService
    {
        IScheduler Dispatcher { get; }

        IScheduler Current { get; }

        IScheduler Background { get; }

        IScheduler TaskPool { get; }

        IScheduler EventLoop { get; }

        IScheduler NewThread { get; }

        IScheduler StaThread { get; }
    }
}