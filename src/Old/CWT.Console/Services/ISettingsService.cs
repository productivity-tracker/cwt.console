﻿using System;

namespace CWT.Viewer.Services
{
    public interface ISettingsService : IService
    {
        object LoadSettings(Type type, ISettingsService settingsService);
        void SetProperty<T>(string propertyName, T value);
        T GetProperty<T>(string propertyName);
    }
}