﻿using CWT.Viewer.Infrastructure.InteractionRequest;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;

namespace CWT.Viewer.Services.ScreenshotsHistories
{
    public interface IScreenshotsHistoryColumnsService : IService
    {
        InteractionRequest<AddEmployeeColumnNotification> AddEmployeeColumn { get; }
        InteractionRequest<AddDateColumnNotification> AddDateColumn { get; }
    }
}