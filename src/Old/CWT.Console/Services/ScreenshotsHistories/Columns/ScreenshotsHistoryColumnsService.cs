﻿using CWT.Viewer.Infrastructure.InteractionRequest;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;

namespace CWT.Viewer.Services.ScreenshotsHistories
{
    public class ScreenshotsHistoryColumnsService : IScreenshotsHistoryColumnsService
    {
        private static ScreenshotsHistoryColumnsService _instance;

        public static ScreenshotsHistoryColumnsService Instance => _instance ??= new ScreenshotsHistoryColumnsService();

        public InteractionRequest<AddEmployeeColumnNotification> AddEmployeeColumn { get; }
        public InteractionRequest<AddDateColumnNotification> AddDateColumn { get; }
        public InteractionRequest<ClearAllColumnsNotification> ClearAllColumns { get; }

        public ScreenshotsHistoryColumnsService()
        {
            AddDateColumn = new InteractionRequest<AddDateColumnNotification>();
            AddEmployeeColumn = new InteractionRequest<AddEmployeeColumnNotification>();
            ClearAllColumns = new InteractionRequest<ClearAllColumnsNotification>();
        }
    }
}