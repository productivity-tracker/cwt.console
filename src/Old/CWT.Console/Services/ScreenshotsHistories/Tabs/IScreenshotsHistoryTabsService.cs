﻿using CWT.Viewer.Infrastructure.InteractionRequest;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;

namespace CWT.Viewer.Services.ScreenshotsHistories
{
    public interface IScreenshotsHistoryTabsService : IService
    {
        InteractionRequest<OpenScreenshotsHistoryNotification> OpenScreenshotsHistory { get; }
    }
}