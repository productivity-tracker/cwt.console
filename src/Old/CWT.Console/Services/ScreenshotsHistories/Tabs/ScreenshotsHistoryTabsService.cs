﻿using CWT.Viewer.Infrastructure.InteractionRequest;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;

namespace CWT.Viewer.Services.ScreenshotsHistories
{
    public class ScreenshotsHistoryTabsService : IScreenshotsHistoryTabsService
    {
        private static ScreenshotsHistoryTabsService _instance;

        public static ScreenshotsHistoryTabsService Instance => _instance ??= new ScreenshotsHistoryTabsService();

        public InteractionRequest<OpenScreenshotsHistoryNotification> OpenScreenshotsHistory { get; }

        public ScreenshotsHistoryTabsService()
        {
            OpenScreenshotsHistory = new InteractionRequest<OpenScreenshotsHistoryNotification>();
        }
    }
}