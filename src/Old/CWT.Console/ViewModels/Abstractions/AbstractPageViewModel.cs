﻿using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using DSFramework.GuardToolkit;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public class AbstractPageViewModel : AbstractViewModel
    {
        protected IDialogIdentifier DialogIdentifier = new DialogIdentifier(GlobalConstants.MAIN_DIALOG_HOST_ID);

        private bool _isBusy;

        public bool IsBusy
        {
            get => _isBusy;
            set => SetAndRaise(ref _isBusy, value);
        }

        public INotificationMessageQueue NotificationMessageQueue { get; }

        protected IDialogService DialogService { get; }

        protected AbstractPageViewModel(IDialogService dialogService,
                                        INotificationMessageQueue notificationMessageQueue) : this(notificationMessageQueue)
        {
            DialogService = Check.NotNull(dialogService, nameof(dialogService));
        }

        protected AbstractPageViewModel(INotificationMessageQueue notificationMessageQueue)
        {
            NotificationMessageQueue = Check.NotNull(notificationMessageQueue, nameof(notificationMessageQueue));
        }
    }
}