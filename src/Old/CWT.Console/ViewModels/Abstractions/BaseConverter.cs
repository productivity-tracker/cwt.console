﻿using DSFramework.Domain.Abstractions.Converters;
using Newtonsoft.Json;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public abstract class BaseConverter
    {
        protected TEntity DeepClone<TEntity>(TEntity input)
        {
            return JsonConvert.DeserializeObject<TEntity>(JsonConvert.SerializeObject(input));
        }
    }
}