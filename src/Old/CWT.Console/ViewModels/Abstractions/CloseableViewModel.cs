﻿using CWT.Viewer.ViewModels.Abstractions;

namespace CWT.Viewer.ViewModels
{
    public abstract class CloseableViewModel : AbstractViewModel, ICloseableViewModel
    {
        private bool _isOpen;

        public bool IsOpen
        {
            get => _isOpen;
            set => SetAndRaise(ref _isOpen, value);
        }
    }
}