﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using CWT.Sdk.Providers.Base;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using DSFramework.Contracts.Common.Models;
using DSFramework.Domain.Abstractions.Converters;
using DSFramework.GuardToolkit;
using DSFramework.Threading;
using DynamicData;
using Microsoft.Extensions.Logging;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public abstract class
        DynamicListViewModel<TDto, TModel, TConverter, TProvider> : AbstractPageViewModel, IDynamicListViewModel<TModel>, IDisposable
        where TDto : IEntityHolder
        where TModel : IEquatable<TModel>
        where TConverter : class, IConverter<TDto, TModel>
        where TProvider : class, IGenericProvider<string, TDto>
    {
        private readonly TProvider _provider;
        private readonly SourceCache<TDto, string> _source;
        private readonly GenericLock<string> _genericLock = new GenericLock<string>();

        private readonly ReadOnlyObservableCollection<TModel> _data;

        private readonly IDisposable _cleanUp;

        private Guid _changeHandlerGuid = Guid.Empty;
        private Guid _cleanupHandlerGuid = Guid.Empty;

        private string _searchText;

        private TModel _selectedItem;
        public ReadOnlyObservableCollection<TModel> Data => _data;

        public TModel SelectedItem
        {
            get => _selectedItem;
            set => SetAndRaise(ref _selectedItem, value);
        }

        public string SearchText
        {
            get => _searchText;
            set => SetAndRaise(ref _searchText, value);
        }

        public ICommand LoadDataCommand { get; }

        protected ILogger Logger { get; }
        protected TConverter Converter { get; }

        protected DynamicListViewModel(ILogger logger,
                                       TConverter converter,
                                       TProvider provider,
                                       IDialogService dialogService,
                                       INotificationMessageQueue notificationMessageQueue)
            : base(dialogService, notificationMessageQueue)
        {
            Logger = logger;
            _provider = Check.NotNull(provider, nameof(provider));
            Converter = Check.NotNull(converter, nameof(converter));
            _source = new SourceCache<TDto, string>(keySelector: e => e.Id);
            _cleanUp = GetSubscription(_source, Converter, out _data);
            LoadDataCommand = new RelayCommandAsync(LoadData);
        }

        public void StartSubscribe()
        {
            if (_changeHandlerGuid != Guid.Empty)
            {
                return;
            }

            _changeHandlerGuid = _provider.RegisterChangeHandler(handler: async dto => await HandleUpdate(dto));
            _cleanupHandlerGuid = _provider.RegisterCleanUpHandler(handler: async dto => await HandleRemove(dto));
        }

        public void StopSubscribe()
        {
            _provider?.RemoveChangeHandler(_changeHandlerGuid);
            _provider?.RemoveCleanUpHandler(_cleanupHandlerGuid);
            _changeHandlerGuid = Guid.Empty;
        }

        public virtual void Dispose()
        {
            StopSubscribe();
            _cleanUp?.Dispose();
            _source?.Dispose();
        }

        protected abstract IDisposable GetSubscription(SourceCache<TDto, string> source,
                                                       TConverter converter,
                                                       out ReadOnlyObservableCollection<TModel> data);

        protected virtual async Task LoadData()
        {
            IsBusy = true;
            try
            {
                var items = await _provider.GetAll();
                _source.AddOrUpdate(items);
                StartSubscribe();
            }
            catch (Exception e)
            {
                Logger.LogError(e, $"Error occurred while {nameof(LoadData)}. {e.Message}");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private Task HandleUpdate(TDto dto)
            => Task.Run(action: () =>
            {
                using (_genericLock.Lock(dto.Id))
                {
                    _source.AddOrUpdate(dto);
                }
            });

        private Task HandleRemove(TDto dto)
            => Task.Run(action: () =>
            {
                using (_genericLock.Lock(dto.Id))
                {
                    _source.Remove(dto);
                }
            });
    }
}