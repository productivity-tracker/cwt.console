﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CWT.Sdk.Facades.Base;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using DSFramework.Contracts.Common.Models;
using DSFramework.Domain.Abstractions.Converters;
using DSFramework.GuardToolkit;
using Microsoft.Extensions.Logging;
using TranslateMe;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public abstract class
        EditableListViewModel<TEntity, TEntityHolder, TWriteCommand, TModel, TConverter, TFacade> : DynamicListViewModel<TEntityHolder, TModel, TConverter, TFacade>, IEditableListViewModel<TModel>
        where TModel : IEquatable<TModel>, IMemento<TModel>, IHaveDefault<TModel>, new()
        where TEntityHolder : EntityHolder<TEntity>, IEntityHolder
        where TConverter : class, IConverter<TEntityHolder, TModel>
        where TFacade : class, IGenericFacade<TEntity, TEntityHolder, TWriteCommand>
    {
        protected TFacade Facade { get; }

        public ICommand NewCommand { get; }
        public ICommand EditCommand { get; }
        public ICommand CloneCommand { get; }
        public ICommand DeleteCommand { get; }

        protected abstract string EditItemTitleTextId { get; }
        protected abstract string NewItemTitleTextId { get; }

        protected EditableListViewModel(ILogger logger,
                                        TConverter converter,
                                        TFacade facade,
                                        IDialogService dialogService,
                                        INotificationMessageQueue notificationMessageQueue)
            : base(logger, converter, facade, dialogService, notificationMessageQueue)
        {
            Facade = Check.NotNull(facade, nameof(facade));
            NewCommand = new RelayCommandAsync(New);
            EditCommand = new RelayCommandAsync(Edit, canExecute: o => SelectedItem != null);
            CloneCommand = new RelayCommandAsync(Edit, canExecute: o => SelectedItem != null);
            DeleteCommand = new RelayCommandAsync(Edit, canExecute: o => SelectedItem != null);
        }

        protected abstract Task<MessageBoxButtons> ShowEditDialog(string titleTextId, TModel model);

        protected virtual async Task New()
        {
            var model = new TModel().GetDefault();
            var result = await ShowEditDialog(NewItemTitleTextId, model);

            if (result == MessageBoxButtons.Cancel)
            {
                return;
            }

            await TryUpdate(model);
        }

        protected virtual async Task Edit()
        {
            var clone = SelectedItem.SaveState();
            var result = await ShowEditDialog(EditItemTitleTextId, clone);

            if (result == MessageBoxButtons.Cancel)
            {
                return;
            }

            await TryUpdate(clone);
        }

        protected virtual async Task TryUpdate(TModel model)
        {
            IsBusy = true;
            try
            {
                var dto = Converter.Convert(model);
                var result = await Facade.Update(dto.Id, dto.Data);

                NotificationMessageQueue.Enqueue(result ? TM.Tr(textId: "ChangesSavedSuccessfully") : TM.Tr(textId: "ChangesNotSaved"));
            }
            catch (Exception e)
            {
                Logger.LogError(e, $"Error occurred while {nameof(TryUpdate)}. {e.Message}");
                NotificationMessageQueue.Enqueue(e.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}