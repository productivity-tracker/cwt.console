﻿namespace CWT.Viewer.ViewModels
{
    public interface ICloseableViewModel : IViewModel
    {
        bool IsOpen { get; set; }
    }
}