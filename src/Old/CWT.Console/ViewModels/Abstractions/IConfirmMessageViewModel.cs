﻿namespace CWT.Viewer.ViewModels
{
    public interface IConfirmMessageViewModel : ICloseableViewModel, ITransientViewModel
    {
        string Message { get; set; }
    }
}