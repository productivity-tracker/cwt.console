﻿using System.Collections.ObjectModel;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public interface IDynamicListViewModel<TModel>
    {
        string SearchText { get; set; }
        TModel SelectedItem { get; set; }
        ReadOnlyObservableCollection<TModel> Data { get; }
        ICommand LoadDataCommand { get; }
    }
}