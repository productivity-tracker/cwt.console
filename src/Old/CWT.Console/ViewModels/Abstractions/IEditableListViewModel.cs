﻿using System.Windows.Input;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public interface IEditableListViewModel<TModel> : IDynamicListViewModel<TModel>
    {
        ICommand NewCommand { get; }
        ICommand EditCommand { get; }
        ICommand CloneCommand { get; }
        ICommand DeleteCommand { get; }
    }
}