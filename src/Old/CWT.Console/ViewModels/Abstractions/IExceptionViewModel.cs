﻿using System.Windows.Input;

namespace CWT.Viewer.ViewModels
{
    public interface IExceptionViewModel : IViewModel
    {
        bool IsOpen { get; set; }
        string Message { get; }
        ICommand CopyCommand { get; }
        ICommand OpenLogFolderCommand { get; }
        ICommand ContinueCommand { get; }
        ICommand RestartCommand { get; }
        ICommand ExitCommand { get; }
    }
}