﻿namespace CWT.Viewer.ViewModels
{
    public interface IInfoMassageViewModel : ICloseableViewModel
    {
        string Message { get; set; }
    }
}