﻿using System.Windows.Input;

// ReSharper disable once CheckNamespace
namespace CWT.Viewer.ViewModels
{
    public interface ISignInViewModel : IViewModel
    {
        string Username { get; set; }
        string Password { get; set; }
        ICommand LoginCommand { get; }

        IInfoMassageViewModel MessageViewModel { get; }
    }
}