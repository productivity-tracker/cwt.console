﻿using System.Collections.ObjectModel;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public interface IViewModelContext<TModel>
    {
        ReadOnlyObservableCollection<TModel> Items { get; }

        string SearchText { get; set; }
    }
}