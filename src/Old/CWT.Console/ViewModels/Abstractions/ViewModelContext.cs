﻿using System.Collections.ObjectModel;
using DynamicData.Binding;

namespace CWT.Viewer.ViewModels.Abstractions
{
    public abstract class ViewModelContext<TModel> : AbstractNotifyPropertyChanged, IViewModelContext<TModel>
    {
        private string _searchText;

        public abstract ReadOnlyObservableCollection<TModel> Items { get; }

        public string SearchText
        {
            get => _searchText;
            set => SetAndRaise(ref _searchText, value);
        }
    }
}