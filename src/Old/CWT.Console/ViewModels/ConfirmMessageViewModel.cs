﻿namespace CWT.Viewer.ViewModels
{
    public class ConfirmMessageViewModel : IConfirmMessageViewModel
    {
        public bool IsOpen { get; set; }
        public string Message { get; set; }
    }
}