﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.AgentSettings;
using CWT.Viewer.Models;
using CWT.Viewer.Models.AgentSettings;
using System;
using System.Linq;

namespace CWT.Viewer.ViewModels.ControlPanel.AgentSettings
{
    public static class AgentSettingsConverter
    {
        public static AgentSettingsModel Convert(EntityHolderOfAgentSettingsDto holder)
        {
            var entity = holder?.Data;
            if (entity == null)
            {
                return null;
            }

            var screenshotsSettings = entity.ScreenshotsSettings;
            var model = new AgentSettingsModel
            {
                Id = holder.Id,
                Name = entity.Name,
                IsDefault = entity.IsDefault,
                CaptureDesktop = screenshotsSettings.EnableCaptureDesktop,
                //Storage = new StorageModel(entity.StorageId, GetStorageName(entity.StorageId)),
                ScreenshotsFrequency = screenshotsSettings.ScreenshotsInterval,
                FrameFrequency = screenshotsSettings.ScreenshotsInterval.GetPeriod().ToFrequency(),
                Quality = screenshotsSettings.Quality,
                //BucketName = entity.BasePath,
                ImageFormat = (ImageFormat)Enum.Parse(typeof(ImageFormat), entity.ScreenshotsSettings.ImgFormat, true),
                ScreenshotsHistoryTtl = screenshotsSettings.ScreenshotsHistoryTtl,
                ScreenshotHistoryPeriod = screenshotsSettings.ScreenshotsHistoryTtl.GetPeriod(),
                ObservedProcesses = string.Join(",", screenshotsSettings.ObservedProcesses),
                Details = GetDetails(holder)
            };

            return model;
        }

        public static EntityHolderOfAgentSettingsDto Convert(AgentSettingsModel model)
        {
            var screenshotSettings = new ScreenshotsSettingsDto
            {
                EnableCaptureDesktop = model.CaptureDesktop,
                ImgFormat = model.ImageFormat.ToString(),
                ObservedProcesses = model.ObservedProcesses?.Split(',', ';').ToList(),
                Quality = model.Quality,
                ScreenshotsHistoryTtl = model.ScreenshotsHistoryTtl,
                ScreenshotsInterval = model.ScreenshotsFrequency
            };
            var snapshot = new EntityHolderOfAgentSettingsDto
            {
                Id = model.Id ?? string.Empty,
                Data = new AgentSettingsDto
                {
                    //StorageId = model.Storage?.Id,
                    Name = model.Name,
                    //BasePath = model.BucketName,
                    IsDefault = model.IsDefault,
                    ScreenshotsSettings = screenshotSettings
                }
            };

            return snapshot;
        }

        private static string GetStorageName(string storageId)
        {
            return string.Empty;
        }

        private static EntityDetails GetDetails(EntityHolderOfAgentSettingsDto holder)
        {
            return new EntityDetails(holder.CreatedBy,
                                     holder.CreatedDate.ToLocalTime(),
                                     holder.ModifiedBy,
                                     holder.ModifiedDate?.ToLocalTime());
        }
    }
}