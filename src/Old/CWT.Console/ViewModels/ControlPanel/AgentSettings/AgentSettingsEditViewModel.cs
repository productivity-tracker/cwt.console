﻿using System;
using System.Collections.Generic;
using System.Linq;
using CWT.Viewer.Models;
using CWT.Viewer.Models.AgentSettings;

namespace CWT.Viewer.ViewModels.ControlPanel.AgentSettings
{
    public class AgentSettingsEditViewModel : IAgentSettingsEditViewModel
    {
        public AgentSettingsModel AgentSettingsSnapshot { get; }

        public IEnumerable<ImageFormat> ScreenShotImageFormats { get; } = Enum.GetValues(typeof(ImageFormat)).Cast<ImageFormat>();
        public IEnumerable<FrameFrequency> ScreenShotFrequency { get; } = Enum.GetValues(typeof(FrameFrequency)).Cast<FrameFrequency>();
        public IEnumerable<Period> ScreenShotPeriods { get; } = Enum.GetValues(typeof(Period)).Cast<Period>();

        public bool IsOpen { get; set; }

        public AgentSettingsEditViewModel(AgentSettingsModel snapshot)
        {
            AgentSettingsSnapshot = snapshot;
        }
    }
}