﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.AgentSettings;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Extensions;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models;
using CWT.Viewer.Models.AgentSettings;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.GuardToolkit;
using DynamicData;
using DynamicData.Binding;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels.ControlPanel.AgentSettings
{
    public class AgentSettingsViewModel : AbstractPageViewModel, IAgentSettingsViewModel, IDisposable
    {
        private readonly IAgentSettingsFacade _agentSettingsFacade;
        private readonly IStorageSettingsFacade _storageSettingsFacade;
        private readonly IDialogIdentifier _dialogIdentifier = new DialogIdentifier(GlobalConstants.MAIN_DIALOG_HOST_ID);
        private readonly AgentSettingsCaretaker _agentSettingsCaretaker = new AgentSettingsCaretaker();

        private readonly SourceCache<EntityHolderOfAgentSettingsDto, string> _agentSettingsSource =
            new SourceCache<EntityHolderOfAgentSettingsDto, string>(s => s.Id);

        private readonly SourceCache<EntityHolderOfStorageSettingsDto, string> _storagesSource =
            new SourceCache<EntityHolderOfStorageSettingsDto, string>(s => s.Id);

        private readonly ReadOnlyObservableCollection<AgentSettingsModel> _agentsSettings;
        private readonly ReadOnlyObservableCollection<StorageModel> _storages;
        private readonly IDisposable _cleanUp;

        private string _searchText;
        private AgentSettingsModel _selectedAgentSettings;

        public ICommand LoadDataCommand { get; }
        public ICommand NewCommand { get; }
        public ICommand EditCommand { get; }
        public ICommand CloneCommand { get; }
        public ICommand DeleteCommand { get; }

        public ReadOnlyObservableCollection<AgentSettingsModel> Items => _agentsSettings;
        public ReadOnlyObservableCollection<StorageModel> Storages => _storages;

        public AgentSettingsModel SelectedAgentSettings
        {
            get => _selectedAgentSettings;
            set => SetAndRaise(ref _selectedAgentSettings, value);
        }

        public string SearchText
        {
            get => _searchText;
            set => SetAndRaise(ref _searchText, value);
        }

        public AgentSettingsViewModel(IAgentSettingsFacade agentSettingsFacade,
                                      IStorageSettingsFacade storageSettingsFacade,
                                      IDialogService dialogService,
                                      INotificationMessageQueue notificationMessageQueue) : base(dialogService, notificationMessageQueue)
        {
            _agentSettingsFacade = Check.NotNull(agentSettingsFacade, nameof(agentSettingsFacade));
            _storageSettingsFacade = Check.NotNull(storageSettingsFacade, nameof(storageSettingsFacade));

            LoadDataCommand = new RelayCommand(LoadData);
            NewCommand = new RelayCommandAsync(Add);
            EditCommand = new RelayCommandAsync(Edit, o => SelectedAgentSettings != null);
            CloneCommand = new RelayCommandAsync(Edit, o => SelectedAgentSettings != null);
            DeleteCommand = new RelayCommandAsync(Edit, o => SelectedAgentSettings != null);

            var textFilter = this.WhenValueChanged(vm => vm.SearchText).Throttle(TimeSpan.FromMilliseconds(250)).Select(BuildFilter);

            var agentSettingsLoader = _agentSettingsSource.Connect()
                                                          .Transform(AgentSettingsConverter.Convert)
                                                          .Filter(textFilter)
                                                          .Sort(SortExpressionComparer<AgentSettingsModel>.Ascending(s => s.Name))
                                                          .ObserveOnDispatcher()
                                                          .Bind(out _agentsSettings)
                                                          .DisposeMany()
                                                          .Subscribe();

            var storagesLoader = _storagesSource.Connect()
                                                .Transform(dto => new StorageModel(string.Empty, string.Empty))
                                                .ObserveOnDispatcher()
                                                .Bind(out _storages)
                                                .DisposeMany()
                                                .Subscribe();

            _cleanUp = new CompositeDisposable(agentSettingsLoader, storagesLoader);
        }

        public void Dispose()
        {
            _cleanUp?.Dispose();
        }

        private async void LoadData()
        {
            IsBusy = true;

            await Task.Run(() =>
            {
                var newEntity = new EntityHolderOfAgentSettingsDto
                {
                    Id = Guid.NewGuid().ToString("N"),
                    ModifiedBy = "Denis",
                    ModifiedDate = DateTime.UtcNow,
                    Data = new AgentSettingsDto
                    {
                        Name = "Test",
                        //BasePath = "BasePath",
                        //StorageId = Guid.NewGuid().ToString("N"),
                        IsDefault = true,
                        ScreenshotsSettings = new ScreenshotsSettingsDto
                        {
                            EnableCaptureDesktop = true,
                            Quality = 80L,
                            ImgFormat = "Jpg",
                            ScreenshotsHistoryTtl = TimeSpan.FromDays(3),
                            ScreenshotsInterval = TimeSpan.FromSeconds(30),
                            ObservedProcesses = new[] { "mstsc" }.ToList()
                        }
                    }
                };
                _agentSettingsSource.AddOrUpdate(newEntity);
            });

            IsBusy = false;
        }

        private Func<AgentSettingsModel, bool> BuildFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return agentSettings => true;
            }

            return s => s.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }

        private async Task Edit()
        {
            try
            {
                var cloneSettings = SelectedAgentSettings.SaveState();
                var result = await ShowEditDialog("EditSettings", cloneSettings);

                if (result == MessageBoxButtons.Cancel)
                {
                    return;
                }

                IsBusy = true;
                await TryUpdate(cloneSettings);
            }
            catch (Exception e)
            {
                NotificationMessageQueue.Enqueue(e.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task TryUpdate(AgentSettingsModel agentSettings)
        {
            var dto = AgentSettingsConverter.Convert(agentSettings);
            var result = await _agentSettingsFacade.Update(dto.Id, dto.Data);

            if (result)
            {
                // Todo
            }
        }

        private async Task Add()
        {
            try
            {
                var settings = AgentSettingsModel.Default();
                var result = await ShowEditDialog("NewSettings", settings);

                if (result == MessageBoxButtons.Cancel)
                {
                    return;
                }

                IsBusy = true;
                await TryUpdate(settings);
            }
            catch (Exception e)
            {
                NotificationMessageQueue.Enqueue(e.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task<MessageBoxButtons> ShowEditDialog(string title, AgentSettingsModel settings)
        {
            IAgentSettingsEditViewModel vm = new AgentSettingsEditViewModel(settings);
            return await DialogService.ShowMessageBox(_dialogIdentifier,
                                                       title,
                                                       vm,
                                                       MessageBoxButtons.SaveCancel);
        }
    }
}