﻿using System.Collections.Generic;
using CWT.Viewer.Models;
using CWT.Viewer.Models.AgentSettings;

namespace CWT.Viewer.ViewModels.ControlPanel.AgentSettings
{
    public interface IAgentSettingsEditViewModel : ICloseableViewModel, ITransientViewModel
    {
        AgentSettingsModel AgentSettingsSnapshot { get; }

        IEnumerable<ImageFormat> ScreenShotImageFormats { get; }

        IEnumerable<FrameFrequency> ScreenShotFrequency { get; }

        IEnumerable<Period> ScreenShotPeriods { get; }
    }
}