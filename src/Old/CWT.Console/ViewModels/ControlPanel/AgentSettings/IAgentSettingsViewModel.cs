﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using CWT.Viewer.Models.AgentSettings;

namespace CWT.Viewer.ViewModels.ControlPanel.AgentSettings
{
    public interface IAgentSettingsViewModel : IViewModel
    {
        ICommand LoadDataCommand { get; }
        
        ICommand NewCommand { get; }

        ICommand EditCommand { get; }

        ICommand CloneCommand { get; }

        ICommand DeleteCommand { get; }

        ReadOnlyObservableCollection<AgentSettingsModel> Items { get; }

        ReadOnlyObservableCollection<StorageModel> Storages { get; }

        AgentSettingsModel SelectedAgentSettings { get; set; }

    }
}