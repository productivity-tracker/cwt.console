﻿using System.Collections.Generic;

namespace CWT.Viewer.ViewModels.ControlPanel
{
    public class ControlPanelViewModel : IControlPanelViewModel
    {
        public List<MenuItemViewModel> MenuItems { get; } = new List<MenuItemViewModel>();
    }
}