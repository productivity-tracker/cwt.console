﻿using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Models.Devices;
using CWT.Viewer.ViewModels.ControlPanel.Devices.Interfaces;
using DSFramework.GuardToolkit;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using CWT.Viewer.Infrastructure;

namespace CWT.Viewer.ViewModels.ControlPanel.Devices
{
    public class DeviceConverter : IDeviceConverter
    {
        private const string UNKNOWN = "Unknown";

        private readonly IUserAgentsFacade _userAgentsFacade;
        private readonly ILogger _logger;

        public DeviceConverter(ILogger logger, IUserAgentsFacade userAgentsFacade)
        {
            _userAgentsFacade = Check.NotNull(userAgentsFacade, nameof(userAgentsFacade));
            _logger = logger ?? NullLogger.Instance;
        }

        public DeviceModel Convert(EntityHolderOfDeviceDto holderOfDeviceDto)
        {
            var deviceDto = holderOfDeviceDto?.Data;
            if (deviceDto == null)
            {
                return null;
            }

            var useragentId = deviceDto.LastLoggedOnUserAgentId;
            var lastUserAgentName = string.IsNullOrWhiteSpace(useragentId) ? string.Empty : GetUserAgentName(useragentId) ?? UNKNOWN;

            var model = new DeviceModel(holderOfDeviceDto.Id,
                                        deviceDto.Metadata?.DeviceName,
                                        deviceDto.Metadata?.Domain,
                                        deviceDto.Metadata?.IpAddress,
                                        deviceDto.Metadata?.OperatingSystem,
                                        deviceDto.AgentVersion,
                                        deviceDto.IsOnline,
                                        deviceDto.LastConnectionDate,
                                        lastUserAgentName);
            return model;
        }

        public EntityHolderOfDeviceDto Convert(DeviceModel source)
        {
            throw new NotImplementedException();
        }

        private string GetUserAgentName(string userAgentId)
        {
            try
            {
                var userAgentHolder = AsyncHelper.RunSync(() => _userAgentsFacade.Get(userAgentId));
                var userAgentName = userAgentHolder?.Data?.DisplayName;
                return userAgentName;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null;
            }
        }
    }
}