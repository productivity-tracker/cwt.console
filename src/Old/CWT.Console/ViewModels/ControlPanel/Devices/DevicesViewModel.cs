﻿using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Extensions;
using CWT.Viewer.Models.Devices;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using CWT.Viewer.ViewModels.ControlPanel.Devices.Interfaces;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;

namespace CWT.Viewer.ViewModels.ControlPanel.Devices
{
    public class DevicesViewModel : DynamicListViewModel<EntityHolderOfDeviceDto, DeviceModel, IDeviceConverter, IDevicesFacade>, IDevicesViewModel
    {
        public DevicesViewModel(
            ILogger logger,
            IDeviceConverter converter,
            IDevicesFacade facade,
            IDialogService dialogService,
            INotificationMessageQueue notificationMessageQueue)
            : base(logger, converter, facade, dialogService, notificationMessageQueue)
        {
        }

        protected override IDisposable GetSubscription(
            SourceCache<EntityHolderOfDeviceDto, string> source,
            IDeviceConverter converter,
            out ReadOnlyObservableCollection<DeviceModel> data)
        {
            var textFilter = this.WhenValueChanged(vm => vm.SearchText).Throttle(TimeSpan.FromMilliseconds(250)).Select(BuildFilter);
            return source.Connect()
                         .Filter(holder => holder?.Data != null)
                         .Transform(converter.Convert)
                         .Filter(textFilter)
                         .Sort(SortExpressionComparer<DeviceModel>.Ascending(s => s.DeviceName))
                         .ObserveOnDispatcher()
                         .Bind(out data)
                         .DisposeMany()
                         .Subscribe();
        }

        private Func<DeviceModel, bool> BuildFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return agentSettings => true;
            }

            return s => s.DeviceName.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }
    }
}