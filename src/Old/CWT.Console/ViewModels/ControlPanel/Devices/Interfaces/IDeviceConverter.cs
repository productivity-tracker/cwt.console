﻿using CWT.Kernel.Contracts;
using CWT.Viewer.Models.Devices;
using DSFramework.Domain.Abstractions.Converters;

namespace CWT.Viewer.ViewModels.ControlPanel.Devices.Interfaces
{
    public interface IDeviceConverter : IConverter<EntityHolderOfDeviceDto, DeviceModel>
    {
        
    }
}