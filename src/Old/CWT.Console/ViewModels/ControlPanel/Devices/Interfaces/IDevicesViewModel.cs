﻿using CWT.Viewer.Models.Devices;
using CWT.Viewer.ViewModels.Abstractions;

namespace CWT.Viewer.ViewModels.ControlPanel.Devices.Interfaces
{
    public interface IDevicesViewModel : IDynamicListViewModel<DeviceModel>, IViewModel
    {
    }
}