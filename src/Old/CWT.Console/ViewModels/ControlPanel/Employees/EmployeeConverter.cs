﻿using System;
using CWT.Kernel.Contracts;
using CWT.Viewer.Models.Employees;
using CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces;

namespace CWT.Viewer.ViewModels.ControlPanel.Employees
{
    public class EmployeeConverter : IEmployeeConverter
    {
        public EmployeeModel Convert(EntityHolderOfEmployeeDto holder)
        {
            var employeeDto = holder?.Data;
            if (employeeDto == null)
            {
                return null;
            }

            return new EmployeeModel(holder.Id,
                                     employeeDto.DisplayName,
                                     employeeDto.JobTitle,
                                     employeeDto.IsOnline,
                                     employeeDto.LastLoggedOnDate,
                                     employeeDto.SettingsId,
                                     employeeDto.OrganizationalUnitsIds,
                                     employeeDto.Notes);
        }

        public EntityHolderOfEmployeeDto Convert(EmployeeModel source)
        {
            throw new NotImplementedException();
        }
    }
}