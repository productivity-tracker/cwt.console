﻿using CWT.Viewer.Models.Employees;
using CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces;

namespace CWT.Viewer.ViewModels.ControlPanel.Employees
{
    public class EmployeeEditViewModel : IEmployeeEditViewModel
    {
        public bool IsOpen { get; set; }

        public EmployeeModel Model { get; set; }

        public EmployeeEditViewModel(EmployeeModel model)
        {
            Model = model;
        }
    }
}