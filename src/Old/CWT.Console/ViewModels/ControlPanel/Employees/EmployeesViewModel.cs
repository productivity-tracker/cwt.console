﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.Employees;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Models;
using CWT.Viewer.Models.Employees;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Threading.Tasks;
using CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces;
using CWT.Viewer.Extensions;

namespace CWT.Viewer.ViewModels.ControlPanel.Employees
{
    public class EmployeesViewModel
        : EditableListViewModel<EmployeeDto, EntityHolderOfEmployeeDto, WriteCommandOfEmployeeDto, EmployeeModel, IEmployeeConverter, IEmployeesFacade>, IEmployeesViewModel
    {
        protected override string NewItemTitleTextId => "NewEmployee";
        protected override string EditItemTitleTextId => "EditEmployee";

        public EmployeesViewModel(
            ILogger logger,
            IEmployeeConverter converter,
            IEmployeesFacade provider,
            IDialogService dialogService,
            INotificationMessageQueue notificationMessageQueue)
            : base(logger, converter, provider, dialogService, notificationMessageQueue)
        {
        }

        protected override IDisposable GetSubscription(
            SourceCache<EntityHolderOfEmployeeDto, string> source,
            IEmployeeConverter converter,
            out ReadOnlyObservableCollection<EmployeeModel> data)
        {
            var textFilter = this.WhenValueChanged(vm => vm.SearchText).Throttle(TimeSpan.FromMilliseconds(250)).Select(BuildFilter);
            return source.Connect()
                         .Filter(holder => holder?.Data != null)
                         .Transform(converter.Convert)
                         .Filter(textFilter)
                         .Sort(SortExpressionComparer<EmployeeModel>.Ascending(s => s.Name))
                         .ObserveOnDispatcher()
                         .Bind(out data)
                         .DisposeMany()
                         .Subscribe();
        }

        protected override async Task<MessageBoxButtons> ShowEditDialog(string titleTextId, EmployeeModel model)
        {
            IEmployeeEditViewModel vm = new EmployeeEditViewModel(model);
            return await DialogService.ShowMessageBox(DialogIdentifier,
                                                      titleTextId,
                                                      vm,
                                                      MessageBoxButtons.SaveCancel);
        }

        private Func<EmployeeModel, bool> BuildFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return agentSettings => true;
            }

            return s => s.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase) 
                      || s.JobTitle.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }
    }
}