﻿using CWT.Kernel.Contracts;
using CWT.Viewer.Models.Employees;
using DSFramework.Domain.Abstractions.Converters;

namespace CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces
{
    public interface IEmployeeConverter : IConverter<EntityHolderOfEmployeeDto, EmployeeModel>
    {

    }
}