﻿using CWT.Viewer.Models.Employees;
using CWT.Viewer.ViewModels.Abstractions;

namespace CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces
{
    public interface IEmployeesViewModel : IEditableListViewModel<EmployeeModel>, IViewModel
    {

    }
}