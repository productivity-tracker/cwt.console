﻿using System.Collections.Generic;

namespace CWT.Viewer.ViewModels.ControlPanel
{
    public interface IControlPanelViewModel : IViewModel
    {
        List<MenuItemViewModel> MenuItems { get; }
    }
}