﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.OrganizationalUnits;
using CWT.Viewer.Models.OrganizationUnits;
using DSFramework.Domain.Abstractions.Converters;

namespace CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces
{
    public interface IOrganizationalUnitConverter : IConverter<EntityHolderOfOrganizationalUnitDto, OrganizationalUnitModel>
    {
        OrganizationalUnitDto Convert(OrganizationalUnitModel dto);
    }
}