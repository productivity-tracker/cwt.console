﻿using CWT.Viewer.Models.OrganizationUnits;

namespace CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces
{
    public interface IOrganizationalUnitEditViewModel : ICloseableViewModel
    {
        OrganizationalUnitModel Model { get; set; }
    }
}