﻿using CWT.Viewer.Models.OrganizationUnits;
using CWT.Viewer.ViewModels.Abstractions;

namespace CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces
{
    public interface IOrganizationalUnitViewModel : IEditableListViewModel<OrganizationalUnitModel>, IViewModel
    {
    }
}