﻿using System;
using System.Collections.Generic;
using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.OrganizationalUnits;
using CWT.Viewer.Models;
using CWT.Viewer.Models.OrganizationUnits;
using CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces;

namespace CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits
{
    public class OrganizationalUnitConverter : IOrganizationalUnitConverter
    {
        public OrganizationalUnitModel Convert(EntityHolderOfOrganizationalUnitDto source)
        {
            var id = source?.Id;
            var orgUnit = source?.Data;
            if (id == null || orgUnit == null)
            {
                return null;
            }

            return GetNode(orgUnit, orgUnit.Id, parent: null, isRoot: true);
        }

        public EntityHolderOfOrganizationalUnitDto Convert(OrganizationalUnitModel source) => throw new NotImplementedException();

        private static OrganizationalUnitModel GetNode(OrganizationalUnitDto orgUnit,
                                                       string id = null,
                                                       Node parent = null,
                                                       bool isRoot = false)
        {
            var node = new OrganizationalUnitModel(orgUnit.Name, id, isRoot);
            if (!isRoot && parent != null)
            {
                node.Parent.Add(parent);
            }

            var children = orgUnit.Subsidiaries ?? new List<OrganizationalUnitDto>();

            foreach (var child in children)
            {
                var childItem = GetNode(child, child.Id, node);
                node.Children.Add(childItem);
            }

            return node;
        }

        OrganizationalUnitDto IOrganizationalUnitConverter.Convert(OrganizationalUnitModel dto) => throw new NotImplementedException();
    }
}