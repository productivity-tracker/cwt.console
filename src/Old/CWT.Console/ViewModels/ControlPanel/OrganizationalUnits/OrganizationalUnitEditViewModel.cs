﻿using CWT.Viewer.Models.OrganizationUnits;
using CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces;
using DynamicData.Binding;

namespace CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits
{
    public class OrganizationalUnitEditViewModel : AbstractNotifyPropertyChanged, IOrganizationalUnitEditViewModel
    {
        public OrganizationalUnitModel Model { get; set; }

        public bool IsOpen { get; set; }

        public OrganizationalUnitEditViewModel(OrganizationalUnitModel model)
        {
            Model = model;
        }
    }
}