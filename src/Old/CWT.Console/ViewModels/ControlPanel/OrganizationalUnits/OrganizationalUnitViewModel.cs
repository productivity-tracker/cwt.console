﻿using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Threading.Tasks;
using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.OrganizationalUnits;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Models;
using CWT.Viewer.Models.OrganizationUnits;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits.Interfaces;
using DSFramework.Extensions;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;
using TranslateMe;

namespace CWT.Viewer.ViewModels.ControlPanel.OrganizationalUnits
{
    public class OrganizationalUnitViewModel :
        EditableListViewModel<OrganizationalUnitDto, EntityHolderOfOrganizationalUnitDto, WriteCommandOfOrganizationalUnitDto,
            OrganizationalUnitModel, IOrganizationalUnitConverter, IOrganizationalUnitsFacade>, IOrganizationalUnitViewModel
    {
        protected override string EditItemTitleTextId => "";
        protected override string NewItemTitleTextId => "";

        public OrganizationalUnitViewModel(ILogger logger,
                                           IOrganizationalUnitConverter converter,
                                           IOrganizationalUnitsFacade facade,
                                           IDialogService dialogService,
                                           INotificationMessageQueue notificationMessageQueue)
            : base(logger, converter, facade, dialogService, notificationMessageQueue)
        { }

        protected override IDisposable GetSubscription(SourceCache<EntityHolderOfOrganizationalUnitDto, string> source,
                                                       IOrganizationalUnitConverter converter,
                                                       out ReadOnlyObservableCollection<OrganizationalUnitModel> data)
            => source.Connect()
                     .Filter(filter: holder => holder?.Data != null)
                     .Transform(converter.Convert)
                     .Sort(SortExpressionComparer<OrganizationalUnitModel>.Ascending(expression: s => s.Name))
                     .ObserveOnDispatcher()
                     .Bind(out data)
                     .DisposeMany()
                     .Subscribe();

        protected override async Task<MessageBoxButtons> ShowEditDialog(string titleTextId, OrganizationalUnitModel model)
        {
            IOrganizationalUnitEditViewModel vm = new OrganizationalUnitEditViewModel(model);
            return await DialogService.ShowMessageBox(DialogIdentifier, titleTextId, vm, MessageBoxButtons.SaveCancel);
        }

        protected override async Task New()
        {
            var parentNode = SelectedItem;
            var newNode = new OrganizationalUnitModel();

            var result = await ShowEditDialog(NewItemTitleTextId, newNode);
            if (result == MessageBoxButtons.Cancel)
            {
                return;
            }

            if (parentNode == null)
            {
                await TryUpdate(newNode.Id, newNode.Name);
            }
            else
            {
                await TryUpdate(parentNode.Id, newNode.Name);
            }
        }

        protected override async Task Edit()
        {
            var clone = SelectedItem.SaveState();
            var result = await ShowEditDialog(EditItemTitleTextId, clone);

            if (result == MessageBoxButtons.Cancel)
            {
                return;
            }

            await TryUpdate(clone);
        }

        protected async Task TryUpdate(string parentId, string newName)
        {
            IsBusy = true;
            try
            {
                var result = await Facade.Add(parentId, newName);
                NotificationMessageQueue.Enqueue(!result.IsNullOrWhiteSpace()
                                                     ? TM.Tr(textId: "ChangesSavedSuccessfully")
                                                     : TM.Tr(textId: "ChangesNotSaved"));
            }
            catch (Exception e)
            {
                Logger.LogError(e, $"Error occurred while {nameof(TryUpdate)}. {e.Message}");
                NotificationMessageQueue.Enqueue(e.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}