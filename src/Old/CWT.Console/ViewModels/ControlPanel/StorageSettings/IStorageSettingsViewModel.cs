﻿using CWT.Viewer.Models.StorageSettings;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels.ControlPanel.StorageSettings
{
    public interface IStorageSettingsViewModel : IViewModel
    {
        ICommand LoadDataCommand { get; }

        ICommand EditCommand { get; }

        ICommand NewCommand { get; }

        ICommand DeleteCommand { get; }

        ReadOnlyObservableCollection<StorageSettingsModel> Items { get; }

        StorageSettingsModel SelectedStorageSettings { get; set; }
    }
}