﻿using CWT.Kernel.Contracts;
using CWT.Viewer.Models.StorageSettings;

namespace CWT.Viewer.ViewModels.ControlPanel.StorageSettings
{
    public static class StorageSettingsConverter
    {
        public static StorageSettingsModel Convert(EntityHolderOfStorageSettingsDto holder)
        {
            var entity = holder?.Data;
            if (entity == null)
            {
                return null;
            }

            var model = new StorageSettingsModel
            {
                Id = holder.Id,
                Name = entity.Name,
                Type = entity.Type,
                Endpoint = entity.Endpoint,
                Status = entity.Status,
                Usage = entity.Usage,
                Capacity = entity.Capacity
            };

            return model;
        }
    }
}