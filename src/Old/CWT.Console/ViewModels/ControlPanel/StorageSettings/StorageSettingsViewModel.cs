﻿using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Extensions;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models.StorageSettings;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.GuardToolkit;
using DynamicData;
using DynamicData.Binding;
using System;
using System.Collections.ObjectModel;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels.ControlPanel.StorageSettings
{
    public class StorageSettingsViewModel : AbstractPageViewModel, IStorageSettingsViewModel, IDisposable
    {
        private string _searchText;

        private readonly IDisposable _cleanUp;
        private readonly IDialogIdentifier _dialogIdentifier = new DialogIdentifier(GlobalConstants.MAIN_DIALOG_HOST_ID);

        private readonly IStorageSettingsFacade _storageSettingsFacade;
        private readonly SourceCache<EntityHolderOfStorageSettingsDto, string> _storageSettingsSource =
            new SourceCache<EntityHolderOfStorageSettingsDto, string>(s => s.Id);
        private readonly ReadOnlyObservableCollection<StorageSettingsModel> _storageSettings;

        public ReadOnlyObservableCollection<StorageSettingsModel> Items { get; }
        public StorageSettingsModel SelectedStorageSettings { get; set; }

        public string SearchText
        {
            get => _searchText;
            set => SetAndRaise(ref _searchText, value);
        }

        public ICommand LoadDataCommand { get; }
        public ICommand EditCommand { get; }
        public ICommand NewCommand { get; }
        public ICommand DeleteCommand { get; }

        public StorageSettingsViewModel(IStorageSettingsFacade storageSettingsFacade, IDialogService dialogService, INotificationMessageQueue notificationMessageQueue)
            : base(dialogService, notificationMessageQueue)
        {
            _storageSettingsFacade = Check.NotNull(storageSettingsFacade, nameof(storageSettingsFacade));

            LoadDataCommand = new RelayCommand(LoadData);
            NewCommand = new RelayCommandAsync(Add);
            EditCommand = new RelayCommandAsync(Edit, o => SelectedStorageSettings != null);
            DeleteCommand = new RelayCommandAsync(Delete, o => SelectedStorageSettings != null);

            var textFilter = this.WhenValueChanged(vm => vm.SearchText).Throttle(TimeSpan.FromMilliseconds(250)).Select(BuildFilter);
            var storageSettingsLoafer = _storageSettingsSource.Connect()
                                                              .Transform(StorageSettingsConverter.Convert)
                                                              .Filter(textFilter)
                                                              .Sort(SortExpressionComparer<StorageSettingsModel>.Ascending(s => s.Name))
                                                              .ObserveOnDispatcher()
                                                              .Bind(out _storageSettings)
                                                              .DisposeMany()
                                                              .Subscribe();

            _cleanUp = new CompositeDisposable(storageSettingsLoafer);
        }

        public void Dispose()
        {
            _cleanUp?.Dispose();
            _storageSettingsSource?.Dispose();
        }

        private void LoadData()
        {

        }

        private Task Add()
        {
            return Task.CompletedTask;
        }

        private Task Edit()
        {
            return Task.CompletedTask;
        }

        private Task Delete()
        {
            return Task.CompletedTask;
        }

        private Func<StorageSettingsModel, bool> BuildFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return storage => true;
            }

            return s => s.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }
    }
}