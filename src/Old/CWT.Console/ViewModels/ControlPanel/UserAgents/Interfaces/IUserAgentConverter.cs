﻿using CWT.Kernel.Contracts;
using CWT.Viewer.Models.UserAgents;
using DSFramework.Domain.Abstractions.Converters;

namespace CWT.Viewer.ViewModels.ControlPanel.UserAgents.Interfaces
{
    public interface IUserAgentConverter : IConverter<EntityHolderOfUserAgentDto, UserAgentModel>
    {
        
    }
}