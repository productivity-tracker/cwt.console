﻿using CWT.Viewer.Models.UserAgents;
using CWT.Viewer.ViewModels.Abstractions;

namespace CWT.Viewer.ViewModels.ControlPanel.UserAgents.Interfaces
{
    public interface IUserAgentsViewModel : IDynamicListViewModel<UserAgentModel>, IViewModel
    {
        
    }
}