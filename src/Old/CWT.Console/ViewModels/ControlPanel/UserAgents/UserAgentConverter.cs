﻿using CWT.Kernel.Contracts;
using CWT.Kernel.Contracts.UserAgents;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Models.UserAgents;
using CWT.Viewer.ViewModels.ControlPanel.UserAgents.Interfaces;
using DSFramework.GuardToolkit;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Threading.Tasks;
using CWT.Viewer.Infrastructure;

namespace CWT.Viewer.ViewModels.ControlPanel.UserAgents
{
    public class UserAgentConverter : IUserAgentConverter
    {
        private const string UNKNOWN = "Unknown";

        private readonly IEmployeesFacade _employeesFacade;
        private readonly ILogger _logger;

        public UserAgentConverter(ILogger logger, IEmployeesFacade employeesFacade)
        {
            _employeesFacade = Check.NotNull(employeesFacade, nameof(employeesFacade));
            _logger = logger ?? NullLogger.Instance;
        }

        public UserAgentModel Convert(EntityHolderOfUserAgentDto holderOfUserAgentDto)
        {
            var userAgentDto = holderOfUserAgentDto?.Data;
            if (userAgentDto == null)
            {
                return null;
            }

            var employeeId = userAgentDto.EmployeeId;
            var linkedEmployeeName = string.IsNullOrWhiteSpace(employeeId) ? string.Empty : GetEmployeeName(employeeId) ?? UNKNOWN;
            var isNew = userAgentDto.Status == UserAgentStatusDto.Discovered;
            var model = new UserAgentModel(holderOfUserAgentDto.Id,
                                           userAgentDto.DisplayName,
                                           userAgentDto.Key?.UserName,
                                           userAgentDto.Key?.Domain,
                                           userAgentDto.Key?.UniqueName,
                                           userAgentDto.IsOnline,
                                           userAgentDto.LastLoggedOnDate,
                                           linkedEmployeeName,
                                           isNew);
            return model;
        }

        public EntityHolderOfUserAgentDto Convert(UserAgentModel model)
        {
            throw new NotImplementedException();
        }

        private string GetEmployeeName(string employeeId)
        {
            try
            {
                var userAgentHolder = AsyncHelper.RunSync(() => _employeesFacade.Get(employeeId));
                var userAgentName = userAgentHolder?.Data?.DisplayName;
                return userAgentName;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null;
            }
        }
    }
}