﻿using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Models.UserAgents;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using CWT.Viewer.ViewModels.ControlPanel.UserAgents.Interfaces;
using DSFramework.GuardToolkit;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;

namespace CWT.Viewer.ViewModels.ControlPanel.UserAgents
{
    public class UserAgentsViewModel
        : DynamicListViewModel<EntityHolderOfUserAgentDto, UserAgentModel, IUserAgentConverter, IUserAgentsFacade>, IUserAgentsViewModel
    {
        private readonly IUserAgentsFacade _userAgentsFacade;

        public UserAgentsViewModel(
            ILogger logger,
            IUserAgentConverter converter,
            IUserAgentsFacade facade,
            IDialogService dialogService,
            INotificationMessageQueue notificationMessageQueue)
            : base(logger, converter, facade, dialogService, notificationMessageQueue)
        {
            _userAgentsFacade = Check.NotNull(facade, nameof(facade));
        }

        protected override IDisposable GetSubscription(
            SourceCache<EntityHolderOfUserAgentDto, string> source,
            IUserAgentConverter converter,
            out ReadOnlyObservableCollection<UserAgentModel> data)
        {
            return source.Connect()
                         .Filter(holder => holder?.Data != null)
                         .Transform(converter.Convert)
                         .Sort(SortExpressionComparer<UserAgentModel>.Ascending(u => u.IsNew).ThenByAscending(u => u.UniqueName))
                         .ObserveOnDispatcher()
                         .Bind(out data)
                         .DisposeMany()
                         .Subscribe();
        }
    }
}