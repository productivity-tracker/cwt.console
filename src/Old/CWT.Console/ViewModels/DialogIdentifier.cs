﻿namespace CWT.Viewer.ViewModels
{
    public class DialogIdentifier : IDialogIdentifier
    {
        public DialogIdentifier(string identifier)
        {
            Identifier = identifier;
        }

        public string Identifier { get; }
    }
}