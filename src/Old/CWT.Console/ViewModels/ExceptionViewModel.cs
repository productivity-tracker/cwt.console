﻿using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Services;
using System;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels
{
    public class ExceptionViewModel : CloseableViewModel, IExceptionViewModel
    {
        private readonly Exception _exception;
        private readonly IApplicationService _applicationService;

        public string Message => _exception?.Message;

        public ICommand CopyCommand => new RelayCommand(Copy);
        public ICommand OpenLogFolderCommand => new RelayCommand(OpenLogFolder);
        public ICommand ContinueCommand => new RelayCommand(Continue);
        public ICommand RestartCommand => new RelayCommand(Restart);
        public ICommand ExitCommand => new RelayCommand(Exit);

        public ExceptionViewModel(Exception exception, IApplicationService applicationService)
        {
            _exception = exception;
            _applicationService = applicationService;
        }

        private void Copy()
        {
            _applicationService.CopyToClipboard(_exception.ToString());
        }

        private void OpenLogFolder()
        {
            _applicationService.OpenFolder(_applicationService.LogFolder);
        }

        private void Exit()
        {
            _applicationService.Exit();
        }

        private void Restart()
        {
            _applicationService.Restart();
        }

        private void Continue()
        {
            IsOpen = false;
        }
    }
}