﻿namespace CWT.Viewer.ViewModels
{
    /// <summary>
    /// DialogHost identifier.
    /// </summary>
    public interface IDialogIdentifier
    {
        /// <summary>
        /// DialogHost identifier.
        /// </summary>
        string Identifier { get; }
    }
}