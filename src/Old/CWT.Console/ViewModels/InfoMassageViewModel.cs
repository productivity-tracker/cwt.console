﻿namespace CWT.Viewer.ViewModels
{
    public class InfoMassageViewModel : CloseableViewModel, IInfoMassageViewModel
    {
        private string _message;
        public string Message
        {
            get => _message;
            set => SetAndRaise(ref _message, value);
        }
    }
}