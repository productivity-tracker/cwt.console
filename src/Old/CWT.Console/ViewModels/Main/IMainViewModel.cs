﻿using System.Collections.Generic;
using System.Windows.Input;

// ReSharper disable once CheckNamespace
namespace CWT.Viewer.ViewModels
{
    public interface IMainViewModel : IViewModel
    {
        string Title { get; }
        List<MenuItemViewModel> MenuItems { get; }
        
        ICommand OpenSettingsCommand { get; }
    }
}