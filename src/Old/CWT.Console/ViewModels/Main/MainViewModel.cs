﻿using CWT.Viewer.Helpers;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.GuardToolkit;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using TranslateMe;

namespace CWT.Viewer.ViewModels
{
    public class MainViewModel : AbstractViewModel, IMainViewModel, IDialogIdentifier
    {
        private readonly IDialogService _dialogService;
        private readonly ISettingsViewModel _settingsViewModel;

        private readonly ILogger _logger;

        public string Title => $"{TM.Tr("MainTitle")}";
        public string Identifier => GlobalConstants.MAIN_DIALOG_HOST_ID;
        public List<MenuItemViewModel> MenuItems { get; } = new List<MenuItemViewModel>();

        public ICommand OpenSettingsCommand => new RelayCommand(
            () => _dialogService.Show(this, _settingsViewModel));

        public MainViewModel(ILogger logger, ISettingsViewModel settingsViewModel, IDialogService dialogService)
        {
            _dialogService = Check.NotNull(dialogService, nameof(dialogService));
            _settingsViewModel = Check.NotNull(settingsViewModel, nameof(settingsViewModel));
            _logger = Check.NotNull(logger, nameof(logger));
        }
    }
}