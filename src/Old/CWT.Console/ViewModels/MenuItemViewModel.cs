﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CWT.Viewer.ViewModels.Abstractions;
using CWT.Viewer.Infrastructure.Commands;

namespace CWT.Viewer.ViewModels
{
    public class MenuItemViewModel : AbstractViewModel
    {
        private string _caption;
        private object _content;
        private string _description;
        private string _icon;
        private bool _isChecked;

        private List<MenuItemViewModel> _menuItems = new List<MenuItemViewModel>();

        public string Icon
        {
            get => _icon;
            set => SetAndRaise(ref _icon, value);
        }

        public bool IsChecked
        {
            get => _isChecked;
            set => SetAndRaise(ref _isChecked, value);
        }

        public string Caption
        {
            get => _caption;
            set => SetAndRaise(ref _caption, value);
        }

        public string Description
        {
            get => _description;
            set => SetAndRaise(ref _description, value);
        }

        public object Content
        {
            get => _content;
            set => SetAndRaise(ref _content, value);
        }

        public List<MenuItemViewModel> MenuItems
        {
            get => _menuItems;
            set => SetAndRaise(ref _menuItems, value);
        }

        public ICommand Command { get; set; }

        public MenuItemViewModel()
        {
        }

        public MenuItemViewModel(string caption, Action action, object content = null)
        {
            _caption = caption;
            _content = content;
            Command = new RelayCommand(action);
        }
    }
}