﻿namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public class ContextInitResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}