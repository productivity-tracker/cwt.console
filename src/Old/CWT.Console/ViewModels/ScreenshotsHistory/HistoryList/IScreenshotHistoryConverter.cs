﻿using System.Collections.Generic;
using CWT.Kernel.Contracts;
using CWT.Viewer.Models;
using CWT.Viewer.Models.ScreenshotsHistory;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public interface IScreenshotHistoryConverter
    {
        EmployeeModel Convert(EntityHolderOfEmployeeDto holderDto);
        DictionaryWithReturnValue<string, ScreenshotsDetailsModel> Convert(IEnumerable<EntityHolderOfScreenshotsHistoryDto> holders);

        string GetOrgUnit(string employeeId);
    }
}