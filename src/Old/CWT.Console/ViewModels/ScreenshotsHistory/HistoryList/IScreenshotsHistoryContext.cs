﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CWT.Viewer.Models.ScreenshotsHistory;
using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.Timing;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public interface IScreenshotsHistoryContext : IViewModelContext<ScreenshotsHistoryModel>, IDisposable
    {
        ReadOnlyObservableCollection<string> OrganizationalUnits { get; }
        string SelectedOrganizationalUnit { get; set; }

        Task<ContextInitResult> Load(DateTimeRange timeRange);
    }
}