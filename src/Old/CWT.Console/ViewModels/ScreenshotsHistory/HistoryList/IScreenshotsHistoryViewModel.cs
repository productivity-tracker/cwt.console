﻿using System.Windows.Input;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public interface IScreenshotsHistoryViewModel : IViewModel
    {
        string SpecifiedRange { get; }
        IScreenshotsHistoryContext Context { get; }
        ICommand LoadDataCommand { get; }
        ICommand ClearFiltersCommand { get; }
        ICommand OpenHistoryCommand { get; }
        ICommand ApplyQuickTimeRangeCommand { get; }
        ICommand ChoiceCustomTimeRangeCommand { get; }
        ICommand ApplyCustomTimeRangeCommand { get; }
    }
}