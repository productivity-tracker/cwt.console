﻿using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Extensions;
using CWT.Viewer.Models.ScreenshotsHistory;
using DSFramework.GuardToolkit;
using DSFramework.Threading;
using DSFramework.Timing;
using DynamicData;
using DynamicData.Binding;
using DynamicData.Kernel;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using CWT.Viewer.ViewModels.Abstractions;
using TranslateMe;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public class ScreenshotsHistoryContext : ViewModelContext<ScreenshotsHistoryModel>, IScreenshotsHistoryContext
    {
        private readonly IEmployeesFacade _employeesFacade;
        private readonly IScreenshotsHistoriesFacade _screenshotsHistoriesFacade;
        private readonly IScreenshotHistoryConverter _converter;

        private readonly ILogger _logger;
        private readonly IDisposable _cleanUp;

        private readonly ReadOnlyObservableCollection<ScreenshotsHistoryModel> _items;
        private readonly ReadOnlyObservableCollection<string> _organizationalUnits;
        private readonly ISubject<EntityHolderOfScreenshotsHistoryDto> _historySubject;
        private readonly SourceCache<EntityHolderOfScreenshotsHistoryDto, string> _sourceScreenshotsHistories;
        private readonly SourceCache<EntityHolderOfEmployeeDto, string> _sourceEmployees;
        private readonly GenericLock<string> _genericLock = new GenericLock<string>();

        private Guid _employeesChangeHandlerGuid;
        private Guid _historyChangeHandlerGuid;

        private string _selectedOrganizationalUnit;

        public override ReadOnlyObservableCollection<ScreenshotsHistoryModel> Items => _items;
        public ReadOnlyObservableCollection<string> OrganizationalUnits => _organizationalUnits;

        public IObservable<EntityHolderOfScreenshotsHistoryDto> HistoryObservable => _historySubject.AsObservable();

        public string SelectedOrganizationalUnit
        {
            get => _selectedOrganizationalUnit;
            set => SetAndRaise(ref _selectedOrganizationalUnit, value);
        }

        public ScreenshotsHistoryContext(ILogger logger,
                                         IEmployeesFacade employeesFacade,
                                         IScreenshotsHistoriesFacade screenshotsHistoriesFacade,
                                         IScreenshotHistoryConverter screenshotHistoryConverter)
        {
            _logger = logger;
            _employeesFacade = Check.NotNull(employeesFacade, nameof(employeesFacade));
            _screenshotsHistoriesFacade = Check.NotNull(screenshotsHistoriesFacade, nameof(screenshotsHistoriesFacade));
            _converter = Check.NotNull(screenshotHistoryConverter, nameof(screenshotHistoryConverter));

            _historySubject = new Subject<EntityHolderOfScreenshotsHistoryDto>();
            _sourceScreenshotsHistories = new SourceCache<EntityHolderOfScreenshotsHistoryDto, string>(s => s.Id);
            _sourceEmployees = new SourceCache<EntityHolderOfEmployeeDto, string>(e => e.Id);

            var historyLoader = HistoryObservable.Buffer(TimeSpan.FromSeconds(3)).Subscribe(_sourceScreenshotsHistories.AddOrUpdate);
            var textFilter = this.WhenValueChanged(ctx => ctx.SearchText).Throttle(TimeSpan.FromMilliseconds(250)).Select(BuildEmployeeNameFilter);
            var orgUnitsFilter = this.WhenValueChanged(vm => vm.SelectedOrganizationalUnit)
                                     .Throttle(TimeSpan.FromMilliseconds(250))
                                     .Select(BuildOrgUnitFilter);

            var employeesWithScreenshotHistories = _sourceEmployees
                                                   .Connect()
                                                   .FullJoinMany(_sourceScreenshotsHistories.Connect(),
                                                                 s => s.Data.Key.EmployeeId,
                                                                 BuildModel)
                                                   .Filter(textFilter)
                                                   .Filter(orgUnitsFilter)
                                                   .Sort(SortExpressionComparer<ScreenshotsHistoryModel>.Ascending(s => s.Employee.DisplayName))
                                                   .ObserveOnDispatcher()
                                                   .Bind(out _items)
                                                   .DisposeMany()
                                                   .Subscribe();

            var orgUnits = _sourceEmployees.Connect()
                                           .DistinctValues(x => _converter.GetOrgUnit(x.Id))
                                           .ObserveOnDispatcher()
                                           .Bind(out _organizationalUnits)
                                           .DisposeMany()
                                           .Subscribe();

            _cleanUp = new CompositeDisposable(employeesWithScreenshotHistories, historyLoader, orgUnits);

            RegisterChangeHandlers();
        }

        public void Dispose()
        {
            _employeesFacade.RemoveChangeHandler(_employeesChangeHandlerGuid);
            _screenshotsHistoriesFacade.RemoveChangeHandler(_historyChangeHandlerGuid);
            _cleanUp?.Dispose();
        }

        public async Task<ContextInitResult> Load(DateTimeRange timeRange)
        {
            var result = new ContextInitResult();

            try
            {
                var historiesDto = await _screenshotsHistoriesFacade.SearchByDateTimeRangeAsync(timeRange);
                if (historiesDto == null || !historiesDto.Any())
                {
                    result.IsSuccess = false;
                    result.Message = TM.Tr("HistoryIsEmpty");
                    return result;
                }

                var employeesDto = await _employeesFacade.GetMany(historiesDto.Select(h => h.Data?.Key?.EmployeeId));

                _sourceEmployees.AddOrUpdate(employeesDto);
                _sourceScreenshotsHistories.AddOrUpdate(historiesDto);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while {nameof(Load)}.");
                result.IsSuccess = false;
                result.Message = ex.Message;
            }

            return result;
        }

        private void RegisterChangeHandlers()
        {
            _employeesChangeHandlerGuid = _employeesFacade.RegisterChangeHandler(async holder => await HandleUpdate(holder));
            _historyChangeHandlerGuid = _screenshotsHistoriesFacade.RegisterChangeHandler(async holder => await HandleUpdate(holder));
        }

        private Task HandleUpdate(EntityHolderOfScreenshotsHistoryDto screenshotsHistoryDto)
        {
            return Task.Run(() =>
            {
                using (_genericLock.Lock(screenshotsHistoryDto.Id))
                {
                    _historySubject.OnNext(screenshotsHistoryDto);
                }
            });
        }

        private Task HandleUpdate(EntityHolderOfEmployeeDto employeeDto)
        {
            return Task.Run(() =>
            {
                using (_genericLock.Lock(employeeDto.Id))
                {
                    _sourceEmployees.AddOrUpdate(employeeDto);
                }
            });
        }

        private ScreenshotsHistoryModel BuildModel(Optional<EntityHolderOfEmployeeDto> employeeHolderDto,
                                                   IGrouping<EntityHolderOfScreenshotsHistoryDto, string, string> histories)
        {
            var employee = _converter.Convert(employeeHolderDto.Value);
            var screenshotDetails = _converter.Convert(histories.Items);
            return new ScreenshotsHistoryModel(employee, screenshotDetails);
        }

        private Func<ScreenshotsHistoryModel, bool> BuildEmployeeNameFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return employee => true;
            }

            return e => e.Employee.DisplayName.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }

        private Func<ScreenshotsHistoryModel, bool> BuildOrgUnitFilter(string orgUnit)
        {
            if (string.IsNullOrEmpty(orgUnit))
            {
                return employee => true;
            }

            return e => e.Employee.OrganizationalUnit.Contains(orgUnit, StringComparison.OrdinalIgnoreCase);
        }
    }
}