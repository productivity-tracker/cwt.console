﻿using System;
using System.Collections.Generic;
using System.Linq;
using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Infrastructure;
using CWT.Viewer.Models;
using CWT.Viewer.Models.ScreenshotsHistory;
using DSFramework.GuardToolkit;
using DSFramework.Timing;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public class ScreenshotsHistoryConverter : IScreenshotHistoryConverter
    {
        public const string UNKNOWN_ORG_UNIT = "Unknown";
        private readonly IOrganizationalUnitsFacade _organizationalUnitFacade;
        private readonly IUserAgentsFacade _userAgentsFacade;
        private readonly IEmployeesFacade _employeesFacade;

        public ScreenshotsHistoryConverter(IOrganizationalUnitsFacade organizationalUnitCachedFacade,
                                          IUserAgentsFacade userAgentsFacade,
                                          IEmployeesFacade employeesFacade)
        {
            _organizationalUnitFacade = Check.NotNull(organizationalUnitCachedFacade, nameof(organizationalUnitCachedFacade));
            _userAgentsFacade = Check.NotNull(userAgentsFacade, nameof(userAgentsFacade));
            _employeesFacade = Check.NotNull(employeesFacade, nameof(employeesFacade));
        }

        public EmployeeModel Convert(EntityHolderOfEmployeeDto holderDto)
        {
            var employee = holderDto?.Data;
            if (employee == null)
            {
                return null;
            }

            var department = GetOrgUnit(holderDto.Id);
            var userAgentHolder = AsyncHelper.RunSync(() => _userAgentsFacade.GetByEmployee(holderDto.Id));
            return userAgentHolder == null ? null : new EmployeeModel(employee.LastLoggedOnDate, holderDto.Id, userAgentHolder.Id, employee.DisplayName, department);
        }

        public DictionaryWithReturnValue<string, ScreenshotsDetailsModel> Convert(IEnumerable<EntityHolderOfScreenshotsHistoryDto> holders)
        {
            var result = new DictionaryWithReturnValue<string, ScreenshotsDetailsModel>();
            foreach (var holder in holders)
            {
                var entity = holder?.Data;
                var first = entity?.DateTimeRanges?.FirstOrDefault()?.StartTime;
                var last = entity?.DateTimeRanges?.LastOrDefault()?.EndTime;

                if (first == null || last == null)
                {
                    continue;
                }

                var sumTime = entity.DateTimeRanges.Sum(range => range.TimeSpan.TotalSeconds);
                var dateKey = entity.Key.Date;

                var model = new ScreenshotsDetailsModel(first.Value.TimeOfDay,
                                                        last.Value.TimeOfDay,
                                                        TimeSpan.FromSeconds(sumTime),
                                                        holder.Id,
                                                        new Date(entity.Key.Date));
                result[dateKey] = model;
            }

            return result;
        }

        public string GetOrgUnit(string employeeId)
        {
            return UNKNOWN_ORG_UNIT;
        }
    }
}