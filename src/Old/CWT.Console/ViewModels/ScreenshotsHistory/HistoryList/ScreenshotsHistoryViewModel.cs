﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.Services.ScreenshotsHistories;
using CWT.Viewer.ViewModels.Abstractions;
using CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications;
using CWT.Viewer.ViewModels.TimeRange;
using DSFramework.Extensions;
using DSFramework.GuardToolkit;
using DSFramework.Timing;
using Microsoft.Extensions.Logging;
using TranslateMe;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList
{
    public class ScreenshotsHistoryViewModel : AbstractPageViewModel, IScreenshotsHistoryViewModel, IDisposable
    {
        private const int DEFAULT_TIME_RANGE = 14;
        private const int MAX_TIME_RANGE_IN_DAYS = 90;

        private readonly Dictionary<int, DateTimeRange> _quickRanges = new Dictionary<int, DateTimeRange>
        {
            { 7, DateTimeRange.Last7Days },
            { 14, new DateTimeRange(DateTime.Now.AddDays(value: -14.0), DateTime.Now) },
            { 30, DateTimeRange.Last30Days }
        };

        private readonly ILogger _logger;
        private readonly IDialogIdentifier _dialogIdentifier = new DialogIdentifier(GlobalConstants.MAIN_DIALOG_HOST_ID);
        private int _currentRangeInDays = 14;

        private bool _isInit;
        private string _specifiedRange;

        public string SpecifiedRange
        {
            get => _specifiedRange;
            private set => SetAndRaise(ref _specifiedRange, value);
        }

        public IScreenshotsHistoryContext Context { get; }

        public ICommand LoadDataCommand
            => new RelayCommand(execute: () =>
                                {
                                    SpecifiedRange = string.Format(TM.Tr(textId: "LastNDays"), _currentRangeInDays);
                                    LoadData(TimeRange);
                                },
                                canExecute: () => !_isInit);

        public ICommand ClearFiltersCommand => new RelayCommand(ClearFilters);
        public ICommand OpenHistoryCommand => new RelayCommand<OpenHistoryCommandArgs>(OpenHistory);
        public ICommand ApplyQuickTimeRangeCommand => new RelayCommand<int>(ApplyQuickTimeRange);
        public ICommand ChoiceCustomTimeRangeCommand => new RelayCommandAsync(ChoiceCustomTimeRange);
        public ICommand ApplyCustomTimeRangeCommand => new RelayCommand(execute: () => { });

        public DateTimeRange TimeRange
            => _quickRanges.TryGetValue(_currentRangeInDays, out var timeRange) ? timeRange : _quickRanges.GetOrDefault(DEFAULT_TIME_RANGE);

        public ScreenshotsHistoryViewModel(ILogger logger,
                                           IScreenshotsHistoryContext context,
                                           INotificationMessageQueue notificationMessageQueue,
                                           IDialogService dialogService)
            : base(dialogService, notificationMessageQueue)
        {
            _logger = logger;
            Context = Check.NotNull(context, nameof(context));
        }

        public void Dispose() => Context?.Dispose();

        private void AddDateColumn(Date date)
        {
            var header = date.ToString();
            var addDateColumnNotification = new AddDateColumnNotification
            {
                Header = header,
                Binding = $"Items[{header}]"
            };
            ScreenshotsHistoryColumnsService.Instance.AddDateColumn.Raise(addDateColumnNotification);
        }

        private void AddEmployeeColumn(string header, string binding)
        {
            var addEmployeeColumnNotification = new AddEmployeeColumnNotification
            {
                Header = header,
                Binding = binding
            };
            ScreenshotsHistoryColumnsService.Instance.AddEmployeeColumn.Raise(addEmployeeColumnNotification);
        }

        private void ApplyCustomTimeRange(DateTimeRange timeRange)
        {
            if (timeRange.TimeSpan.TotalDays > MAX_TIME_RANGE_IN_DAYS)
            {
                NotificationMessageQueue.Enqueue(string.Format(TM.Tr(textId: "MaxTimeRangeWarning"), MAX_TIME_RANGE_IN_DAYS));
            }
        }

        private void ApplyQuickTimeRange(int numberOfLastDays)
        {
            if (!_quickRanges.TryGetValue(numberOfLastDays, out var timeRange))
            {
                timeRange = _quickRanges.GetOrDefault(DEFAULT_TIME_RANGE);
            }

            _currentRangeInDays = numberOfLastDays;
            SpecifiedRange = string.Format(TM.Tr(textId: "LastNDays"), numberOfLastDays);

            LoadData(timeRange, isForce: true);
        }

        private async Task ChoiceCustomTimeRange()
        {
            var timeRange = new DateTimeRange(TimeRange);
            var result = await ShowChoiceCustomTimeRangeDialog(title: "ChoiceDateRange", timeRange);

            if (result == MessageBoxButtons.Apply)
            {
                SpecifiedRange =
                    $"{TM.Tr(textId: "DateTimeFrom")} {timeRange.StartTime:MMMM d, yyyy} {TM.Tr(textId: "DateTimeTo").ToLower()} {timeRange.EndTime:MMMM d, yyyy}";
                LoadData(timeRange, isForce: true);
            }
        }

        private void ClearFilters()
        {
            Context.SearchText = null;
            Context.SelectedOrganizationalUnit = null;
        }

        private void GenerateDateColumns(DateTimeRange timeRange)
        {
            foreach (var date in timeRange.GetDates())
            {
                AddDateColumn(date);
            }
        }

        private void InitializeEmployeeColumn()
        {
            var header = TM.Tr(textId: "Employees").ToUpper();
            AddEmployeeColumn(header, binding: "Employee.DisplayName");
        }

        private async void LoadData(DateTimeRange timeRange, bool isForce = false)
        {
            if (_isInit && !isForce)
            {
                return;
            }

            try
            {
                IsBusy = true;
                if (_isInit)
                {
                    ScreenshotsHistoryColumnsService.Instance.ClearAllColumns.Raise(new ClearAllColumnsNotification());
                }

                var result = await Context.Load(timeRange);
                if (!result.IsSuccess)
                {
                    NotificationMessageQueue.Enqueue(result.Message);
                    return;
                }

                InitializeEmployeeColumn();
                GenerateDateColumns(timeRange);
                _isInit = true;
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void OpenHistory(OpenHistoryCommandArgs args)
        {
            var notification = new OpenScreenshotsHistoryNotification
            {
                Header = $"{args.EmployeeName} [{args.Date}]",
                ScreenshotHistoryId = args.ScreenshotsHistoryId,
                EmployeeId = args.EmployeeId,
                Date = args.Date
            };
            ScreenshotsHistoryTabsService.Instance.OpenScreenshotsHistory.Raise(notification);
        }

        private async Task<MessageBoxButtons> ShowChoiceCustomTimeRangeDialog(string title, DateTimeRange timeRange)
        {
            ICustomTimeRangeViewModel vm = new CustomTimeRangeViewModel(timeRange);
            return await DialogService.ShowMessageBox(_dialogIdentifier, title, vm, MessageBoxButtons.ApplyCancel);
        }
    }
}