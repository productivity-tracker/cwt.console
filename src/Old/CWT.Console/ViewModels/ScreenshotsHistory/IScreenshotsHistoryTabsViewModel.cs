﻿using System.Collections.ObjectModel;
using CWT.Console.Infrastructure;
using CWT.Viewer.Infrastructure;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory
{
    public interface IScreenshotsHistoryTabsViewModel : IViewModel
    {
        ViewContainer SelectedItem { get; set; }
        ObservableCollection<ViewContainer> Items { get; }
    }
}