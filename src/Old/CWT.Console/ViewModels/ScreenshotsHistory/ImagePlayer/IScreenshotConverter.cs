﻿using CWT.StorageAdapter.Contracts;
using CWT.Viewer.Models.ScreenshotsHistory;
using DSFramework.Domain.Abstractions.Converters;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer
{
    public interface IScreenshotConverter : IConverter<ScreenshotInfo, ImageModel>
    {
        
    }
}