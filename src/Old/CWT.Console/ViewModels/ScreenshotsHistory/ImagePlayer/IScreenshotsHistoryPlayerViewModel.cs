﻿using System.Collections.Generic;
using CWT.Viewer.Models.ScreenshotsHistory;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Threading;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer
{
    public interface IScreenshotsHistoryPlayerViewModel : IViewModel
    {
        bool AllowAction { get; }
        int SelectedImageIndex { get; set; }
        string SelectedTime { get; }
        ImageModel SelectedImage { get; set; }
        ImageModel LastImage { get; }
        ReadOnlyObservableCollection<ImageModel> ImageListCollection { get; }
        Dictionary<string, double> SlideShowSpeedCoefficients { get; }
        bool IsClockTicking { get; set; }
        DispatcherTimer Clock { get; }

        ICommand LoadDataCommand { get; }
        ICommand SelectPreviousImageCommand { get; }
        ICommand SelectNextImageCommand { get; }
        ICommand DummyCommand { get; }
        ICommand SkipBackwardCommand { get; }
        ICommand SkipForwardCommand { get; }
    }
}