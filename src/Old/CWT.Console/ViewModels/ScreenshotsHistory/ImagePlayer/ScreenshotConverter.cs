﻿using CWT.StorageAdapter.Contracts;
using CWT.Viewer.Models.ScreenshotsHistory;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer
{
    public class ScreenshotConverter : IScreenshotConverter
    {
        public ImageModel Convert(ScreenshotInfo source)
        {
            return new ImageModel
            {
                EmployeeId = source.EmployeeId,
                Timestamp = source.CreatedAtUtc.ToLocalTime(),
                ImageUrl = source.Uri
            };
        }

        public ScreenshotInfo Convert(ImageModel source)
        {
            throw new System.NotImplementedException();
        }
    }
}