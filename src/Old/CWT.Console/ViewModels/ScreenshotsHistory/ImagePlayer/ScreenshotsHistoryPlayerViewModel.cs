﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using CWT.StorageAdapter.Contracts;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models.ScreenshotsHistory;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.Timing;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer
{
    public class ScreenshotsHistoryPlayerViewModel : AbstractPageViewModel, IScreenshotsHistoryPlayerViewModel, IDisposable
    {
        private const int MAX_SLIDE_SHOW_SPEED = 2200;
        private const double SLIDE_SHOW_SPEED_DEF_VALUE = 1000;
        private const int ONE_STEP = 1;
        private const int SKIP_STEP = 15;

        private readonly string _employeeId;
        private readonly Date _dateFrom;
        private readonly ScreenshotsProvider _screenshotsProvider = new ScreenshotsProvider();
        private readonly Dispatcher _currentDispatcher;
        private readonly IDisposable _cleanUp;
        private readonly ILogger _logger;

        private readonly SourceCache<ScreenshotInfo, string> _source = new SourceCache<ScreenshotInfo, string>(x => x.Id);
        private readonly ReadOnlyObservableCollection<ImageModel> _data;
        private bool _allowAction;

        private DispatcherTimer _clock;
        private bool _isClockTicking;
        private ImageModel _lastImage;
        private ImageModel _selectedImage;
        private int _selectedImageIndex;
        private string _selectedSlideShowSpeedCoefficient;
        private string _selectedTime = "0";

        private ICommand _dummyCommand;
        private ICommand _loadDataCommand;
        private ICommand _selectNextImageCommand;
        private ICommand _selectPreviousImageCommand;
        private ICommand _skipBackwardCommand;
        private ICommand _skipForwardCommand;

        public ReadOnlyObservableCollection<ImageModel> ImageListCollection => _data;
        public DispatcherTimer Clock => _clock ??= new DispatcherTimer(DispatcherPriority.Normal, _currentDispatcher);
        public Dictionary<string, double> SlideShowSpeedCoefficients { get; } = new Dictionary<string, double>();

        public string SelectedTime
        {
            get => _selectedTime;
            private set => SetAndRaise(ref _selectedTime, value);
        }

        public ImageModel SelectedImage
        {
            get => _selectedImage;
            set
            {
                SetAndRaise(ref _selectedImage, value);
                SelectedImageIndex = ImageListCollection.IndexOf(SelectedImage) + 1;
            }
        }

        public int SelectedImageIndex
        {
            get => _selectedImageIndex;
            set
            {
                var newValue = value > 0 ? value - 1 : value;
                SetAndRaise(ref _selectedImageIndex, newValue);
                SelectedTime = ImageListCollection != null ? ImageListCollection[_selectedImageIndex].Time : "0";
            }
        }

        public bool AllowAction
        {
            get => _allowAction;
            private set => SetAndRaise(ref _allowAction, value);
        }

        public ImageModel LastImage
        {
            get => _lastImage;
            private set => SetAndRaise(ref _lastImage, value);
        }

        public bool IsClockTicking
        {
            get => _isClockTicking;
            set
            {
                SetAndRaise(ref _isClockTicking, value);
                Clock.IsEnabled = _isClockTicking;
            }
        }

        public string SelectedSlideShowSpeedCoefficient
        {
            get => _selectedSlideShowSpeedCoefficient;
            set
            {
                Clock.Interval = TimeSpan.FromMilliseconds(MAX_SLIDE_SHOW_SPEED - SLIDE_SHOW_SPEED_DEF_VALUE * SlideShowSpeedCoefficients[value]);
                SetAndRaise(ref _selectedSlideShowSpeedCoefficient, value);
            }
        }

        public ICommand SelectPreviousImageCommand => _selectPreviousImageCommand ??= new RelayCommand(() => SelectPreviousImage(ONE_STEP),
                                                                                                       () => CanSelectPreviousImage(ONE_STEP));
        public ICommand SelectNextImageCommand => _selectNextImageCommand ??= new RelayCommand(() => SelectNextImage(ONE_STEP), 
                                                                                               () => CanSelectNextImage(ONE_STEP));
        public ICommand SkipBackwardCommand => _skipBackwardCommand ??= new RelayCommand(() => SelectPreviousImage(SKIP_STEP),
                                                                                         () => CanSelectPreviousImage(SKIP_STEP));
        public ICommand SkipForwardCommand => _skipForwardCommand ??= new RelayCommand(() => SelectNextImage(SKIP_STEP),
                                                                                       () => CanSelectNextImage(SKIP_STEP));
        public ICommand DummyCommand => _dummyCommand ??= new RelayCommand(() => { }, () => ImageListCollection?.Count > 0);
        public ICommand LoadDataCommand => _loadDataCommand ??= new RelayCommandAsync(LoadData);

        public ScreenshotsHistoryPlayerViewModel(ILogger logger,
                                                 string employeeId,
                                                 string date,
                                                 Dispatcher currentDispatcher,
                                                 IDialogService dialogService,
                                                 INotificationMessageQueue messageQueue) : base(dialogService, messageQueue)
        {
            _logger = logger;
            _employeeId = employeeId;
            _dateFrom = new Date(date);
            _currentDispatcher = currentDispatcher;

            var converter = new ScreenshotConverter();
            _cleanUp = _source.Connect()
                              .Filter(screenshot => screenshot?.Uri != null)
                              .Transform(converter.Convert)
                              .Sort(SortExpressionComparer<ImageModel>.Ascending(x => x.Timestamp))
                              .ObserveOnDispatcher()
                              .Bind(out _data)
                              .DisposeMany()
                              .Subscribe(OnUpdateList);

            InitialiseAutoPlay();
        }

        public void Dispose()
        {
            Clock.Stop();
            _cleanUp?.Dispose();
        }

        private void InitialiseAutoPlay()
        {
            for (var i = 0.5; i <= 2;)
            {
                SlideShowSpeedCoefficients.Add($"x{i}", i);
                i += 0.25;
            }

            SelectedSlideShowSpeedCoefficient = "x1";
            Clock.Interval =
                TimeSpan.FromMilliseconds(MAX_SLIDE_SHOW_SPEED -
                                          SLIDE_SHOW_SPEED_DEF_VALUE * SlideShowSpeedCoefficients[SelectedSlideShowSpeedCoefficient]);
            Clock.Tick += OnClockTick;
        }

        private bool CanSelectPreviousImage(int step)
        {
            return ImageListCollection?.IndexOf(SelectedImage) - step >= 0;
        }

        private void SelectPreviousImage(int step)
        {
            SelectedImage = ImageListCollection[ImageListCollection.IndexOf(SelectedImage) - step];
        }

        private bool CanSelectNextImage(int step)
        {
            return ImageListCollection?.IndexOf(SelectedImage) < ImageListCollection?.Count - step;
        }

        private void SelectNextImage(int step)
        {
            SelectedImage = ImageListCollection[ImageListCollection.IndexOf(SelectedImage) + step];
        }

        private void OnUpdateList(IChangeSet<ImageModel, string> changes)
        {
            if (SelectedImage == null && ImageListCollection?.Count >= 1)
            {
                SelectedImage = ImageListCollection[0];
                AllowAction = true;
                RelayCommandBase.Refresh();
            }

            if (ImageListCollection?.Count > 0)
            {
                LastImage = ImageListCollection[ImageListCollection.Count - 1];
            }
        }

        private async Task LoadData()
        {
            if (ImageListCollection?.Count > 0)
            {
                return;
            }

            IsBusy = true;
            try
            {
                var items = await _screenshotsProvider.GetImagesList(_employeeId, _dateFrom);
                _source.AddOrUpdate(items);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error occurred while {nameof(LoadData)}. {e.Message}");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void OnClockTick(object sender, EventArgs e)
        {
            var thisVm = this;
            if (thisVm.ImageListCollection != null && thisVm.ImageListCollection.Count > 1)
            {
                thisVm.SelectedImage = thisVm.ImageListCollection.IndexOf(thisVm.SelectedImage) < thisVm.ImageListCollection.Count - 1 
                    ? thisVm.ImageListCollection[thisVm.ImageListCollection.IndexOf(thisVm.SelectedImage) + 1] 
                    : thisVm.ImageListCollection[0];
            }
        }
    }
}