﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using CWT.StorageAdapter.Contracts;
using DSFramework.Timing;
using Newtonsoft.Json;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.ImagePlayer
{
    public class ScreenshotsProvider
    {
        public const string API_ROUTE = "https://localhost:5002/api/Screenshot";

        public async Task<List<ScreenshotInfo>> GetImagesList(string employeeId, Date date)
        {
            var url = new Uri($"{API_ROUTE}/{employeeId}/{date.ToString()}", UriKind.RelativeOrAbsolute);
            using var client = new HttpClient();
            using var request = new HttpRequestMessage(HttpMethod.Get, url);
            using var response = await client.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ScreenshotInfo>>(content);
        }
    }
}