﻿using CWT.Viewer.Infrastructure.InteractionRequest;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications
{
    public class AddDateColumnNotification : INotification
    {
        public string Header { get; set; }

        public string Binding { get; set; }
    }
}