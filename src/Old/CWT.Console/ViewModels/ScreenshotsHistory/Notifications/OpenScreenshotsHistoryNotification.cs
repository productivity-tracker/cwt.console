﻿using CWT.Viewer.Infrastructure.InteractionRequest;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory.Notifications
{
    public class OpenScreenshotsHistoryNotification : INotification
    {
        public string Header { get; set; }
        public string ScreenshotHistoryId { get; set; }
        public string EmployeeId { get; set; }
        public string Date { get; set; }
    }
}