﻿namespace CWT.Viewer.ViewModels.ScreenshotsHistory
{
    public class OpenHistoryCommandArgs
    {
        public string EmployeeId { get; }
        public string EmployeeName { get; }
        public string Date { get; }
        public string ScreenshotsHistoryId { get; }

        public OpenHistoryCommandArgs(string employeeName, string employeeId, string date, string screenshotsHistoryId)
        {
            EmployeeId = employeeId;
            EmployeeName = employeeName;
            Date = date;
            ScreenshotsHistoryId = screenshotsHistoryId;
        }
    }
}