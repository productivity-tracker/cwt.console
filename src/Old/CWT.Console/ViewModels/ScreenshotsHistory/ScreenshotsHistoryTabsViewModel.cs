﻿using System.Collections.ObjectModel;
using CWT.Console.Infrastructure;
using CWT.Viewer.Infrastructure;
using CWT.Viewer.ViewModels.Abstractions;

namespace CWT.Viewer.ViewModels.ScreenshotsHistory
{
    public class ScreenshotsHistoryTabsViewModel : AbstractViewModel, IScreenshotsHistoryTabsViewModel
    {
        private ViewContainer _selectedItem;

        public ViewContainer SelectedItem
        {
            get => _selectedItem;
            set => SetAndRaise(ref _selectedItem, value);
        }
        public ObservableCollection<ViewContainer> Items { get; }

        public ScreenshotsHistoryTabsViewModel(params ViewContainer[] items)
        {
            Items = new ObservableCollection<ViewContainer>(items);
        }
    }
}