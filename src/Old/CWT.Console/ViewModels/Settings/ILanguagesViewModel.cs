﻿using System.Collections.ObjectModel;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels
{
    public interface ILanguagesViewModel : ICloseableViewModel
    {
        ObservableCollection<string> Languages { get; }
        string SelectedItem { get; set; }
        ICommand ChangeLanguageCommand { get; }
    }
}