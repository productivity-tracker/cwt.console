﻿using System.Windows.Input;

namespace CWT.Viewer.ViewModels
{
    public interface IPaletteViewModel : ICloseableViewModel
    {
        ICommand ChangeBackgroundCommand { get; }
        ICommand ApplyPaletteCommand { get; }
    }
}