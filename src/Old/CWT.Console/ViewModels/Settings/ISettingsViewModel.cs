﻿using CWT.Viewer.Infrastructure.Commands;
using System.Collections.Generic;

namespace CWT.Viewer.ViewModels
{
    public interface ISettingsViewModel : ICloseableViewModel
    {
        string DisplayName { get; set; }
        string Username { get; set; }
        List<MenuItemViewModel> MenuItems { get; }
        IPaletteViewModel PaletteViewModel { get; }
        ILanguagesViewModel LanguagesViewModel { get; }

        RelayCommand OpenPaletteCommand { get; }
        RelayCommand OpenLanguagesCommand { get; }

    }
}