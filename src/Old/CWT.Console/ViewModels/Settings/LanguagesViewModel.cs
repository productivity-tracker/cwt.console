﻿using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models.Settings;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using TranslateMe;

namespace CWT.Viewer.ViewModels.Settings
{
    public class LanguagesViewModel : CloseableViewModel, ILanguagesViewModel
    {
        private readonly LanguageSettings _languageSettings;
        private readonly TM _languagesManager = TM.Instance;
        private string _selectedItem;

        public ObservableCollection<string> Languages => _languagesManager.AvailableLanguages;

        public string SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetAndRaise(ref _selectedItem, value);
                ChangeLanguage(_selectedItem);
            }
        }

        public ICommand ChangeLanguageCommand => new RelayCommand<string>(ChangeLanguage);

        public LanguagesViewModel(LanguageSettings languageSettings)
        {
            _languageSettings = languageSettings ?? throw new ArgumentNullException(nameof(languageSettings));
            _selectedItem = _languagesManager.CurrentLanguage;
            TM.Instance.LogOutMissingTranslations = true;
        }

        public void ChangeLanguage(string lang)
        {
            _languagesManager.CurrentLanguage = lang;
            _languageSettings.Language = lang;
        }
    }
}