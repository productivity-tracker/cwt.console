﻿using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models.Settings;
using CWT.Viewer.Services;
using MaterialDesignColors;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels.Settings
{
    public class PaletteViewModel : CloseableViewModel, IPaletteViewModel
    {
        private bool _isDarkTheme;
        private string _currentSwatch;

        private readonly IPaletteService _paletteService;
        private readonly PaletteSettings _paletteSettings;

        public ICommand ChangeBackgroundCommand => new RelayCommand<bool>(
            isDark =>
            {
                _paletteService.ChangeBackground(isDark);
                IsDark = isDark;
            });

        public ICommand ApplyPaletteCommand => new RelayCommand<Swatch>(
            swatch =>
            {
                _paletteService.ApplyPalette(swatch);
                CurrentSwatch = swatch.Name;
            });

        public IEnumerable<Swatch> Swatches => _paletteService.Swatches;

        public bool IsDark
        {
            get => _paletteSettings.ThemeIsDark;
            set
            {
                SetAndRaise(ref _isDarkTheme, value);
                _paletteSettings.ThemeIsDark = value;
            }
        }

        public string CurrentSwatch
        {
            get => _paletteSettings.ThemeColor;
            set
            {
                SetAndRaise(ref _currentSwatch, value);
                _paletteSettings.ThemeColor = value;
            }
        }

        public PaletteViewModel(IPaletteService paletteService, PaletteSettings paletteSettings)
        {
            _paletteService = paletteService ?? throw new ArgumentNullException(nameof(paletteService));
            _paletteSettings = paletteSettings;
        }
    }
}