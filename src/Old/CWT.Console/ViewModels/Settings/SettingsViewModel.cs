﻿using CWT.Viewer.Infrastructure.Commands;
using System;
using System.Collections.Generic;

namespace CWT.Viewer.ViewModels.Settings
{
    public class SettingsViewModel : CloseableViewModel, ISettingsViewModel
    {
        public IPaletteViewModel PaletteViewModel { get; }
        public ILanguagesViewModel LanguagesViewModel { get; }

        public string DisplayName { get; set; } = "Denis S";
        public string Username { get; set; } = "densidenko";
        public List<MenuItemViewModel> MenuItems { get; } = new List<MenuItemViewModel>();

        public RelayCommand OpenPaletteCommand => new RelayCommand(
            () => PaletteViewModel.IsOpen = true,
            () => !PaletteViewModel.IsOpen);
        public RelayCommand OpenLanguagesCommand => new RelayCommand(
            () => LanguagesViewModel.IsOpen = true,
            () => !LanguagesViewModel.IsOpen);

        public SettingsViewModel(IPaletteViewModel paletteViewModel, ILanguagesViewModel languagesViewModel)
        {
            PaletteViewModel = paletteViewModel ?? throw new ArgumentNullException(nameof(paletteViewModel));
            LanguagesViewModel = languagesViewModel ?? throw new ArgumentNullException(nameof(languagesViewModel));
        }
    }
}