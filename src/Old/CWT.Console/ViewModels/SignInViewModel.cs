﻿using System;
using System.Windows.Input;
using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models.Settings;
using CWT.Viewer.Services.Auth;
using CWT.Viewer.ViewModels.Abstractions;
using ReactiveUI;
using TranslateMe;

namespace CWT.Viewer.ViewModels
{
    public class SignInViewModel : AbstractViewModel, ISignInViewModel
    {
        private const int MAX_PASSWORD_LENGTH = 128;
        private const int MIN_PASSWORD_LENGTH = 6;

        private readonly IAuthenticationService _authenticationService;
        private readonly SignInSettings _settings;

        private bool _isAuthenticationExecuting;

        private RelayCommand _loginCommand;
        private string _password;
        private bool _rememberUsername;
        private string _username;

        public string Username
        {
            get => _username;
            set => SetAndRaise(ref _username, value);
        }

        public string Password
        {
            get => _password;
            set => SetAndRaise(ref _password, value);
        }

        public ICommand LoginCommand => _loginCommand ??= new RelayCommand(TryLogin, canExecute: () => !IsAuthenticationExecuting);

        public IInfoMassageViewModel MessageViewModel { get; }

        public bool RememberUsername
        {
            get => _rememberUsername;
            set => SetAndRaise(ref _rememberUsername, value);
        }

        public bool IsAuthenticationExecuting
        {
            get => _isAuthenticationExecuting;
            private set => SetAndRaise(ref _isAuthenticationExecuting, value);
        }

        public SignInViewModel(IAuthenticationService authenticationService, IInfoMassageViewModel messageViewModel, SignInSettings settings)
        {
            _authenticationService = authenticationService;
            _settings = settings ?? throw new ArgumentException(nameof(SignInSettings));
            MessageViewModel = messageViewModel;
            RememberUsername = _settings.RememberUsername;
            Username = _settings.Username;

            //PropertyChanged += (sender, args) => _loginCommand.RaiseCanExecuteChanged();
        }

        private async void TryLogin()
        {
            if (string.IsNullOrWhiteSpace(Username))
            {
                var message = string.Format(TM.Tr(textId: "RequireNotNullTemplate"), TM.Tr(textId: "Email"));
                ShowMessage(message);
                return;
            }

            if (string.IsNullOrWhiteSpace(Password) || Password.Length < MIN_PASSWORD_LENGTH)
            {
                var message = string.Format(TM.Tr(textId: "PasswordRequireTemplate"), MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH);
                ShowMessage(message);
                return;
            }

            try
            {
                _settings.RememberUsername = RememberUsername;
                _settings.Username = RememberUsername ? Username : string.Empty;

                IsAuthenticationExecuting = true;
                using (var creds = new UserCredential(Username, Password))
                {
                    await _authenticationService.LogIn(creds);
                }
            }
            finally
            {
                IsAuthenticationExecuting = false;
            }
        }

        private void ShowMessage(string message)
        {
            MessageViewModel.Message = message;
            MessageViewModel.IsOpen = true;
        }
    }
}