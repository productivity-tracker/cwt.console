﻿using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.Timing;

namespace CWT.Viewer.ViewModels.TimeRange
{
    public class CustomTimeRangeViewModel : AbstractViewModel, ICustomTimeRangeViewModel
    {
        public DateTimeRange TimeRange { get; }
        public bool IsOpen { get; set; }

        public CustomTimeRangeViewModel(DateTimeRange timeRange)
        {
            TimeRange = timeRange;
        }
    }
}