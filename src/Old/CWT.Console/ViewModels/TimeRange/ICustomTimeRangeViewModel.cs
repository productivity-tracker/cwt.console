﻿using DSFramework.Timing;

namespace CWT.Viewer.ViewModels.TimeRange
{
    public interface ICustomTimeRangeViewModel : ICloseableViewModel, ITransientViewModel
    {
        DateTimeRange TimeRange { get; }
    }
}