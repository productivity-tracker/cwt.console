﻿using CWT.Kernel.Contracts;
using CWT.Sdk.Facades.Interfaces;
using CWT.Viewer.Extensions;
using CWT.Viewer.Infrastructure.MappedValue;
using CWT.Viewer.Models.Timeline;
using CWT.Viewer.ViewModels.ControlPanel.Employees.Interfaces;
using CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList;
using DSFramework.GuardToolkit;
using DSFramework.Threading;
using DSFramework.Timing;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using TranslateMe;
using CompositeDisposable = System.Reactive.Disposables.CompositeDisposable;
using EmployeeModel = CWT.Viewer.Models.Employees.EmployeeModel;
using TimelineModel = CWT.Viewer.Models.Timeline.Timeline;

namespace CWT.Viewer.ViewModels.Timeline
{
    public class EmployeeTimelineContext : AbstractNotifyPropertyChanged, IEmployeeTimelineContext, IDisposable
    {
        public class TimelineKey : IEquatable<TimelineKey>
        {
            public Date Date { get; private set; }
            public string EmployeeId { get; private set; }

            public TimelineKey(Date date, string employeeId)
            {
                Date = date;
                EmployeeId = employeeId;
            }

            public bool Equals(TimelineKey other)
            {
                if (ReferenceEquals(null, other))
                {
                    return false;
                }

                if (ReferenceEquals(this, other))
                {
                    return true;
                }

                return Date.Equals(other.Date) && string.Equals(EmployeeId, other.EmployeeId);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj))
                {
                    return false;
                }

                if (ReferenceEquals(this, obj))
                {
                    return true;
                }

                if (obj.GetType() != this.GetType())
                {
                    return false;
                }

                return Equals((TimelineKey)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (Date.GetHashCode() * 397) ^ (EmployeeId != null ? EmployeeId.GetHashCode() : 0);
                }
            }

            public static bool operator ==(TimelineKey left, TimelineKey right)
            {
                return Equals(left, right);
            }

            public static bool operator !=(TimelineKey left, TimelineKey right)
            {
                return !Equals(left, right);
            }
        }

        private readonly IEmployeesFacade _employeesFacade;
        private readonly ITimelinesFacade _timelinesFacade;
        private readonly IEmployeeConverter _employeeConverter;

        private readonly ILogger _logger;
        private readonly IDisposable _cleanUp;

        private readonly ConcurrentDictionary<TimelineKey, MappedValue> _mappedValuesTags;

        private readonly ReadOnlyObservableCollection<EmployeeModel> _employees;
        private readonly SourceCache<EmployeeModel, string> _sourceEmployees;
        private ObservableCollection<DateModel> _dates;

        private readonly ReadOnlyObservableCollection<string> _organizationalUnits;
        private string _searchText;
        private string _selectedOrganizationalUnit;

        private readonly GenericLock<string> _genericLock = new GenericLock<string>();
        private Guid _timelinesChangeHandlerGuid;
        private Guid _timelinesCleanupHandlerGuid;
        private int _onlineEmployeesCount;

        public ReadOnlyObservableCollection<EmployeeModel> Employees => _employees;

        public ObservableCollection<DateModel> Dates
        {
            get => _dates;
            set => SetAndRaise(ref _dates, value);
        }

        public MappedValueCollection TimelinesMappings { get; }

        public ReadOnlyObservableCollection<string> OrganizationalUnits => _organizationalUnits;
        public string SearchText
        {
            get => _searchText;
            set => SetAndRaise(ref _searchText, value);
        }

        public string SelectedOrganizationalUnit
        {
            get => _selectedOrganizationalUnit;
            set => SetAndRaise(ref _selectedOrganizationalUnit, value);
        }

        public int OnlineEmployeesCount
        {
            get => _onlineEmployeesCount;
            private set => SetAndRaise(ref _onlineEmployeesCount, value);
        }

        public EmployeeTimelineContext(ILogger logger,
                                       IEmployeesFacade employeesFacade,
                                       ITimelinesFacade timelinesFacade,
                                       IEmployeeConverter employeeConverter)
        {
            _employeesFacade = Check.NotNull(employeesFacade, nameof(employeesFacade));
            _timelinesFacade = Check.NotNull(timelinesFacade, nameof(timelinesFacade));
            _employeeConverter = Check.NotNull(employeeConverter, nameof(employeeConverter));
            _logger = logger ?? NullLogger.Instance;

            TimelinesMappings = new MappedValueCollection();
            TimelinesMappings.CollectionChanged += OnTimelinesMappingsChanged;
            Dates = new ObservableCollection<DateModel>();
            _sourceEmployees = new SourceCache<EmployeeModel, string>(x => x.EntityId);
            _mappedValuesTags = new ConcurrentDictionary<TimelineKey, MappedValue>();

            var textFilter = this.WhenValueChanged(ctx => ctx.SearchText).Throttle(TimeSpan.FromMilliseconds(250)).Select(BuildEmployeeNameFilter);
            var orgUnitsFilter = this.WhenValueChanged(vm => vm.SelectedOrganizationalUnit)
                                     .Throttle(TimeSpan.FromMilliseconds(250))
                                     .Select(BuildOrgUnitFilter);

            var employeesLoader = _sourceEmployees.Connect()
                                                  .Filter(textFilter)
                                                  .Filter(orgUnitsFilter)
                                                  .ObserveOnDispatcher()
                                                  .Bind(out _employees)
                                                  .DisposeMany()
                                                  .Subscribe();

            var orgUnitsLoader = _sourceEmployees.Connect()
                                           .DistinctValues(x => "Unknown")
                                           .ObserveOnDispatcher()
                                           .Bind(out _organizationalUnits)
                                           .DisposeMany()
                                           .Subscribe();

            _cleanUp = new CompositeDisposable(employeesLoader, orgUnitsLoader);

            RegisterChangeHandlers();
        }

        public void Dispose()
        {
            _timelinesFacade.RemoveChangeHandler(_timelinesChangeHandlerGuid);
            _timelinesFacade.RemoveCleanUpHandler(_timelinesCleanupHandlerGuid);
            _cleanUp?.Dispose();
        }

        public async Task<ContextInitResult> LoadData()
        {
            var result = new ContextInitResult();

            try
            {
                var timelinesDtos = await _timelinesFacade.GetAll();
                if (timelinesDtos == null || !timelinesDtos.Any())
                {
                    result.IsSuccess = false;
                    result.Message = TM.Tr("HistoryIsEmpty");
                    return result;
                }

                var employeesDtos = await _employeesFacade.GetMany(timelinesDtos.Select(h => h.Data?.Key?.SourceId));

                _sourceEmployees.AddOrUpdate(employeesDtos.Select(_employeeConverter.Convert));
                timelinesDtos.ForEach(async dto => await HandleUpdate(dto));

                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while {nameof(LoadData)}. ");
                result.IsSuccess = false;
                result.Message = ex.Message;
            }

            return result;
        }

        public TimelineModel GeTimeline(TimelineKey key)
        {
            _mappedValuesTags.TryGetValue(key, out var mappedValue);
            return (TimelineModel)mappedValue?.Value;
        }

        public EmployeeModel GetEmployee(string employeeId)
        {
            return _sourceEmployees.KeyValues.FirstOrDefault(pair => pair.Key == employeeId).Value;
        }

        private void OnTimelinesMappingsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in e.NewItems)
                {
                    if (!(newItem is MappedValue newMappedValue))
                    {
                        continue;
                    }

                    var date = (DateModel)newMappedValue.ColumnBinding;
                    var employeeModel = (EmployeeModel)newMappedValue.RowBinding;
                    var key = new TimelineKey(date.Date, employeeModel.EntityId);
                    _mappedValuesTags.AddOrUpdate(key, newMappedValue, (timelineKey, value) => newMappedValue);
                }
            }
        }

        private void RegisterChangeHandlers()
        {
            _timelinesChangeHandlerGuid = _timelinesFacade.RegisterChangeHandler(async holder => await HandleUpdate(holder));
            _timelinesCleanupHandlerGuid = _timelinesFacade.RegisterCleanUpHandler(async holder => await HandleCleanup(holder));
        }

        private async Task HandleUpdate(EntityHolderOfTimeLineDto timelineHolder)
        {
            var timelineDto = timelineHolder.Data;
            if (timelineDto == null)
            {
                return;
            }

            var employeeId = timelineDto.Key.SourceId;
            var timeRanges = timelineHolder.Data.DateTimeRanges;
            await UpdateInternal(timelineHolder.Id, employeeId, timeRanges);
        }

        private Task HandleCleanup(EntityHolderOfTimeLineDto timelineHolder)
        {
            //var timelineDto = timelineHolder.Data;
            //if (timelineDto == null)
            //{
            //    return null;
            //}

            //var timelineModel = new TimelineModel(timelineHolder.Data.DateTimeRanges);
            //var localDate = timelineModel.Date;
            //var key = new TimelineKey(localDate, timelineDto.Key?.SourceId);
            //if (_mappedValuesTags.TryGetValue(key, out var mappedValue))
            //{
            //    mappedValue.Value = null;
            //}

            return Task.CompletedTask;
        }

        private async Task UpdateInternal(string timelineId, string employeeId, IEnumerable<DateTimeRange> timeRanges)
        {
            using (_genericLock.Lock(timelineId))
            {
                var localTimeRanges = ConvertToLocalTime(timeRanges);
                foreach (var localTimeRange in localTimeRanges)
                {
                    var date = localTimeRange.Key;
                    var timelineModel = new TimelineModel(localTimeRange.Value);
                    var key = new TimelineKey(date, employeeId);
                    if (_mappedValuesTags.TryGetValue(key, out var mappedValue))
                    {
                        //var currentTimeline = (TimelineModel)mappedValue.Value;
                        //if (currentTimeline.DayTimeRangeCommon.StartTime == currentTimeline.DayTimeRangeCommon.StartTime.Date)
                        //{
                        //    var updRanges = new List<DateTimeRange>(currentTimeline.TimeRanges)
                        //    {
                        //        new DateTimeRange(currentTimeline.DayTimeRangeCommon.StartTime,
                        //                          timelineModel.DayTimeRangeCommon.EndTime)
                        //    };
                        //    var updCurrentTimeline = new TimelineModel(updRanges.OrderBy(d => d.StartTime));
                        //    mappedValue.Value = updCurrentTimeline;
                        //    continue;
                        //}
                        mappedValue.Value = timelineModel;
                        return;
                    }

                    var dateModel = GetOrAddDate(date);
                    var employeeModel = await GetOrAddEmployee(employeeId);
                    mappedValue = new MappedValue
                    {
                        ColumnBinding = dateModel,
                        RowBinding = employeeModel,
                        Value = timelineModel
                    };

                    TimelinesMappings.Add(mappedValue);
                }
            }
        }

        private DateModel GetOrAddDate(Date date)
        {
            var model = Dates.FirstOrDefault(x => x.Date == date);
            if (model != null)
            {
                return model;
            }

            model = new DateModel(date);
            Dates.Add(model);
            return model;
        }

        private async Task<EmployeeModel> GetOrAddEmployee(string employeeId)
        {
            var model = GetEmployee(employeeId);
            if (model != null)
            {
                return model;
            }

            var employeeDto = await _employeesFacade.Get(employeeId);
            model = _employeeConverter.Convert(employeeDto);
            _sourceEmployees.AddOrUpdate(model);
            return model;
        }

        private static Dictionary<DateTime, List<DateTimeRange>> ConvertToLocalTime(IEnumerable<DateTimeRange> timeRanges)
        {
            var localTimeRanges = new Dictionary<DateTime, List<DateTimeRange>>();

            void Add(DateTime keyDate, DateTimeRange range)
            {
                if (localTimeRanges.TryGetValue(keyDate, out var ranges))
                {
                    ranges.Add(range);
                    return;
                }

                localTimeRanges[keyDate] = new List<DateTimeRange> { range };
            }

            var sortedRanges = timeRanges.OrderBy(x => x.StartTime);
            foreach (var timeRange in sortedRanges)
            {
                var localTimeRange = new DateTimeRange(timeRange.StartTime.ToLocalTime(), timeRange.EndTime.ToLocalTime());
                var startDate = localTimeRange.StartTime.Date;
                var endDate = localTimeRange.EndTime.Date;
                if (endDate > startDate)
                {
                    var startTime = localTimeRange.StartTime;
                    var endTime = localTimeRange.EndTime;

                    do
                    {
                        var endCurrentDay = startDate.AddDays(1).AddMilliseconds(-1.0);
                        var range = new DateTimeRange(startTime, endCurrentDay);
                        Add(startDate, range);

                        startTime = startTime.AddDays(1).Date;
                    }
                    while (startTime.Date < endTime.Date);

                    var lastDateStartTime = endTime.Date;
                    Add(lastDateStartTime, new DateTimeRange(lastDateStartTime, endTime));

                    continue;
                }

                Add(startDate, localTimeRange);
            }

            return localTimeRanges.OrderBy(pair => pair.Key).ToDictionary(x => x.Key, x => x.Value);
        }

        private Func<EmployeeModel, bool> BuildEmployeeNameFilter(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return employee => true;
            }

            return e => e.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase);
        }

        private Func<EmployeeModel, bool> BuildOrgUnitFilter(string orgUnit)
        {
            if (string.IsNullOrEmpty(orgUnit))
            {
                return employee => true;
            }

            return e => e.OrganizationalUnitIds.Count(ou => ou.Contains(orgUnit, StringComparison.OrdinalIgnoreCase)) > 0;
        }
    }
}