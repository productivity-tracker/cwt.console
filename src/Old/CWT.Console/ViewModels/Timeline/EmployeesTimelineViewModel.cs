﻿using CWT.Viewer.Infrastructure.Commands;
using CWT.Viewer.Models;
using CWT.Viewer.Services.Dialogs;
using CWT.Viewer.Services.Notifications;
using CWT.Viewer.ViewModels.Abstractions;
using DSFramework.GuardToolkit;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CWT.Viewer.ViewModels.Timeline
{
    public class EmployeesTimelineViewModel : AbstractPageViewModel, IEmployeesTimelineViewModel
    {
        private bool _isInit;
        public IEmployeeTimelineContext Context { get; }

        public ICommand LoadDataCommand => new RelayCommandAsync(async () => await Load(),
                                                                 o => !_isInit);

        public ICommand OpenTimelineCommand => new RelayCommand<EmployeeTimelineContext.TimelineKey>(OpenTimeline);
        public ICommand ClearFiltersCommand => new RelayCommand(ClearFilters);

        public EmployeesTimelineViewModel(IEmployeeTimelineContext context,
                                          IDialogService dialogService,
                                          INotificationMessageQueue notificationMessageQueue)
            : base(dialogService, notificationMessageQueue)
        {
            Context = Check.NotNull(context, nameof(context));
        }

        private async Task Load()
        {
            IsBusy = true;
            try
            {
                var result = await Context.LoadData();
                if (!result.IsSuccess)
                {
                    NotificationMessageQueue.Enqueue(result.Message);
                    return;
                }

                _isInit = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async void OpenTimeline(EmployeeTimelineContext.TimelineKey key)
        {
            var timeline = Context.GeTimeline(key);
            var employeeName = Context.GetEmployee(key.EmployeeId)?.Name;
            ITimelineViewModel vm = new TimelineViewModel(employeeName, key.Date, timeline.TimeRanges);

            await DialogService.ShowMessageBox(DialogIdentifier,
                                               $"{employeeName} ({key.Date})",
                                               vm,
                                               MessageBoxButtons.Ok);
        }

        private void ClearFilters()
        {
            Context.SearchText = null;
            Context.SelectedOrganizationalUnit = null;
        }
    }
}