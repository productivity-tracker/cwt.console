﻿using CWT.Viewer.Infrastructure.MappedValue;
using CWT.Viewer.Models.Employees;
using CWT.Viewer.Models.Timeline;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CWT.Viewer.ViewModels.ScreenshotsHistory.HistoryList;
using TimelineModel = CWT.Viewer.Models.Timeline.Timeline;

namespace CWT.Viewer.ViewModels.Timeline
{
    public interface IEmployeeTimelineContext
    {
        ReadOnlyObservableCollection<EmployeeModel> Employees { get; }
        ObservableCollection<DateModel> Dates { get; }
        MappedValueCollection TimelinesMappings { get; }

        string SearchText { get; set; }
        string SelectedOrganizationalUnit { get; set; }
        ReadOnlyObservableCollection<string> OrganizationalUnits { get; }
        int OnlineEmployeesCount { get; }

        Task<ContextInitResult> LoadData();
        TimelineModel GeTimeline(EmployeeTimelineContext.TimelineKey key);
        EmployeeModel GetEmployee(string employeeId);
    }
}