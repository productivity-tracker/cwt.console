﻿using System.Windows.Input;

namespace CWT.Viewer.ViewModels.Timeline
{
    public interface IEmployeesTimelineViewModel : IViewModel
    {
        IEmployeeTimelineContext Context { get; }
        ICommand LoadDataCommand { get; }
        ICommand OpenTimelineCommand { get; }
        ICommand ClearFiltersCommand { get; }
    }
}