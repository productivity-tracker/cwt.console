﻿using DSFramework.Timing;
using System.Collections.Generic;

namespace CWT.Viewer.ViewModels.Timeline
{
    public interface ITimelineViewModel : ICloseableViewModel, ITransientViewModel
    {
        string EmployeeName { get; }
        Date Date { get; }
        IList<DateTimeRange> TimeRanges { get; }
    }
}