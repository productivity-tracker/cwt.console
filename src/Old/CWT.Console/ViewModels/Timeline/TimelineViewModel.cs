﻿using System.Collections.Generic;
using DSFramework.Timing;
using DynamicData.Binding;

namespace CWT.Viewer.ViewModels.Timeline
{
    public class TimelineViewModel : AbstractNotifyPropertyChanged, ITimelineViewModel
    {
        private string _employeeName;
        private Date _date;
        private IList<DateTimeRange> _timeRanges;

        public string EmployeeName
        {
            get => _employeeName;
            private set => SetAndRaise(ref _employeeName, value);
        }

        public Date Date
        {
            get => _date;
            private set => SetAndRaise(ref _date, value);
        }

        public IList<DateTimeRange> TimeRanges
        {
            get => _timeRanges;
            private set => SetAndRaise(ref _timeRanges, value);
        }

        public bool IsOpen { get; set; }

        public TimelineViewModel(string employeeName, Date date, IList<DateTimeRange> timeRanges)
        {
            EmployeeName = employeeName;
            Date = date;
            TimeRanges = timeRanges;
        }
    }
}